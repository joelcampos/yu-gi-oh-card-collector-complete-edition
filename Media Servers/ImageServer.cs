﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    static class ImageServer
    {
        public static Image CheckMark()
        {
            return Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\Other Icons\\Checkmark.jpg");
        }

        public static Image StarIcon()
        {
            return Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\Other Icons\\StarMark.png");
        }

        public static Image ExclamationMark()
        {
            return Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\Other Icons\\exlamimark.png");
        }

        public static Image Rarity(string rarity)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + "\\images\\Rarities\\" + rarity + ".jpg"))
            {
                return Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\Rarities\\" + rarity + ".jpg");
            }
            else
            {
                return Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\Rarities\\Unknown Rarity.png");
            }
        }

        public static Image CardImage(int id)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + "\\images\\Cards\\" + id + ".jpg"))
            {
                return Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\Cards\\" + id + ".jpg");
            }
            else
            {
                return Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\Cards\\MissingCard.png");
            }
        }
    }
}
