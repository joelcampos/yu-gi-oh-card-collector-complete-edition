﻿//Joel Campos
//12/9/2021
//Database Class

using System;
using System.Collections.Generic;
using System.IO;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public static class DataBase
    {
        public static void LoadDataBase()
        {
            List<string> filenames = new List<string>();
            filenames.Add("Aqua.txt");
            filenames.Add("Beast.txt");
            filenames.Add("Beast-Warrior.txt");
            filenames.Add("Cyberse.txt");
            filenames.Add("Divine-Beast.txt");
            filenames.Add("Dinosaur.txt");
            filenames.Add("Dragon.txt");
            filenames.Add("Fairy.txt");
            filenames.Add("Fiend.txt");
            filenames.Add("Fish.txt");
            filenames.Add("Insect.txt");
            filenames.Add("Machine.txt");
            filenames.Add("Plant.txt");
            filenames.Add("Psychic.txt");
            filenames.Add("Pyro.txt");
            filenames.Add("Reptile.txt");
            filenames.Add("Rock.txt");
            filenames.Add("Sea Serpent.txt");
            filenames.Add("Spellcaster.txt");
            filenames.Add("Thunder.txt");
            filenames.Add("Warrior.txt");
            filenames.Add("Winged Beast.txt");
            filenames.Add("Wyrm.txt");
            filenames.Add("Zombie.txt");

            filenames.Add("Normal Spells.txt");
            filenames.Add("Continuous Spells.txt");
            filenames.Add("Equip Spells.txt");
            filenames.Add("Field Spells.txt");
            filenames.Add("Quick-Play Spells.txt");
            filenames.Add("Ritual Spells.txt");

            filenames.Add("Normal Traps.txt");
            filenames.Add("Continuous Traps.txt");
            filenames.Add("Counter Traps.txt");

            for (int x = 0; x < filenames.Count; x++)
            {
                LoadACardGroupDB(filenames[x]);
            }

            //Sort the main card list
            mainCardList.Sort();
            //Once sorted set the ID list
            for (int x = 0; x < mainCardList.Count; x++)
            {
                mainIDsList.Add(mainCardList[x].ID);
            }

            //Add missing codes card
            AddMissingCodes();
        }
        private static void LoadACardGroupDB(string filename)
        {
            StreamReader SR_SaveFile = new StreamReader(
                Directory.GetCurrentDirectory() + "\\Database\\" + filename);

            //String that hold the data of one line of the txt file
            string line = "";

            //Read the first line for the total cards registered
            line = SR_SaveFile.ReadLine();
            int cardCount = Convert.ToInt32(line);

            List<CardObject> thisCardList = new List<CardObject>();
            switch(filename)
            {
                case "Aqua.txt": thisCardList = aquaList; break;
                case "Beast.txt": thisCardList = beastList; break;
                case "Beast-Warrior.txt": thisCardList = beastWarriorList; break;
                case "Cyberse.txt": thisCardList = cyberseList; break;
                case "Dinosaur.txt": thisCardList = dinosaurList; break;
                case "Divine-Beast.txt": thisCardList = divineBeastList; break;
                case "Dragon.txt": thisCardList = dragonList; break;
                case "Fairy.txt": thisCardList = fairyList; break;
                case "Fiend.txt": thisCardList = fiendList; break;
                case "Fish.txt": thisCardList = fishList; break;
                case "Insect.txt": thisCardList = insectList; break;
                case "Machine.txt": thisCardList = machineList; break;
                case "Plant.txt": thisCardList = plantList; break;
                case "Psychic.txt": thisCardList = psychicList; break;
                case "Pyro.txt": thisCardList = pyroList; break;
                case "Reptile.txt": thisCardList = reptileList; break;
                case "Rock.txt": thisCardList = rockList; break;
                case "Sea Serpent.txt": thisCardList = seaSerpentList; break;
                case "Spellcaster.txt": thisCardList = spellcasterList; break;
                case "Thunder.txt": thisCardList = thunderList; break;
                case "Warrior.txt": thisCardList = warriorList; break;
                case "Winged Beast.txt": thisCardList = wingedBeastList; break;
                case "Wyrm.txt": thisCardList = wyrmList; break;
                case "Zombie.txt": thisCardList = zombieList; break;               
            }

            for (int x = 0; x < cardCount; x++)
            {
                line = SR_SaveFile.ReadLine();
                CardObject thisCardObject = new CardObject(line);
                thisCardList.Add(thisCardObject);
                mainCardList.Add(thisCardObject);

                if(thisCardObject.Name == "Aqua Madoor")
                {
                    int a = 0;
                }

                //Add card to card group
                if (!thisCardObject.Type.Contains("Spellcaster") && thisCardObject.Type.Contains("Spell")) 
                { 
                    spellList.Add(thisCardObject);
                    if (thisCardObject.Type.Contains("Normal")) { spellNormalList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Continuous")) { spellContinuousList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Equip")) { spellEquipList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Field")) { spellFieldList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Quick-Play")) { spellQuickplayList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Ritual")) { spellRitualList.Add(thisCardObject); }
                }
                else if (thisCardObject.Type.Contains("Trap"))
                { 
                    trapsList.Add(thisCardObject);
                    if (thisCardObject.Type.Contains("Normal")) { trapNormalList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Continuous")) { trapContinuousList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Counter")) { trapCounterList.Add(thisCardObject); }
                }
                else 
                { 
                    monstersList.Add(thisCardObject);

                    //Add card to corresponding sublists
                    if (thisCardObject.Type.Contains("Normal")) { normalList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Effect") && !thisCardObject.Type.Contains("Fusion") &&
                        !thisCardObject.Type.Contains("Ritual") && !thisCardObject.Type.Contains("Synchro") &&
                        !thisCardObject.Type.Contains("Xyz") && !thisCardObject.Type.Contains("Link"))
                    {
                        effectList.Add(thisCardObject);
                    }
                    if (thisCardObject.Type.Contains("Fusion")) { fusionList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Ritual")) { ritualList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Synchro")) { synchroList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Xyz")) { xyzList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Pendulum")) { pendulumList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Link")) { linkList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Flip")) { flipList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Spirit")) { spiritList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Toon")) { toonList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Union")) { unionList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Tuner")) { tunerList.Add(thisCardObject); }
                    if (thisCardObject.Type.Contains("Gemini")) { geminiList.Add(thisCardObject); }
                    if (thisCardObject.Attribute.Contains("WATER")) { waterList.Add(thisCardObject); }
                    if (thisCardObject.Attribute.Contains("FIRE")) { fireList.Add(thisCardObject); }
                    if (thisCardObject.Attribute.Contains("EARTH")) { earthList.Add(thisCardObject); }
                    if (thisCardObject.Attribute.Contains("WIND")) { windList.Add(thisCardObject); }
                    if (thisCardObject.Attribute.Contains("LIGHT")) { lightList.Add(thisCardObject); }
                    if (thisCardObject.Attribute.Contains("DARK")) { darkList.Add(thisCardObject); }
                    if (thisCardObject.Attribute.Contains("DIVINE")) { divineList.Add(thisCardObject); }
                }                   
            }

            //sort all subtype lists
            normalList.Sort();
            effectList.Sort();
            fusionList.Sort();
            ritualList.Sort();
            synchroList.Sort();
            xyzList.Sort();
            pendulumList.Sort();
            linkList.Sort();
            flipList.Sort();
            spiritList.Sort();
            toonList.Sort();
            unionList.Sort();
            tunerList.Sort();
            geminiList.Sort();
            waterList.Sort();
            fireList.Sort();
            earthList.Sort();
            windList.Sort();
            darkList.Sort();
            lightList.Sort();
            divineList.Sort();


            SR_SaveFile.Close();
        }
        public static void LoadSetsNames()
        {
            //Load the sets names into each group
            string[] filenames = new string[13];
            filenames[0] = "Booster Packs.txt";
            filenames[1] = "Duel Termina Cards.txt";
            filenames[2] = "Duelist Packs.txt";
            filenames[3] = "Magazines, Books, Comics.txt";
            filenames[4] = "Others.txt";
            filenames[5] = "Promotional Cards.txt";
            filenames[6] = "Special Edition Boxes.txt";
            filenames[7] = "SPEED DUEL.txt";
            filenames[8] = "Starter Decks.txt";
            filenames[9] = "Structure Decks.txt";
            filenames[10] = "Tins.txt";
            filenames[11] = "Tournaments.txt";
            filenames[12] = "Video Game Bundles.txt";

            for(int x = 0; x < filenames.Length; x++)
            {
                LoadIndividualSet(filenames[x]);
            }

            //Add the missing sets
            otherNameList.Add("Master Collection Volume 1");
            otherList.Add(new SetPack("Master Collection Volume 1"));

            otherNameList.Add("Legendary Collection 1");
            otherList.Add(new SetPack("Legendary Collection 1"));

            //tinsNameList.Add("2002 Collectors Tin");
            //tinsList.Add(new SetPack("2002 Collectors Tin"));
        }
        public static List<SingleCardObject> GetSetCardList(string setName)
        {
            string thisSetGroup = GetSetGroupOf(setName);

            int index = -1;
            switch (thisSetGroup)
            {
                case "Booster Packs":
                    index = boosterNameList.IndexOf(setName);
                    return boosterList[index].CardList;
                case "Special Edition Boxes":
                    index = spEditionBoxesNameList.IndexOf(setName);
                    return spEditionBoxesList[index].CardList;
                case "Starter Decks":
                    index = starterDecksNameList.IndexOf(setName);
                    return starterDecksList[index].CardList;
                 case "Structure Decks":
                    index = structureDecksNameList.IndexOf(setName);
                    return structureDecksList[index].CardList;
                 case "Tins":
                    index = tinsNameList.IndexOf(setName);
                    return tinsList[index].CardList;
                 case "SPEED DUEL":
                    index = speedDuelNameList.IndexOf(setName);
                    return speedDuelList[index].CardList;
                case "Duelist Packs":
                    index = duelistPackNameList.IndexOf(setName);
                    return duelistPackList[index].CardList;
                case "Duel Terminal Cards":
                    index = duelterminaNameList.IndexOf(setName);
                    return duelterminaList[index].CardList;
                 case "Others":
                    index = otherNameList.IndexOf(setName);
                    return otherList[index].CardList;
                 case "Magazines, Books, Comics":
                    index = magazineBooksComicsNameList.IndexOf(setName);
                    return magazineBooksComicsList[index].CardList;
                 case "Tournaments":
                    index = tournamentsNameList.IndexOf(setName);
                    return tournamentsList[index].CardList;
                case "Promotional Cards":
                    index = promotionalCardsNameList.IndexOf(setName);
                    return promotionalCardsList[index].CardList;
                case "Video Game Bundles":
                    index = videoGameBundlesNameList.IndexOf(setName);
                    return videoGameBundlesList[index].CardList;
                case "None":
                    index = noneNameList.IndexOf(setName);
                    return noneList[index].CardList;
                default: throw new Exception(thisSetGroup + " is not valid");
            }
        }

        private static void AddMissingCodes()
        {
            //Master Collection Volume 1 Cards
            CardObject exodiatheforbiddenone = GetCardObjectWithID("33396948");
            exodiatheforbiddenone.AddNewSetInfo("2002-01-01", "MC1-EN001", "Master Collection Volume 1", "Secret Rare", 7.14, 8.75);
            CardObject barreldragon = GetCardObjectWithID("81480461");
            barreldragon.AddNewSetInfo("2002-01-01", "MC1-EN002", "Master Collection Volume 1", "Secret Rare", 3.59, 3.68);
            CardObject relinquished = GetCardObjectWithID("64631466");
            relinquished.AddNewSetInfo("2002-01-01", "MC1-EN003", "Master Collection Volume 1", "Secret Rare", 5.24, 5.45);
            CardObject thousandeyesrestrict = GetCardObjectWithID("63519819");
            thousandeyesrestrict.AddNewSetInfo("2002-01-01", "MC1-EN004", "Master Collection Volume 1", "Secret Rare", 9.11, 9.80);
            CardObject darknecrofear = GetCardObjectWithID("31829185");
            darknecrofear.AddNewSetInfo("2002-01-01", "MC1-EN005", "Master Collection Volume 1", "Secret Rare", 2.67, 2.00);
            CardObject darkrulerhades = GetCardObjectWithID("53982768");
            darkrulerhades.AddNewSetInfo("2002-01-01", "MC1-EN006", "Master Collection Volume 1", "Secret Rare", 1.68, 1.55);

            //Legendary Collection 1 Cards
            CardObject obeliskthetormemtor = GetCardObjectWithID("10000000");
            obeliskthetormemtor.AddNewSetInfo("2002-01-01", "LC01-EN001", "Legendary Collection 1", "Ultra Rare", 2.12, 2.12);
            CardObject slifertheskydragon = GetCardObjectWithID("10000020");
            slifertheskydragon.AddNewSetInfo("2002-01-01", "LC01-EN002", "Legendary Collection 1", "Ultra Rare", 2.16, 2.00);
            CardObject thewingeddragonofra = GetCardObjectWithID("10000010");
            thewingeddragonofra.AddNewSetInfo("2002-01-01", "LC01-EN003", "Legendary Collection 1", "Ultra Rare", 2.25, 2.70);
            CardObject blueeyeswhitedragon = GetCardObjectWithID("89631139");
            blueeyeswhitedragon.AddNewSetInfo("2002-01-01", "LC01-EN004", "Legendary Collection 1", "Ultra Rare", 1.31, 1.65);
            CardObject darkmagician = GetCardObjectWithID("46986414");
            darkmagician.AddNewSetInfo("2002-01-01", "LC01-EN005", "Legendary Collection 1", "Ultra Rare", 0.86, 1.00);
            CardObject redeyesblackdragon = GetCardObjectWithID("74677422");
            redeyesblackdragon.AddNewSetInfo("2002-01-01", "LC01-EN006", "Legendary Collection 1", "Ultra Rare", 1.47, 1.62);

            //2002 Collectors Tin
            //CardObject dmagician = GetCardObjectWithID("46986414");
            //dmagician.AddNewSetInfo("2002-01-01", "BPT-001", "2002 Collectors Tin", "Secret Rare", 144.80, 89.21);
            //CardObject sumskull = GetCardObjectWithID("70781052");
            //sumskull.AddNewSetInfo("2002-01-01", "BPT-002", "2002 Collectors Tin", "Secret Rare", 67.76, 54.10);
            //CardObject blueeyss = GetCardObjectWithID("89631139");
            //blueeyss.AddNewSetInfo("2002-01-01", "BPT-003", "2002 Collectors Tin", "Secret Rare", 84.98, 86.00);
            //CardObject lordd = GetCardObjectWithID("17985575");
            //lordd.AddNewSetInfo("2002-01-01", "BPT-004", "2002 Collectors Tin", "Secret Rare", 17.17, 15.84);
            //CardObject redeysbdragon = GetCardObjectWithID("74677422");
            //redeysbdragon.AddNewSetInfo("2002-01-01", "BPT-005", "2002 Collectors Tin", "Secret Rare", 113.70, 74.99);
            //CardObject blackskull = GetCardObjectWithID("11901678");
            //blackskull.AddNewSetInfo("2002-01-01", "BPT-006", "2002 Collectors Tin", "Secret Rare", 73.98, 33.86);
        }

        public static SetPack GetSetPackObject(string setName)
        {
            string thisSetGroup = GetSetGroupOf(setName);

            int index = -1;
            switch (thisSetGroup)
            {
                case "Booster Packs":
                    index = boosterNameList.IndexOf(setName);
                    return boosterList[index];
                case "Special Edition Boxes":
                    index = spEditionBoxesNameList.IndexOf(setName);
                    return spEditionBoxesList[index];
                case "Starter Decks":
                    index = starterDecksNameList.IndexOf(setName);
                    return starterDecksList[index];
                case "Structure Decks":
                    index = structureDecksNameList.IndexOf(setName);
                    return structureDecksList[index];
                case "Tins":
                    index = tinsNameList.IndexOf(setName);
                    return tinsList[index];
                case "SPEED DUEL":
                    index = speedDuelNameList.IndexOf(setName);
                    return speedDuelList[index];
                case "Duelist Packs":
                    index = duelistPackNameList.IndexOf(setName);
                    return duelistPackList[index];
                case "Duel Terminal Cards":
                    index = duelterminaNameList.IndexOf(setName);
                    return duelterminaList[index];
                case "Others":
                    index = otherNameList.IndexOf(setName);
                    return otherList[index];
                case "Magazines, Books, Comics":
                    index = magazineBooksComicsNameList.IndexOf(setName);
                    return magazineBooksComicsList[index];
                case "Tournaments":
                    index = tournamentsNameList.IndexOf(setName);
                    return tournamentsList[index];
                case "Promotional Cards":
                    index = promotionalCardsNameList.IndexOf(setName);
                    return promotionalCardsList[index];
                case "Video Game Bundles":
                    index = videoGameBundlesNameList.IndexOf(setName);
                    return videoGameBundlesList[index];
                case "None":
                    index = noneNameList.IndexOf(setName);
                    return noneList[index];
                default: throw new Exception(thisSetGroup + " is not valid");
            }
        }
        public static int GetSetCardIndex(string setName)
        {
            string thisSetGroup = GetSetGroupOf(setName);

            int index = -1;
            switch (thisSetGroup)
            {
                case "Booster Packs":
                    index = boosterNameList.IndexOf(setName); break;
                case "Special Edition Boxes":
                    index = spEditionBoxesNameList.IndexOf(setName); break;
                case "Starter Decks":
                    index = starterDecksNameList.IndexOf(setName); break;
                case "Structure Decks":
                    index = structureDecksNameList.IndexOf(setName); break;
                case "Tins":
                    index = tinsNameList.IndexOf(setName); break;
                case "SPEED DUEL":
                    index = speedDuelNameList.IndexOf(setName); break;;
                case "Duelist Packs":
                    index = duelistPackNameList.IndexOf(setName); break;
                case "Duel Terminal Cards":
                    index = duelterminaNameList.IndexOf(setName); break;
                case "Others":
                    index = otherNameList.IndexOf(setName); break;
                case "Magazines, Books, Comics":
                    index = magazineBooksComicsNameList.IndexOf(setName); break;
                case "Tournaments":
                    index = tournamentsNameList.IndexOf(setName); break;
                case "Promotional Cards":
                    index = promotionalCardsNameList.IndexOf(setName); break;
                case "Video Game Bundles":
                    index = videoGameBundlesNameList.IndexOf(setName); break;
                case "None":
                    index = noneNameList.IndexOf(setName); break;
                default: throw new Exception(thisSetGroup + " is not valid");
            }

            return index;
        }
        private static void LoadIndividualSet(string filename)
        {
            //Set the list that this is going to be added to
            List<string> thisNameList = new List<string>();
            List<SetPack> thisSetList = new List<SetPack>();
            switch(filename)
            {
                case "Booster Packs.txt": thisNameList = boosterNameList; thisSetList = boosterList; break;
                case "Duel Termina Cards.txt": thisNameList = duelterminaNameList; thisSetList = duelterminaList; break;
                case "Duelist Packs.txt": thisNameList = duelistPackNameList; thisSetList = duelistPackList; break;
                case "Magazines, Books, Comics.txt": thisNameList = magazineBooksComicsNameList; thisSetList = magazineBooksComicsList; break;
                case "Others.txt": thisNameList = otherNameList; thisSetList = otherList; break;
                case "Promotional Cards.txt": thisNameList = promotionalCardsNameList; thisSetList = promotionalCardsList; break;
                case "Special Edition Boxes.txt": thisNameList = spEditionBoxesNameList; thisSetList = spEditionBoxesList; break;
                case "SPEED DUEL.txt": thisNameList = speedDuelNameList; thisSetList = speedDuelList; break;
                case "Starter Decks.txt": thisNameList = starterDecksNameList; thisSetList = starterDecksList; break;
                case "Structure Decks.txt": thisNameList = structureDecksNameList; thisSetList = structureDecksList; break;
                case "Tins.txt": thisNameList = tinsNameList; thisSetList = tinsList; break;
                case "Tournaments.txt": thisNameList = tournamentsNameList; thisSetList = tournamentsList; break;
                case "Video Game Bundles.txt": thisNameList = videoGameBundlesNameList; thisSetList = videoGameBundlesList; break;
            }

            //Open the file
            StreamReader SR_SaveFile = new StreamReader(
                Directory.GetCurrentDirectory() + "\\Database\\Sets\\" + filename);

            //String that hold the data of one line of the txt file
            string line = "";

            //Read the first line for the total cards registered
            line = SR_SaveFile.ReadLine();
            line = SR_SaveFile.ReadLine();
            int yearCount = Convert.ToInt32(line);

            for (int x = 0; x < yearCount; x++)
            {
                string thisYearLine = SR_SaveFile.ReadLine();
                //Separator used by Split()
                string[] separator = new string[] { "|" };
                //Array of tokens (Size will be rewritten when Split() is called)
                string[] tokens = new string[1];
                //Split the data
                tokens = thisYearLine.Split(separator, StringSplitOptions.None);

                int setCount = Convert.ToInt32(tokens[1]);

                for (int y = 0; y < setCount; y++)
                {
                    //Add this name to the name list
                    //thisNameList.Add(tokens[y + 2]);
                    thisNameList.Insert(0,tokens[y + 2]);
                    //Create a empty set to its list
                    thisSetList.Insert(0, new SetPack(tokens[y + 2]));
                }
            }            
        }
        public static void LoadSets()
        {
            //Scan the entire card database
            for (int x = 0; x < MainList.Count; x++)
            {
                //Save ref to the active card
                CardObject thisCard = MainList[x];
                //scan each set for each card
                for (int y = 0; y < thisCard.Sets.Count; y++)
                {
                    //Place each card in its respective set

                    //Create the single card
                    SingleCardObject thisSingleCard = new SingleCardObject(thisCard, thisCard.Sets[y].Code, thisCard.Sets[y].Rarity, thisCard.Sets[y].ReleaseDate, thisCard.Sets[y].MarketPrice, thisCard.Sets[y].MedianPrice);

                    //Add Card to its set
                    AddSingleCardToSet(thisSingleCard, thisCard.Sets[y].Name);
                }
            }
        }
        private static void AddSingleCardToSet(SingleCardObject thisCard, string setName)
        {
            string thisSetGroup = GetSetGroupOf(setName);

            int index = -1;
            switch (thisSetGroup)
            {
                case "Booster Packs":
                    index = boosterNameList.IndexOf(setName);
                    boosterList[index].AddCard(thisCard);
                    break;
                case "Special Edition Boxes":
                    index = spEditionBoxesNameList.IndexOf(setName);
                    spEditionBoxesList[index].AddCard(thisCard);
                    break;
                case "Starter Decks":
                    index = starterDecksNameList.IndexOf(setName);
                    starterDecksList[index].AddCard(thisCard);
                    break;
                case "Structure Decks":
                    index = structureDecksNameList.IndexOf(setName);
                    structureDecksList[index].AddCard(thisCard);
                    break;
                case "Tins":
                    index = tinsNameList.IndexOf(setName);
                    tinsList[index].AddCard(thisCard);
                    break;
                case "SPEED DUEL":
                    index = speedDuelNameList.IndexOf(setName);
                    speedDuelList[index].AddCard(thisCard);
                    break;
                case "Duelist Packs":
                    index = duelistPackNameList.IndexOf(setName);
                    duelistPackList[index].AddCard(thisCard);
                    break;
                case "Duel Terminal Cards":
                    index = duelterminaNameList.IndexOf(setName);
                    duelterminaList[index].AddCard(thisCard);
                    break;
                case "Others":
                    index = otherNameList.IndexOf(setName);
                    otherList[index].AddCard(thisCard);
                    break;
                case "Magazines, Books, Comics":
                    index = magazineBooksComicsNameList.IndexOf(setName);
                    magazineBooksComicsList[index].AddCard(thisCard);
                    break;
                case "Tournaments":
                    index = tournamentsNameList.IndexOf(setName);
                    tournamentsList[index].AddCard(thisCard);
                    break;
                case "Promotional Cards":
                    index = promotionalCardsNameList.IndexOf(setName);
                    promotionalCardsList[index].AddCard(thisCard);
                    break;
                case "Video Game Bundles":
                    index = videoGameBundlesNameList.IndexOf(setName);
                    videoGameBundlesList[index].AddCard(thisCard);
                    break;
                case "none":
                    index = noneNameList.IndexOf(setName);
                    noneList[index].AddCard(thisCard);
                    break;
            }
        }
        public static string GetSetGroupOf(string setName)
        {
            string group = "none";
            if(boosterNameList.Contains(setName)) { group = "Booster Packs";}
            if(spEditionBoxesNameList.Contains(setName)) { group = "Special Edition Boxes"; }
            if(starterDecksNameList.Contains(setName)) { group = "Starter Decks"; }
            if(structureDecksNameList.Contains(setName)) { group = "Structure Decks"; }
            if(tinsNameList.Contains(setName)) { group = "Tins";}
            if(speedDuelNameList.Contains(setName)) { group = "SPEED DUEL"; }
            if(duelistPackNameList.Contains(setName)) { group = "Duelist Packs"; }
            if(duelterminaNameList.Contains(setName)) { group = "Duel Terminal Cards"; }
            if(otherNameList.Contains(setName)) { group = "Others"; }
            if(magazineBooksComicsNameList.Contains(setName)) { group = "Magazines, Books, Comics"; }
            if(tournamentsNameList.Contains(setName)) { group = "Tournaments"; }
            if(promotionalCardsNameList.Contains(setName)) { group = "Promotional Cards"; }
            if(videoGameBundlesNameList.Contains(setName)) { group = "Video Game Bundles"; }

            if (group == "none") { throw new Exception("Set: " + setName + " wasn't found in the DB. Set list may need to be update. Check for the latest DB update."); }

            return group;
        }
        public static CardObject GetCardObjectWithID(string id)
        {
            int index = mainIDsList.IndexOf(id);

            if (index == -1) { throw new Exception("Card id: " + id + " is invalid or not in the database. (Check Savefile.txt for possible wrong data)"); }

            return mainCardList[index];
        }
        public static CardObject GetCardObjectWithSetCode(string code)
        {
            int index = -1;
            for(int x = 0; x < mainCardList.Count; x++)
            {
                if(mainCardList[x].HasCode(code))
                {
                    index = x; break;
                }
            }

            if (index == -1) { return null; }
            else { return mainCardList[index]; }
        }
        public static List<string> GetSetNameList(string setGroup)
        {
            List<SetPack> thisSetGroup;
            switch (setGroup)
            {
                //case "Booster Packs": return boosterNameList;
                //case "Special Edition Boxes": return spEditionBoxesNameList;
                //case "Starter Decks": return starterDecksNameList;
                //case "Structure Decks": return structureDecksNameList;
                //case "Tins": return tinsNameList;
                //case "SPEED DUEL": return speedDuelNameList;
                //case "Duelist Packs": return duelistPackNameList;
                //case "Duel Terminal Cards": return duelterminaNameList;
                //case "Others": return otherNameList;
                //case "Magazines, Books, Comics": return magazineBooksComicsNameList;
                //case "Tournaments": return tournamentsNameList;
                //case "Promotional Cards": return promotionalCardsNameList;
                //case "Video Game Bundles": return videoGameBundlesNameList;
                //default: throw new Exception("Set group: " + setGroup + " is invalid");
                case "Booster Packs": thisSetGroup = boosterList; break;
                case "Special Edition Boxes": thisSetGroup = spEditionBoxesList; break;
                case "Starter Decks": thisSetGroup = starterDecksList; break;
                case "Structure Decks": thisSetGroup = structureDecksList; break;
                case "Tins": thisSetGroup = tinsList; break;
                case "SPEED DUEL": thisSetGroup = speedDuelList; break;
                case "Duelist Packs": thisSetGroup = duelistPackList; break;
                case "Duel Terminal Cards": thisSetGroup = duelterminaList; break;
                case "Others": thisSetGroup = otherList; break;
                case "Magazines, Books, Comics": thisSetGroup = magazineBooksComicsList; break;
                case "Tournaments": thisSetGroup = tournamentsList; break;
                case "Promotional Cards": thisSetGroup = promotionalCardsList; break;
                case "Video Game Bundles": thisSetGroup = videoGameBundlesList; break;
                default: throw new Exception("Set group: " + setGroup + " is invalid");
            }

            List<string> sets = new List<string>();

            for(int x = 0; x < thisSetGroup.Count; x++)
            {
                sets.Add(thisSetGroup[x].Year + " - " + thisSetGroup[x].Code + " - " + thisSetGroup[x].Name);
            }

            return sets;
        }
        public static void SortAllSetPacksContents()
        {
            for(int x = 0; x < boosterList.Count; x++)
            {
                boosterList[x].SortContents();
            }
            for (int x = 0; x < spEditionBoxesList.Count; x++)
            {
                spEditionBoxesList[x].SortContents();
            }
            for (int x = 0; x < starterDecksList.Count; x++)
            {
                starterDecksList[x].SortContents();
            }
            for (int x = 0; x < structureDecksList.Count; x++)
            {
                structureDecksList[x].SortContents();
            }
            for (int x = 0; x < tinsList.Count; x++)
            {
                tinsList[x].SortContents();
            }
            for (int x = 0; x < speedDuelList.Count; x++)
            {
                speedDuelList[x].SortContents();
            }
            for (int x = 0; x < duelistPackList.Count; x++)
            {
                duelistPackList[x].SortContents();
            }
            for (int x = 0; x < duelterminaList.Count; x++)
            {
                duelterminaList[x].SortContents();
            }
            for (int x = 0; x < otherList.Count; x++)
            {
                otherList[x].SortContents();
            }
            for (int x = 0; x < magazineBooksComicsList.Count; x++)
            {
                magazineBooksComicsList[x].SortContents();
            }
            for (int x = 0; x < tournamentsList.Count; x++)
            {
                tournamentsList[x].SortContents();
            }
            for (int x = 0; x < promotionalCardsList.Count; x++)
            {
                promotionalCardsList[x].SortContents();
            }
            for (int x = 0; x < videoGameBundlesList.Count; x++)
            {
                videoGameBundlesList[x].SortContents();
            }
        }
        public static List<SetPack> GetSetsFromGroup(string group)
        {
            switch (group)
            {
                case "Booster Packs": return boosterList;
                case "Special Edition Boxes": return spEditionBoxesList;
                case "Starter Decks": return starterDecksList;
                case "Structure Decks": return structureDecksList;
                case "Tins": return tinsList;
                case "SPEED DUEL": return speedDuelList;
                case "Duelist Packs": return duelistPackList;
                case "Duel Terminal Cards": return duelterminaList;
                case "Others": return otherList;
                case "Magazines, Books, Comics": return magazineBooksComicsList;
                case "Tournaments": return tournamentsList;
                case "Promotional Cards": return promotionalCardsList;
                case "Video Game Bundles": return videoGameBundlesList;
                default: throw new Exception(group + "is an invalid group");
            }
        }

        public static int GetListObtainedCount(string group)
        {
            List<CardObject> thisList = GetCardTypeList(group);

            int count = 0;
            for(int x = 0; x < thisList.Count; x++)
            {
                if (thisList[x].Obtained) { count++; }
            }
            return count;
        }

        public static List<CardObject> GetCardTypeList(string group)
        {
            switch(group)
            {
                case "Aqua": return aquaList;
                case "Beast": return beastList;
                case "Beast-Warrior": return beastWarriorList;
                case "Cyberse": return cyberseList;
                case "Dinosaur": return dinosaurList;
                case "Divine-Beast": return divineBeastList;
                case "Dragon": return dragonList;
                case "Fairy": return fairyList;
                case "Fiend": return fiendList;
                case "Fish": return fishList;
                case "Insect": return insectList;
                case "Machine": return machineList;
                case "Plant": return plantList;
                case "Psychic": return psychicList;
                case "Pyro": return pyroList;
                case "Reptile": return reptileList;
                case "Rock": return rockList;
                case "Sea Serpent": return seaSerpentList;
                case "Spellcaster": return spellcasterList;
                case "Thunder": return thunderList;
                case "Warrior": return warriorList;
                case "Winged Beast": return wingedBeastList;
                case "Wyrm": return wyrmList;
                case "Zombie": return zombieList;

                case "All Monsters": return monstersList;
                case "All Spells": return spellList;
                case "All Traps": return trapsList;
                case "All Cards": return mainCardList;

                case "Normal Spells": return spellNormalList;
                case "Continuous Spells": return spellContinuousList;
                case "Equip Spells": return spellEquipList;
                case "Field Spells": return spellFieldList;
                case "Quick-Play Spells": return spellQuickplayList;
                case "Ritual Spells": return spellRitualList;

                case "Normal Traps": return trapNormalList;
                case "Continuous Traps": return trapContinuousList;
                case "Counter Traps": return trapCounterList;

                case "Normal Monsters": return normalList;
                case "Effect Monsters": return effectList;
                case "Fusion Monsters": return fusionList;
                case "Ritual Monsters": return ritualList;
                case "Synchro Monsters": return synchroList;
                case "Xyz Monsters": return xyzList;
                case "Pendulum Monsters": return pendulumList;
                case "Link Monsters": return linkList;


                case "Flip Monsters": return flipList;
                case "Spirit Monsters": return spiritList;
                case "Toon Monsters": return toonList;
                case "Union Monsters": return unionList;
                case "Tuner Monsters": return tunerList;
                case "Gemini Monsters": return geminiList;

                case "Water": return waterList;
                case "Fire": return fireList;
                case "Earth": return earthList;
                case "Wind": return windList;
                case "Dark": return darkList;
                case "Light": return lightList;
                case "Divine": return divineList;
                default: throw new Exception(group + " is not a valid filter group");
            }
        }
        public static List<CardObject> GetCardsWithString(string searchTerm)
        {
            List<CardObject> results = new List<CardObject>();

            for(int x = 0; x < mainCardList.Count; x++)
            {
                string cardname = mainCardList[x].Name;

                string allLowerCase = cardname.ToLower();

                string searchTermAllLowerCase = searchTerm.ToLower();

                if (allLowerCase.Contains(searchTermAllLowerCase)) { results.Add(mainCardList[x]); }
            }

            return results;
        }

       public static List<string> GetSetsWithCode(string code)
        {
            List<string> results = new List<string>();

            foreach (SetPack set in boosterList) 
            { 
                if(set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Booster Pack | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in starterDecksList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Starter Decks | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in structureDecksList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Structure Decks | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in spEditionBoxesList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Sp. Edtion Boxes | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in tinsList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Tins | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in speedDuelList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: SPEED DUEL | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in duelistPackList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Duelists Pack | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in duelterminaList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Duel Terminal | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in otherList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Other | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in magazineBooksComicsList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: MBC | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in tournamentsList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Tournaments | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in promotionalCardsList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Promo Cards | Set Name: " + set.Name);
                }
            }

            foreach (SetPack set in videoGameBundlesList)
            {
                if (set.Code == code)
                {
                    results.Add("Code: " + set.Code + " | Year: " + set.Year + " | Set Group: Video Game Bundles | Set Name: " + set.Name);
                }
            }

            return results;
        }


        #region Data
        public static List<CardObject> MainList { get { return mainCardList; } }

        private static List<CardObject> mainCardList = new List<CardObject>();
        private static List<string> mainIDsList = new List<string>();


        private static List<CardObject> monstersList = new List<CardObject>();
        private static List<CardObject> spellList = new List<CardObject>();
        private static List<CardObject> trapsList = new List<CardObject>();


        private static List<CardObject> aquaList = new List<CardObject>();
        private static List<CardObject> beastList = new List<CardObject>();
        private static List<CardObject> beastWarriorList = new List<CardObject>();
        private static List<CardObject> cyberseList = new List<CardObject>();
        private static List<CardObject> dinosaurList = new List<CardObject>();
        private static List<CardObject> divineBeastList = new List<CardObject>();
        private static List<CardObject> dragonList = new List<CardObject>();
        private static List<CardObject> fairyList = new List<CardObject>();
        private static List<CardObject> fiendList = new List<CardObject>();
        private static List<CardObject> fishList = new List<CardObject>();
        private static List<CardObject> insectList = new List<CardObject>();
        private static List<CardObject> machineList = new List<CardObject>();
        private static List<CardObject> plantList = new List<CardObject>();
        private static List<CardObject> psychicList = new List<CardObject>();
        private static List<CardObject> pyroList = new List<CardObject>();
        private static List<CardObject> reptileList = new List<CardObject>();
        private static List<CardObject> rockList = new List<CardObject>();
        private static List<CardObject> seaSerpentList = new List<CardObject>();
        private static List<CardObject> spellcasterList = new List<CardObject>();
        private static List<CardObject> thunderList = new List<CardObject>();
        private static List<CardObject> warriorList = new List<CardObject>();
        private static List<CardObject> wingedBeastList = new List<CardObject>();
        private static List<CardObject> wyrmList = new List<CardObject>();
        private static List<CardObject> zombieList = new List<CardObject>();

        private static List<CardObject> spellNormalList = new List<CardObject>();
        private static List<CardObject> spellContinuousList = new List<CardObject>();
        private static List<CardObject> spellEquipList = new List<CardObject>();
        private static List<CardObject> spellFieldList = new List<CardObject>();
        private static List<CardObject> spellQuickplayList = new List<CardObject>();
        private static List<CardObject> spellRitualList = new List<CardObject>();
        
        private static List<CardObject> trapNormalList = new List<CardObject>();
        private static List<CardObject> trapContinuousList = new List<CardObject>();
        private static List<CardObject> trapCounterList = new List<CardObject>();
  
        private static List<CardObject> normalList = new List<CardObject>();
        private static List<CardObject> effectList = new List<CardObject>();
        private static List<CardObject> fusionList = new List<CardObject>();
        private static List<CardObject> ritualList = new List<CardObject>();
        private static List<CardObject> synchroList = new List<CardObject>();
        private static List<CardObject> xyzList = new List<CardObject>();
        private static List<CardObject> pendulumList = new List<CardObject>();
        private static List<CardObject> linkList = new List<CardObject>();
        //Secondary lists
        private static List<CardObject> flipList = new List<CardObject>();
        private static List<CardObject> spiritList = new List<CardObject>();
        private static List<CardObject> toonList = new List<CardObject>();
        private static List<CardObject> unionList = new List<CardObject>();
        private static List<CardObject> tunerList = new List<CardObject>();
        private static List<CardObject> geminiList = new List<CardObject>();
        //Attributes lists
        private static List<CardObject> waterList = new List<CardObject>();
        private static List<CardObject> fireList = new List<CardObject>();
        private static List<CardObject> earthList = new List<CardObject>();
        private static List<CardObject> windList = new List<CardObject>();
        private static List<CardObject> lightList = new List<CardObject>();
        private static List<CardObject> darkList = new List<CardObject>();
        private static List<CardObject> divineList = new List<CardObject>();

        private static List<SetPack> boosterList = new List<SetPack>();
        private static List<SetPack> spEditionBoxesList = new List<SetPack>();
        private static List<SetPack> starterDecksList = new List<SetPack>();
        private static List<SetPack> structureDecksList = new List<SetPack>();
        private static List<SetPack> tinsList = new List<SetPack>();
        private static List<SetPack> speedDuelList = new List<SetPack>();
        private static List<SetPack> duelistPackList = new List<SetPack>();
        private static List<SetPack> duelterminaList = new List<SetPack>();
        private static List<SetPack> otherList = new List<SetPack>();
        private static List<SetPack> magazineBooksComicsList = new List<SetPack>();
        private static List<SetPack> tournamentsList = new List<SetPack>();
        private static List<SetPack> promotionalCardsList = new List<SetPack>();
        private static List<SetPack> videoGameBundlesList = new List<SetPack>();
        private static List<SetPack> noneList = new List<SetPack>();

        private static List<string> boosterNameList = new List<string>();
        private static List<string> spEditionBoxesNameList = new List<string>();
        private static List<string> starterDecksNameList = new List<string>();
        private static List<string> structureDecksNameList = new List<string>();
        private static List<string> tinsNameList = new List<string>();
        private static List<string> speedDuelNameList = new List<string>();
        private static List<string> duelistPackNameList = new List<string>();
        private static List<string> duelterminaNameList = new List<string>();
        private static List<string> otherNameList = new List<string>();
        private static List<string> magazineBooksComicsNameList = new List<string>();
        private static List<string> tournamentsNameList = new List<string>();
        private static List<string> promotionalCardsNameList = new List<string>();
        private static List<string> videoGameBundlesNameList = new List<string>();
        private static List<string> noneNameList = new List<string>();
        #endregion
    }
}
