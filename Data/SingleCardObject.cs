﻿//Joel Campos
//12/9/2021
//SingleCard Object Class

using System;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public class SingleCardObject : IComparable
    {
        public SingleCardObject(CardObject thisCard, string thisSetCode, string thisRarity, string thisReleaseDate, double thisMarketPrice, double thisMedianPrice)
        {
            baseCard = thisCard;
            setCode = thisSetCode;
            rarity = thisRarity;
            releaseDate = thisReleaseDate;
            marketPrice = thisMarketPrice;
            medianPrice = thisMedianPrice;
        }

        public CardObject BaseCard { get { return baseCard; } }
        public string Name { get { return baseCard.Name; } }
        public string ID { get { return baseCard.ID; } }
        public int IDInterger { get { return Convert.ToInt32(baseCard.ID); } }
        public string Code { get { return setCode; } }
        public string Rarity { get { return rarity; } }
        public string ReleaseDate { get { return releaseDate; } }
        public double MarketPrice { get { return marketPrice; } }
        public double MedianPrice { get { return medianPrice; } }
        public bool Obtained { get { return baseCard.IsObtained(setCode); } }

        private CardObject baseCard;
        private string setCode = "NONE";
        private string rarity = "Common";
        private string releaseDate = "YYYY/MM/DD";
        private double marketPrice = 0;
        private double medianPrice = 0;

        public int CompareTo(object obj)
        {
            SingleCardObject otherCard = obj as SingleCardObject;
            return Code.CompareTo(otherCard.Code);
        }
    }
}
