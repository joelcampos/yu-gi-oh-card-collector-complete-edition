﻿//Joel Campos
//12/9/2021
//CardObject Class

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public class CardObject : IComparable
    {
        public CardObject(string data)
        {
            //Separator used by Split()
            string[] separator = new string[] { "|" };
            //Array of tokens (Size will be rewritten when Split() is called)
            string[] tokens = new string[1];
            //Split the data
            tokens = data.Split(separator, StringSplitOptions.None);

            //Set all the data
            //CARD NAME
            _name = tokens[1];

            //ID
            string idfromdata = tokens[0];
            if (idfromdata == "IDMISSING") { throw new Exception("ID for CARD:" + _name + " is missing from the DB file."); }
            else { _id = tokens[0]; }
            
            //ATTRIBUTE
            _attribute = tokens[2];

            //CARD TYPE
            _type = tokens[3];

            //LEVEL/RANK/LINK
            _levelLinkRank = tokens[4];

            //ATA
            string atktmp = tokens[5];
            if (atktmp != "-" && atktmp != "?") { _attack = Convert.ToInt32(tokens[5]); }
            
            //DEF
            string deftmp = tokens[6];
            if (deftmp != "-" && deftmp != "?") { _defense = Convert.ToInt32(tokens[6]); }

            //PENDULUM SCALE
            if (tokens[7] != "") { _scale = Convert.ToInt32(tokens[7]); }

            //SETS COUNT
            int setsAmount = Convert.ToInt32(tokens[8]);

            int setIterator = 9;
            int priceIterator = 7 + (setsAmount * 4) + 2;
            List<string> codeaddedlist = new List<string>();
            for (int x = 0; x < setsAmount; x++)
            {
                string setDate = tokens[setIterator];
                string setCode = tokens[setIterator + 1];
                string setName = tokens[setIterator + 2];
                string setRarit = tokens[setIterator + 3];

                string marketPrice = tokens[priceIterator];
                string medianPrice = tokens[priceIterator + 1];
                //Remove the $ sign
                marketPrice = marketPrice.Substring(1);
                medianPrice = medianPrice.Substring(1);

                double marketPriceAmount = 0;
                double medianPriceAmount = 0;

                if (marketPrice != "") { marketPriceAmount = Convert.ToDouble(marketPrice); }
                if (medianPrice != "") { medianPriceAmount = Convert.ToDouble(medianPrice); }

                SetInfo thisSet = new SetInfo(setDate, setCode, setName, setRarit, marketPriceAmount, medianPriceAmount);
                //_sets.Add(thisSet);
                if (!codeaddedlist.Contains(setCode))
                {
                    _sets.Add(thisSet);
                    codeaddedlist.Add(setCode);
                }

                setIterator += 4;
                priceIterator += 2;
            }
        }

        public bool IsObtained(string code)
        {
            int indexOfSet = -1;
            for(int x = 0; x < _sets.Count; x++)
            {
                if (_sets[x].Code == code) { indexOfSet = x; break; }
            }

            return _sets[indexOfSet].Obtained;
        }

        public void FlipObtainedStatus(int setIndex)
        {
            _sets[setIndex].Obtained = !(_sets[setIndex].Obtained);
        }
        public void FlipObtainedStatus(string setCode)
        {
            for(int x = 0; x < _sets.Count; x++)
            {
                if(_sets[x].Code == setCode)
                {
                    _sets[x].Obtained = !(_sets[x].Obtained);
                }
            }
        }
        public bool HasCode(string setCode)
        {
            bool found = false;
            for (int x = 0; x < _sets.Count; x++)
            {
                if (_sets[x].Code == setCode)
                {
                    found = true; break;
                }
            }
            return found;
        }

        public void AddNewSetInfo(string date, string code, string name, string rarity, double marketPrice, double medianPrice)
        {
            Sets.Add(new SetInfo(date, code, name, rarity, marketPrice, medianPrice));
        }

        public string ID { get { return _id; } }
        public int IDInterger { get { return Convert.ToInt32(_id); } }
        public string Name { get { return _name; } }
        public string Attribute { get { return _attribute; } }
        public string Type {get{ return _type;} }
        public bool Obtained 
        { 
            get 
            {
                bool oneObtained = false;
                for (int x = 0; x < _sets.Count; x++)
                {
                    if (_sets[x].Obtained) { oneObtained = true; break; }
                }

                return oneObtained;
            }
        }
        public int SetsObtained 
        {
            get 
            {
                int count = 0;
                for (int x = 0; x < _sets.Count; x++)
                {
                    if (_sets[x].Obtained) { count++; }
                }

                return count;
            }
        }
        public List<SetInfo> Sets { get { return _sets; } }

        private string _id = "";
        private string _name = "";
        private string _attribute = "";
        private string _type = "";
        private string _levelLinkRank = "";
        private int _attack = 0;
        private int _defense = 0;
        private int _scale = 0;
        private List<SetInfo> _sets = new List<SetInfo>();

        public class SetInfo
        {
            public SetInfo(string date, string code, string name, string rarity, double marketPrice, double medianPrice)
            {
                _releaseDate = date;
                _name = name;
                _code = code;
                _rarity = rarity;
                _marketPrice = marketPrice;
                _medianPrice = medianPrice;
            }

            public string Name { get { return _name; } }
            public string Code { get { return _code; } }
            public string Rarity { get { return _rarity; } }
            public string ReleaseDate { get { return _releaseDate; } }
            public double MarketPrice { get { return _marketPrice; } }
            public double MedianPrice { get { return _medianPrice; } }
            public bool Obtained { get { return _obtained; } set { _obtained = value; } }

            private string _releaseDate = "";
            private string _name = "";
            private string _code = "";
            private string _rarity = "";
            private double _marketPrice = 0;
            private double _medianPrice = 0;
            private bool _obtained = false;
        }

        public int CompareTo(object obj)
        {
            CardObject otherCard = obj as CardObject;
            return _name.CompareTo(otherCard.Name);
        }
    }
}
