﻿//Joel Campos
//12/10/2021
//SetPack Class

using System.Collections.Generic;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public class SetPack
    {
        public SetPack(string name)
        {
            _name = name;
        }

        public void AddCard(SingleCardObject thisSingleCard)
        {
            _cardList.Add(thisSingleCard);

            if(!_dateSet)
            {
                string date = thisSingleCard.ReleaseDate;
                if (date != " " && date != "") { _releaseYear = date.Substring(0, 4); }
                _dateSet = true;
            }

            if(!_setCodeSet)
            {
                string code = thisSingleCard.Code;
                //If the code happens to be empty trigger an exption
                //if (code == "") { throw new System.Exception("Single Card:" + thisSingleCard.Name + " has a print with a null set code. Check DB file"); }
                
                if(code != "")
                {
                    int index = code.IndexOf("-");
                    _setCodePrefix = code.Substring(0, index);
                    _setCodeSet = true;
                }               
            }


        }
        public void SortContents()
        {
            _cardList.Sort();
        }

        public double GetSetMarketPrice()
        {
            double totalcost = 0;

            for (int x = 0; x < _cardList.Count; x++)
            {
                totalcost += _cardList[x].MarketPrice;
            }

            return totalcost;
        }

        public double GetSetMediantPrice()
        {
            double totalcost = 0;

            for (int x = 0; x < _cardList.Count; x++)
            {
                totalcost += _cardList[x].MedianPrice;
            }

            return totalcost;
        }

        public string Year { get { return _releaseYear; } }
        public string Code { get { return _setCodePrefix; } }
        public string Name { get { return _name; } }
        public int TotalCount { get { return _cardList.Count; } }
        public int CardsObtained 
        { 
            get 
            {
                int count = 0;
                for(int x = 0; x < _cardList.Count; x++)
                {
                    if (_cardList[x].Obtained) { count++; }
                }
                return count;
            } 
        }
        public List<SingleCardObject> CardList { get { return _cardList; } }

        private List<SingleCardObject> _cardList = new List<SingleCardObject>();
        private string _releaseYear = "0000";
        private string _setCodePrefix = "TMPV";
        private string _name = "name";
        private bool _setCodeSet = false;
        private bool _dateSet = false;
        private int _cardsObtained = 0;
    }
}
