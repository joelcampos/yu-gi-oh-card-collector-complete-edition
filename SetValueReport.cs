﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public partial class SetValueReport : Form
    {
        public SetValueReport(string setName, List<SingleCardObject> setCardList, Collector mainform)
        {
            InitializeComponent();
            lblSet.Text = setName;
            refMainForm = mainform;

            int Y_Cord = 40;

            Form Box = this;
            int Ysize = 14;

            //Code Label
            Label codeLabel = new Label();
            Box.Controls.Add(codeLabel);
            codeLabel.Text = "Set Code";
            codeLabel.BorderStyle = BorderStyle.FixedSingle;
            codeLabel.ForeColor = Color.White;
            codeLabel.AutoSize = false;
            codeLabel.Size = new Size(80, Ysize);
            codeLabel.Location = new Point(10, Y_Cord);

            //Name Label
            Label nameLabel = new Label();
            Box.Controls.Add(nameLabel);
            nameLabel.Text = "Card Name";
            nameLabel.BorderStyle = BorderStyle.FixedSingle;
            nameLabel.ForeColor = Color.White;
            nameLabel.AutoSize = false;
            nameLabel.Size = new Size(240, Ysize);
            nameLabel.Location = new Point(90, Y_Cord);

            //Rarity Label
            Label rarityLabel = new Label();
            Box.Controls.Add(rarityLabel);
            rarityLabel.Text = "Rarity";
            rarityLabel.BorderStyle = BorderStyle.FixedSingle;
            rarityLabel.ForeColor = Color.White;
            rarityLabel.AutoSize = false;
            rarityLabel.Size = new Size(120, Ysize);
            rarityLabel.Location = new Point(330, Y_Cord);

            //market Label
            Label marketLabel = new Label();
            Box.Controls.Add(marketLabel);
            marketLabel.Text = "Market Price";
            marketLabel.BorderStyle = BorderStyle.FixedSingle;
            marketLabel.ForeColor = Color.White;
            marketLabel.AutoSize = false;
            marketLabel.Size = new Size(80, Ysize);
            marketLabel.Location = new Point(450, Y_Cord);

            //median Label
            Label medianLabel = new Label();
            Box.Controls.Add(medianLabel);
            medianLabel.Text = "Median Price";
            medianLabel.BorderStyle = BorderStyle.FixedSingle;
            medianLabel.ForeColor = Color.White;
            medianLabel.AutoSize = false;
            medianLabel.Size = new Size(80, Ysize);
            medianLabel.Location = new Point(530, Y_Cord);

            //obtained Label
            Label obtainedLabel = new Label();
            Box.Controls.Add(obtainedLabel);
            obtainedLabel.Text = "Obtained?";
            obtainedLabel.BorderStyle = BorderStyle.FixedSingle;
            obtainedLabel.ForeColor = Color.White;
            obtainedLabel.AutoSize = false;
            obtainedLabel.Size = new Size(60, Ysize);
            obtainedLabel.Location = new Point(610, Y_Cord);

            Y_Cord += 14;


            for (int x = 0; x < setCardList.Count; x++)
            {
                InitializeLabelsRow(Y_Cord, setCardList[x]);
                Y_Cord += 14;
            }

            //Code Label
            Label TcodeLabel = new Label();
            Box.Controls.Add(TcodeLabel);
            TcodeLabel.Text = "-----";
            TcodeLabel.BorderStyle = BorderStyle.FixedSingle;
            TcodeLabel.ForeColor = Color.White;
            TcodeLabel.AutoSize = false;
            TcodeLabel.Size = new Size(80, Ysize);
            TcodeLabel.Location = new Point(10, Y_Cord);

            //Name Label
            Label TnameLabel = new Label();
            Box.Controls.Add(TnameLabel);
            TnameLabel.Text = "Set Totals";
            TnameLabel.BorderStyle = BorderStyle.FixedSingle;
            TnameLabel.ForeColor = Color.White;
            TnameLabel.AutoSize = false;
            TnameLabel.Size = new Size(240, Ysize);
            TnameLabel.Location = new Point(90, Y_Cord);

            //Rarity Label
            Label TrarityLabel = new Label();
            Box.Controls.Add(TrarityLabel);
            TrarityLabel.Text = "------";
            TrarityLabel.BorderStyle = BorderStyle.FixedSingle;
            TrarityLabel.ForeColor = Color.White;
            TrarityLabel.AutoSize = false;
            TrarityLabel.Size = new Size(120, Ysize);
            TrarityLabel.Location = new Point(330, Y_Cord);

            SetPack thisSet = DataBase.GetSetPackObject(setName);

            //market Label
            Label TmarketLabel = new Label();
            Box.Controls.Add(TmarketLabel);
            TmarketLabel.Text = "$" + thisSet.GetSetMarketPrice();
            TmarketLabel.BorderStyle = BorderStyle.FixedSingle;
            TmarketLabel.ForeColor = Color.White;
            TmarketLabel.AutoSize = false;
            TmarketLabel.Size = new Size(80, Ysize);
            TmarketLabel.Location = new Point(450, Y_Cord);

            //median Label
            Label TmedianLabel = new Label();
            Box.Controls.Add(TmedianLabel);
            TmedianLabel.Text = "$" + thisSet.GetSetMediantPrice();
            TmedianLabel.BorderStyle = BorderStyle.FixedSingle;
            TmedianLabel.ForeColor = Color.White;
            TmedianLabel.AutoSize = false;
            TmedianLabel.Size = new Size(80, Ysize);
            TmedianLabel.Location = new Point(530, Y_Cord);

            //obtained Label
            Label TobtainedLabel = new Label();
            Box.Controls.Add(TobtainedLabel);
            TobtainedLabel.Text = "";
            TobtainedLabel.BorderStyle = BorderStyle.FixedSingle;
            TobtainedLabel.ForeColor = Color.White;
            TobtainedLabel.AutoSize = false;
            TobtainedLabel.Size = new Size(60, Ysize);
            TobtainedLabel.Location = new Point(610, Y_Cord);

            Y_Cord += 14;

            double totalObtainedMarket = 0;
            double totalObtainedMedian = 0;
            int totalobtainedCards = 0;
            for(int x = 0; x < setCardList.Count; x++)
            {
                if(setCardList[x].Obtained)
                {
                    totalObtainedMarket += setCardList[x].MarketPrice;
                    totalObtainedMedian += setCardList[x].MedianPrice;
                    totalobtainedCards++;
                }
            }

            //Code Label
            Label CcodeLabel = new Label();
            Box.Controls.Add(CcodeLabel);
            CcodeLabel.Text = "-----";
            CcodeLabel.BorderStyle = BorderStyle.FixedSingle;
            CcodeLabel.ForeColor = Color.White;
            CcodeLabel.AutoSize = false;
            CcodeLabel.Size = new Size(80, Ysize);
            CcodeLabel.Location = new Point(10, Y_Cord);

            //Name Label
            Label CnameLabel = new Label();
            Box.Controls.Add(CnameLabel);
            CnameLabel.Text = "Collection Totals";
            CnameLabel.BorderStyle = BorderStyle.FixedSingle;
            CnameLabel.ForeColor = Color.White;
            CnameLabel.AutoSize = false;
            CnameLabel.Size = new Size(240, Ysize);
            CnameLabel.Location = new Point(90, Y_Cord);

            //Rarity Label
            Label CrarityLabel = new Label();
            Box.Controls.Add(CrarityLabel);
            CrarityLabel.Text = "------";
            CrarityLabel.BorderStyle = BorderStyle.FixedSingle;
            CrarityLabel.ForeColor = Color.White;
            CrarityLabel.AutoSize = false;
            CrarityLabel.Size = new Size(120, Ysize);
            CrarityLabel.Location = new Point(330, Y_Cord);

            //market Label
            Label CmarketLabel = new Label();
            Box.Controls.Add(CmarketLabel);
            CmarketLabel.Text = "$" + totalObtainedMarket;
            CmarketLabel.BorderStyle = BorderStyle.FixedSingle;
            CmarketLabel.ForeColor = Color.White;
            CmarketLabel.AutoSize = false;
            CmarketLabel.Size = new Size(80, Ysize);
            CmarketLabel.Location = new Point(450, Y_Cord);

            //median Label
            Label CmedianLabel = new Label();
            Box.Controls.Add(CmedianLabel);
            CmedianLabel.Text = "$" + totalObtainedMedian;
            CmedianLabel.BorderStyle = BorderStyle.FixedSingle;
            CmedianLabel.ForeColor = Color.White;
            CmedianLabel.AutoSize = false;
            CmedianLabel.Size = new Size(80, Ysize);
            CmedianLabel.Location = new Point(530, Y_Cord);

            //obtained Label
            Label CobtainedLabel = new Label();
            Box.Controls.Add(CobtainedLabel);
            CobtainedLabel.Text = totalobtainedCards + " cards";
            CobtainedLabel.BorderStyle = BorderStyle.FixedSingle;
            CobtainedLabel.ForeColor = Color.White;
            CobtainedLabel.AutoSize = false;
            CobtainedLabel.Size = new Size(60, Ysize);
            CobtainedLabel.Location = new Point(610, Y_Cord);

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            refMainForm.ReenableSetReportButton();
        }

        private Collector refMainForm = null;

        private void InitializeLabelsRow(int GroupNameLabelYPoint, SingleCardObject thisCard)
        {
            Form Box = this;
            int Ysize = 14;
            
            //Code Label
            Label codeLabel = new Label();
            Box.Controls.Add(codeLabel);
            codeLabel.Text = thisCard.Code;
            codeLabel.BorderStyle = BorderStyle.FixedSingle;
            codeLabel.ForeColor = Color.White;
            codeLabel.AutoSize = false;
            codeLabel.Size = new Size(80, Ysize);
            codeLabel.Location = new Point(10, GroupNameLabelYPoint);

            //Name Label
            Label nameLabel = new Label();
            Box.Controls.Add(nameLabel);
            nameLabel.Text = thisCard.Name;
            nameLabel.BorderStyle = BorderStyle.FixedSingle;
            nameLabel.ForeColor = Color.White;
            nameLabel.AutoSize = false;
            nameLabel.Size = new Size(240, Ysize);
            nameLabel.Location = new Point(90, GroupNameLabelYPoint);

            //Rarity Label
            Label rarityLabel = new Label();
            Box.Controls.Add(rarityLabel);
            rarityLabel.Text = thisCard.Rarity;
            rarityLabel.BorderStyle = BorderStyle.FixedSingle;
            rarityLabel.ForeColor = Color.White;
            rarityLabel.AutoSize = false;
            rarityLabel.Size = new Size(120, Ysize);
            rarityLabel.Location = new Point(330, GroupNameLabelYPoint);

            switch (thisCard.Rarity)
            {
                case "Common": rarityLabel.ForeColor = Color.White; break;
                case "Rare": rarityLabel.ForeColor = Color.PaleGreen; break;
                case "Ultra Rare": rarityLabel.ForeColor = Color.Moccasin; break;
                case "Ultimate Rare": rarityLabel.ForeColor = Color.HotPink; break;
                case "Gold Rare": rarityLabel.ForeColor = Color.Gold; break;
                case "Hobby": rarityLabel.ForeColor = Color.MediumPurple; break;
                case "Millennium Secret Rare": rarityLabel.ForeColor = Color.Goldenrod; break;
                case "Platinum Secret Rare": rarityLabel.ForeColor = Color.LightSkyBlue; break;
                case "Starfoil": rarityLabel.ForeColor = Color.BlueViolet; break;
                case "Shattefoil": rarityLabel.ForeColor = Color.MediumTurquoise; break;
                case "Super Rare": rarityLabel.ForeColor = Color.SteelBlue; break;
                case "Secret Rare": rarityLabel.ForeColor = Color.Pink; break;
                case "Ghost Rare": rarityLabel.ForeColor = Color.PowderBlue; break;
                case "Gold Secret": rarityLabel.ForeColor = Color.Yellow; break;
                case "Platinum Rare": rarityLabel.ForeColor = Color.Aqua; break;
                case "Mosaic Rare": rarityLabel.ForeColor = Color.DarkViolet; break;
                case "Prismatic Secret Rare": rarityLabel.ForeColor = Color.Plum; break;
                default: rarityLabel.ForeColor = Color.White; break;
            }

            //market Label
            Label marketLabel = new Label();
            Box.Controls.Add(marketLabel);
            marketLabel.Text = "$" + thisCard.MarketPrice.ToString();
            marketLabel.BorderStyle = BorderStyle.FixedSingle;
            marketLabel.ForeColor = Color.White;
            marketLabel.AutoSize = false;
            marketLabel.Size = new Size(80, Ysize);
            marketLabel.Location = new Point(450, GroupNameLabelYPoint);

            if (thisCard.MarketPrice < 1)
            {
                marketLabel.ForeColor = Color.White;
            }
            else if (thisCard.MarketPrice < 5)
            {
                marketLabel.ForeColor = Color.LightGreen;
            }
            else if (thisCard.MarketPrice < 50)
            {
                marketLabel.ForeColor = Color.HotPink;
            }
            else
            {
                marketLabel.ForeColor = Color.Gold;
            }

            //median Label
            Label medianLabel = new Label();
            Box.Controls.Add(medianLabel);
            medianLabel.Text = "$" + thisCard.MedianPrice.ToString();
            medianLabel.BorderStyle = BorderStyle.FixedSingle;
            medianLabel.ForeColor = Color.White;
            medianLabel.AutoSize = false;
            medianLabel.Size = new Size(80, Ysize);
            medianLabel.Location = new Point(530, GroupNameLabelYPoint);

            if (thisCard.MedianPrice < 1)
            {
                medianLabel.ForeColor = Color.White;
            }
            else if (thisCard.MedianPrice < 5)
            {
                medianLabel.ForeColor = Color.LightGreen;
            }
            else if (thisCard.MedianPrice < 50)
            {
                medianLabel.ForeColor = Color.HotPink;
            }
            else
            {
                medianLabel.ForeColor = Color.Gold;
            }

            //obtained Label
            Label obtainedLabel = new Label();
            Box.Controls.Add(obtainedLabel);
            if (thisCard.Obtained) { obtainedLabel.Text = "x"; }
            obtainedLabel.BorderStyle = BorderStyle.FixedSingle;
            obtainedLabel.ForeColor = Color.White;
            obtainedLabel.AutoSize = false;
            obtainedLabel.Size = new Size(60, Ysize);
            obtainedLabel.Location = new Point(610, GroupNameLabelYPoint);
        }
    }
}
