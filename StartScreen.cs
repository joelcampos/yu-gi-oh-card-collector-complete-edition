﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public partial class StartScreen : Form
    {
        public StartScreen()
        {
            InitializeComponent();
        }

        public void UpdateLoading(int percent)
        {
            bar1.Value = percent;

            if(percent == 100)
            {
                Hide();
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Visible = false;
            lblerrorOutpput.Visible = false;
            lblLoading.Visible = true;
            bar1.Visible = true;

            //Collector mainform = new Collector(this);
            //mainform.Show();

            try
            {
                Collector mainform = new Collector(this);
                mainform.Show();
            }
            catch(Exception ei)
            {
                lblerrorOutpput.Visible = true;
                lblerrorOutpput.Text = "Error: " + ei.Message + "\rFor further assistance message u/joelcampos5";
                lblLoading.Visible = false;
                bar1.Visible = false;
            }
            btnStart.Visible = true;
        }
    }
}
