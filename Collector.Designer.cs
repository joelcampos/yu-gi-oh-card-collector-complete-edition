﻿
namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    partial class Collector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Collector));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TabCardView = new System.Windows.Forms.TabPage();
            this.GroupCardView = new System.Windows.Forms.GroupBox();
            this.TabStats = new System.Windows.Forms.TabPage();
            this.BoxOtherStats = new System.Windows.Forms.GroupBox();
            this.BoxTrapsStats = new System.Windows.Forms.GroupBox();
            this.BoxSpellsStats = new System.Windows.Forms.GroupBox();
            this.BoxMonsterStats = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TabBoosterPacks = new System.Windows.Forms.TabPage();
            this.TabSpecialEditonBoxes = new System.Windows.Forms.TabPage();
            this.TabStsarterDecks = new System.Windows.Forms.TabPage();
            this.TabStructureDecks = new System.Windows.Forms.TabPage();
            this.TabTins = new System.Windows.Forms.TabPage();
            this.TabSPDuel = new System.Windows.Forms.TabPage();
            this.TabDP = new System.Windows.Forms.TabPage();
            this.TabDT = new System.Windows.Forms.TabPage();
            this.TabOthers = new System.Windows.Forms.TabPage();
            this.TabMBC = new System.Windows.Forms.TabPage();
            this.TabTournaments = new System.Windows.Forms.TabPage();
            this.TabPromo = new System.Windows.Forms.TabPage();
            this.TabVideoGames = new System.Windows.Forms.TabPage();
            this.lblFilterTittle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFilterZombie = new System.Windows.Forms.Button();
            this.btnFilterWyrm = new System.Windows.Forms.Button();
            this.btnFilterWingedBeast = new System.Windows.Forms.Button();
            this.btnFilterWarrior = new System.Windows.Forms.Button();
            this.btnFilterThunder = new System.Windows.Forms.Button();
            this.btnFilterSpellcaster = new System.Windows.Forms.Button();
            this.btnFilterPsychic = new System.Windows.Forms.Button();
            this.btnFilterFiend = new System.Windows.Forms.Button();
            this.btnFilterCyberce = new System.Windows.Forms.Button();
            this.btnFilterSeaSerpent = new System.Windows.Forms.Button();
            this.btnFilterPlant = new System.Windows.Forms.Button();
            this.btnFilterFairy = new System.Windows.Forms.Button();
            this.btnFilterRock = new System.Windows.Forms.Button();
            this.btnFilterReptile = new System.Windows.Forms.Button();
            this.btnFilterPyro = new System.Windows.Forms.Button();
            this.btnFilterMachine = new System.Windows.Forms.Button();
            this.btnFilterInsect = new System.Windows.Forms.Button();
            this.btnFilterFish = new System.Windows.Forms.Button();
            this.btnFilterDragon = new System.Windows.Forms.Button();
            this.btnFilterDivine = new System.Windows.Forms.Button();
            this.btnFilterDinosaur = new System.Windows.Forms.Button();
            this.btnFilterBeastWarrior = new System.Windows.Forms.Button();
            this.btnFilterBeast = new System.Windows.Forms.Button();
            this.btnFilterAqua = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnTextSearch = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnToon = new System.Windows.Forms.Button();
            this.btnTuner = new System.Windows.Forms.Button();
            this.btnUnion = new System.Windows.Forms.Button();
            this.btnGemini = new System.Windows.Forms.Button();
            this.btnSpirit = new System.Windows.Forms.Button();
            this.btnFlip = new System.Windows.Forms.Button();
            this.groupSpell = new System.Windows.Forms.GroupBox();
            this.btnRitualSpell = new System.Windows.Forms.Button();
            this.btnEquipSpell = new System.Windows.Forms.Button();
            this.btnQuickPlaySpell = new System.Windows.Forms.Button();
            this.btnFieldSpell = new System.Windows.Forms.Button();
            this.btnContinousSpell = new System.Windows.Forms.Button();
            this.btnNormalSpell = new System.Windows.Forms.Button();
            this.groupColor = new System.Windows.Forms.GroupBox();
            this.btnEffect = new System.Windows.Forms.Button();
            this.btnLink = new System.Windows.Forms.Button();
            this.btnPendulum = new System.Windows.Forms.Button();
            this.btnXyz = new System.Windows.Forms.Button();
            this.btnSynchro = new System.Windows.Forms.Button();
            this.btnRitual = new System.Windows.Forms.Button();
            this.btnFusion = new System.Windows.Forms.Button();
            this.btnNormal = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCounterTrap = new System.Windows.Forms.Button();
            this.btnContinuosTrap = new System.Windows.Forms.Button();
            this.btnNormalTrap = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnTraps = new System.Windows.Forms.Button();
            this.btnSpells = new System.Windows.Forms.Button();
            this.btnMonster = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.PanelTagList = new System.Windows.Forms.Panel();
            this.PicImage = new System.Windows.Forms.PictureBox();
            this.lblTCGName = new System.Windows.Forms.Label();
            this.lblRariryLabel = new System.Windows.Forms.Label();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.btnPreviousPage = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.chkCollected = new System.Windows.Forms.CheckBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.MonsterType = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDivine = new System.Windows.Forms.Button();
            this.btnWind = new System.Windows.Forms.Button();
            this.btnFire = new System.Windows.Forms.Button();
            this.btnWater = new System.Windows.Forms.Button();
            this.btnEarth = new System.Windows.Forms.Button();
            this.btnLight = new System.Windows.Forms.Button();
            this.btnDark = new System.Windows.Forms.Button();
            this.lblRarity = new System.Windows.Forms.Label();
            this.lblMarketPrice = new System.Windows.Forms.Label();
            this.lblMarketPricelabel = new System.Windows.Forms.Label();
            this.lblMedianPrice = new System.Windows.Forms.Label();
            this.lblMedianPricelabel = new System.Windows.Forms.Label();
            this.listSetGroups = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listSetlist = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFilterSet = new System.Windows.Forms.Button();
            this.btnMissingCards = new System.Windows.Forms.Button();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblCodelabel = new System.Windows.Forms.Label();
            this.btnValueReport = new System.Windows.Forms.Button();
            this.btnSetPriceReport = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.TabCardView.SuspendLayout();
            this.TabStats.SuspendLayout();
            this.BoxMonsterStats.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupSpell.SuspendLayout();
            this.groupColor.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicImage)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.MonsterType.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TabCardView);
            this.tabControl1.Controls.Add(this.TabStats);
            this.tabControl1.Controls.Add(this.TabBoosterPacks);
            this.tabControl1.Controls.Add(this.TabSpecialEditonBoxes);
            this.tabControl1.Controls.Add(this.TabStsarterDecks);
            this.tabControl1.Controls.Add(this.TabStructureDecks);
            this.tabControl1.Controls.Add(this.TabTins);
            this.tabControl1.Controls.Add(this.TabSPDuel);
            this.tabControl1.Controls.Add(this.TabDP);
            this.tabControl1.Controls.Add(this.TabDT);
            this.tabControl1.Controls.Add(this.TabOthers);
            this.tabControl1.Controls.Add(this.TabMBC);
            this.tabControl1.Controls.Add(this.TabTournaments);
            this.tabControl1.Controls.Add(this.TabPromo);
            this.tabControl1.Controls.Add(this.TabVideoGames);
            this.tabControl1.Location = new System.Drawing.Point(4, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(641, 480);
            this.tabControl1.TabIndex = 197;
            // 
            // TabCardView
            // 
            this.TabCardView.BackColor = System.Drawing.Color.Black;
            this.TabCardView.Controls.Add(this.GroupCardView);
            this.TabCardView.Location = new System.Drawing.Point(4, 22);
            this.TabCardView.Name = "TabCardView";
            this.TabCardView.Padding = new System.Windows.Forms.Padding(3);
            this.TabCardView.Size = new System.Drawing.Size(633, 454);
            this.TabCardView.TabIndex = 0;
            this.TabCardView.Text = "CardView";
            // 
            // GroupCardView
            // 
            this.GroupCardView.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupCardView.ForeColor = System.Drawing.Color.Lime;
            this.GroupCardView.Location = new System.Drawing.Point(3, -2);
            this.GroupCardView.Name = "GroupCardView";
            this.GroupCardView.Size = new System.Drawing.Size(621, 455);
            this.GroupCardView.TabIndex = 0;
            this.GroupCardView.TabStop = false;
            this.GroupCardView.Text = "Page 1";
            // 
            // TabStats
            // 
            this.TabStats.BackColor = System.Drawing.Color.Black;
            this.TabStats.Controls.Add(this.BoxOtherStats);
            this.TabStats.Controls.Add(this.BoxTrapsStats);
            this.TabStats.Controls.Add(this.BoxSpellsStats);
            this.TabStats.Controls.Add(this.BoxMonsterStats);
            this.TabStats.Location = new System.Drawing.Point(4, 22);
            this.TabStats.Name = "TabStats";
            this.TabStats.Padding = new System.Windows.Forms.Padding(3);
            this.TabStats.Size = new System.Drawing.Size(633, 454);
            this.TabStats.TabIndex = 1;
            this.TabStats.Text = "Stats";
            // 
            // BoxOtherStats
            // 
            this.BoxOtherStats.ForeColor = System.Drawing.Color.White;
            this.BoxOtherStats.Location = new System.Drawing.Point(313, 217);
            this.BoxOtherStats.Name = "BoxOtherStats";
            this.BoxOtherStats.Size = new System.Drawing.Size(303, 228);
            this.BoxOtherStats.TabIndex = 4;
            this.BoxOtherStats.TabStop = false;
            this.BoxOtherStats.Text = "Other Stats";
            // 
            // BoxTrapsStats
            // 
            this.BoxTrapsStats.ForeColor = System.Drawing.Color.White;
            this.BoxTrapsStats.Location = new System.Drawing.Point(313, 130);
            this.BoxTrapsStats.Name = "BoxTrapsStats";
            this.BoxTrapsStats.Size = new System.Drawing.Size(303, 88);
            this.BoxTrapsStats.TabIndex = 2;
            this.BoxTrapsStats.TabStop = false;
            this.BoxTrapsStats.Text = "Traps";
            // 
            // BoxSpellsStats
            // 
            this.BoxSpellsStats.ForeColor = System.Drawing.Color.White;
            this.BoxSpellsStats.Location = new System.Drawing.Point(313, -1);
            this.BoxSpellsStats.Name = "BoxSpellsStats";
            this.BoxSpellsStats.Size = new System.Drawing.Size(303, 132);
            this.BoxSpellsStats.TabIndex = 1;
            this.BoxSpellsStats.TabStop = false;
            this.BoxSpellsStats.Text = "Spells";
            // 
            // BoxMonsterStats
            // 
            this.BoxMonsterStats.Controls.Add(this.label5);
            this.BoxMonsterStats.ForeColor = System.Drawing.Color.White;
            this.BoxMonsterStats.Location = new System.Drawing.Point(4, -1);
            this.BoxMonsterStats.Name = "BoxMonsterStats";
            this.BoxMonsterStats.Size = new System.Drawing.Size(303, 446);
            this.BoxMonsterStats.TabIndex = 0;
            this.BoxMonsterStats.TabStop = false;
            this.BoxMonsterStats.Text = "Monsters";
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(13, 371);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(282, 1);
            this.label5.TabIndex = 0;
            // 
            // TabBoosterPacks
            // 
            this.TabBoosterPacks.AutoScroll = true;
            this.TabBoosterPacks.BackColor = System.Drawing.Color.Black;
            this.TabBoosterPacks.Location = new System.Drawing.Point(4, 22);
            this.TabBoosterPacks.Name = "TabBoosterPacks";
            this.TabBoosterPacks.Padding = new System.Windows.Forms.Padding(3);
            this.TabBoosterPacks.Size = new System.Drawing.Size(633, 454);
            this.TabBoosterPacks.TabIndex = 2;
            this.TabBoosterPacks.Text = "Booster";
            // 
            // TabSpecialEditonBoxes
            // 
            this.TabSpecialEditonBoxes.AutoScroll = true;
            this.TabSpecialEditonBoxes.BackColor = System.Drawing.Color.Black;
            this.TabSpecialEditonBoxes.Location = new System.Drawing.Point(4, 22);
            this.TabSpecialEditonBoxes.Name = "TabSpecialEditonBoxes";
            this.TabSpecialEditonBoxes.Padding = new System.Windows.Forms.Padding(3);
            this.TabSpecialEditonBoxes.Size = new System.Drawing.Size(633, 454);
            this.TabSpecialEditonBoxes.TabIndex = 3;
            this.TabSpecialEditonBoxes.Text = "SpEd Boxes";
            // 
            // TabStsarterDecks
            // 
            this.TabStsarterDecks.AutoScroll = true;
            this.TabStsarterDecks.BackColor = System.Drawing.Color.Black;
            this.TabStsarterDecks.Location = new System.Drawing.Point(4, 22);
            this.TabStsarterDecks.Name = "TabStsarterDecks";
            this.TabStsarterDecks.Padding = new System.Windows.Forms.Padding(3);
            this.TabStsarterDecks.Size = new System.Drawing.Size(633, 454);
            this.TabStsarterDecks.TabIndex = 4;
            this.TabStsarterDecks.Text = "StarterDKs";
            // 
            // TabStructureDecks
            // 
            this.TabStructureDecks.AutoScroll = true;
            this.TabStructureDecks.BackColor = System.Drawing.Color.Black;
            this.TabStructureDecks.Location = new System.Drawing.Point(4, 22);
            this.TabStructureDecks.Name = "TabStructureDecks";
            this.TabStructureDecks.Padding = new System.Windows.Forms.Padding(3);
            this.TabStructureDecks.Size = new System.Drawing.Size(633, 454);
            this.TabStructureDecks.TabIndex = 5;
            this.TabStructureDecks.Text = "StrucDKs";
            // 
            // TabTins
            // 
            this.TabTins.AutoScroll = true;
            this.TabTins.BackColor = System.Drawing.Color.Black;
            this.TabTins.Location = new System.Drawing.Point(4, 22);
            this.TabTins.Name = "TabTins";
            this.TabTins.Padding = new System.Windows.Forms.Padding(3);
            this.TabTins.Size = new System.Drawing.Size(633, 454);
            this.TabTins.TabIndex = 6;
            this.TabTins.Text = "Tins";
            // 
            // TabSPDuel
            // 
            this.TabSPDuel.AutoScroll = true;
            this.TabSPDuel.BackColor = System.Drawing.Color.Black;
            this.TabSPDuel.Location = new System.Drawing.Point(4, 22);
            this.TabSPDuel.Name = "TabSPDuel";
            this.TabSPDuel.Padding = new System.Windows.Forms.Padding(3);
            this.TabSPDuel.Size = new System.Drawing.Size(633, 454);
            this.TabSPDuel.TabIndex = 7;
            this.TabSPDuel.Text = "SPDuel";
            // 
            // TabDP
            // 
            this.TabDP.AutoScroll = true;
            this.TabDP.BackColor = System.Drawing.Color.Black;
            this.TabDP.Location = new System.Drawing.Point(4, 22);
            this.TabDP.Name = "TabDP";
            this.TabDP.Padding = new System.Windows.Forms.Padding(3);
            this.TabDP.Size = new System.Drawing.Size(633, 454);
            this.TabDP.TabIndex = 8;
            this.TabDP.Text = "DP";
            // 
            // TabDT
            // 
            this.TabDT.AutoScroll = true;
            this.TabDT.BackColor = System.Drawing.Color.Black;
            this.TabDT.Location = new System.Drawing.Point(4, 22);
            this.TabDT.Name = "TabDT";
            this.TabDT.Padding = new System.Windows.Forms.Padding(3);
            this.TabDT.Size = new System.Drawing.Size(633, 454);
            this.TabDT.TabIndex = 9;
            this.TabDT.Text = "DT";
            // 
            // TabOthers
            // 
            this.TabOthers.AutoScroll = true;
            this.TabOthers.BackColor = System.Drawing.Color.Black;
            this.TabOthers.Location = new System.Drawing.Point(4, 22);
            this.TabOthers.Name = "TabOthers";
            this.TabOthers.Padding = new System.Windows.Forms.Padding(3);
            this.TabOthers.Size = new System.Drawing.Size(633, 454);
            this.TabOthers.TabIndex = 10;
            this.TabOthers.Text = "Others";
            // 
            // TabMBC
            // 
            this.TabMBC.AutoScroll = true;
            this.TabMBC.BackColor = System.Drawing.Color.Black;
            this.TabMBC.Location = new System.Drawing.Point(4, 22);
            this.TabMBC.Name = "TabMBC";
            this.TabMBC.Padding = new System.Windows.Forms.Padding(3);
            this.TabMBC.Size = new System.Drawing.Size(633, 454);
            this.TabMBC.TabIndex = 11;
            this.TabMBC.Text = "M,B,C";
            // 
            // TabTournaments
            // 
            this.TabTournaments.AutoScroll = true;
            this.TabTournaments.BackColor = System.Drawing.Color.Black;
            this.TabTournaments.Location = new System.Drawing.Point(4, 22);
            this.TabTournaments.Name = "TabTournaments";
            this.TabTournaments.Padding = new System.Windows.Forms.Padding(3);
            this.TabTournaments.Size = new System.Drawing.Size(633, 454);
            this.TabTournaments.TabIndex = 12;
            this.TabTournaments.Text = "Tourns";
            // 
            // TabPromo
            // 
            this.TabPromo.AutoScroll = true;
            this.TabPromo.BackColor = System.Drawing.Color.Black;
            this.TabPromo.Location = new System.Drawing.Point(4, 22);
            this.TabPromo.Name = "TabPromo";
            this.TabPromo.Padding = new System.Windows.Forms.Padding(3);
            this.TabPromo.Size = new System.Drawing.Size(633, 454);
            this.TabPromo.TabIndex = 13;
            this.TabPromo.Text = "Promo";
            // 
            // TabVideoGames
            // 
            this.TabVideoGames.AutoScroll = true;
            this.TabVideoGames.BackColor = System.Drawing.Color.Black;
            this.TabVideoGames.Location = new System.Drawing.Point(4, 22);
            this.TabVideoGames.Name = "TabVideoGames";
            this.TabVideoGames.Padding = new System.Windows.Forms.Padding(3);
            this.TabVideoGames.Size = new System.Drawing.Size(633, 454);
            this.TabVideoGames.TabIndex = 14;
            this.TabVideoGames.Text = "VGs";
            // 
            // lblFilterTittle
            // 
            this.lblFilterTittle.AutoSize = true;
            this.lblFilterTittle.BackColor = System.Drawing.Color.Transparent;
            this.lblFilterTittle.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilterTittle.ForeColor = System.Drawing.Color.Red;
            this.lblFilterTittle.Location = new System.Drawing.Point(192, 10);
            this.lblFilterTittle.Name = "lblFilterTittle";
            this.lblFilterTittle.Size = new System.Drawing.Size(66, 20);
            this.lblFilterTittle.TabIndex = 200;
            this.lblFilterTittle.Text = "All Cards";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 23);
            this.label1.TabIndex = 199;
            this.label1.Text = "Card Collection Gallery:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnFilterZombie);
            this.groupBox1.Controls.Add(this.btnFilterWyrm);
            this.groupBox1.Controls.Add(this.btnFilterWingedBeast);
            this.groupBox1.Controls.Add(this.btnFilterWarrior);
            this.groupBox1.Controls.Add(this.btnFilterThunder);
            this.groupBox1.Controls.Add(this.btnFilterSpellcaster);
            this.groupBox1.Controls.Add(this.btnFilterPsychic);
            this.groupBox1.Controls.Add(this.btnFilterFiend);
            this.groupBox1.Controls.Add(this.btnFilterCyberce);
            this.groupBox1.Controls.Add(this.btnFilterSeaSerpent);
            this.groupBox1.Controls.Add(this.btnFilterPlant);
            this.groupBox1.Controls.Add(this.btnFilterFairy);
            this.groupBox1.Controls.Add(this.btnFilterRock);
            this.groupBox1.Controls.Add(this.btnFilterReptile);
            this.groupBox1.Controls.Add(this.btnFilterPyro);
            this.groupBox1.Controls.Add(this.btnFilterMachine);
            this.groupBox1.Controls.Add(this.btnFilterInsect);
            this.groupBox1.Controls.Add(this.btnFilterFish);
            this.groupBox1.Controls.Add(this.btnFilterDragon);
            this.groupBox1.Controls.Add(this.btnFilterDivine);
            this.groupBox1.Controls.Add(this.btnFilterDinosaur);
            this.groupBox1.Controls.Add(this.btnFilterBeastWarrior);
            this.groupBox1.Controls.Add(this.btnFilterBeast);
            this.groupBox1.Controls.Add(this.btnFilterAqua);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox1.Location = new System.Drawing.Point(6, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 109);
            this.groupBox1.TabIndex = 205;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter by Monster Type";
            // 
            // btnFilterZombie
            // 
            this.btnFilterZombie.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterZombie.BackgroundImage")));
            this.btnFilterZombie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterZombie.Location = new System.Drawing.Point(206, 72);
            this.btnFilterZombie.Name = "btnFilterZombie";
            this.btnFilterZombie.Size = new System.Drawing.Size(30, 30);
            this.btnFilterZombie.TabIndex = 24;
            this.btnFilterZombie.Tag = "23";
            this.btnFilterZombie.UseVisualStyleBackColor = true;
            this.btnFilterZombie.Click += new System.EventHandler(this.btnFilterZombie_Click);
            // 
            // btnFilterWyrm
            // 
            this.btnFilterWyrm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterWyrm.BackgroundImage")));
            this.btnFilterWyrm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterWyrm.Location = new System.Drawing.Point(177, 72);
            this.btnFilterWyrm.Name = "btnFilterWyrm";
            this.btnFilterWyrm.Size = new System.Drawing.Size(30, 30);
            this.btnFilterWyrm.TabIndex = 23;
            this.btnFilterWyrm.Tag = "22";
            this.btnFilterWyrm.UseVisualStyleBackColor = true;
            this.btnFilterWyrm.Click += new System.EventHandler(this.btnFilterWyrm_Click);
            // 
            // btnFilterWingedBeast
            // 
            this.btnFilterWingedBeast.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterWingedBeast.BackgroundImage")));
            this.btnFilterWingedBeast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterWingedBeast.Location = new System.Drawing.Point(148, 72);
            this.btnFilterWingedBeast.Name = "btnFilterWingedBeast";
            this.btnFilterWingedBeast.Size = new System.Drawing.Size(30, 30);
            this.btnFilterWingedBeast.TabIndex = 22;
            this.btnFilterWingedBeast.Tag = "21";
            this.btnFilterWingedBeast.UseVisualStyleBackColor = true;
            this.btnFilterWingedBeast.Click += new System.EventHandler(this.btnFilterWingedBeast_Click);
            // 
            // btnFilterWarrior
            // 
            this.btnFilterWarrior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterWarrior.BackgroundImage")));
            this.btnFilterWarrior.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterWarrior.Location = new System.Drawing.Point(119, 72);
            this.btnFilterWarrior.Name = "btnFilterWarrior";
            this.btnFilterWarrior.Size = new System.Drawing.Size(30, 30);
            this.btnFilterWarrior.TabIndex = 21;
            this.btnFilterWarrior.Tag = "20";
            this.btnFilterWarrior.UseVisualStyleBackColor = true;
            this.btnFilterWarrior.Click += new System.EventHandler(this.btnFilterWarrior_Click);
            // 
            // btnFilterThunder
            // 
            this.btnFilterThunder.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterThunder.BackgroundImage")));
            this.btnFilterThunder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterThunder.Location = new System.Drawing.Point(90, 72);
            this.btnFilterThunder.Name = "btnFilterThunder";
            this.btnFilterThunder.Size = new System.Drawing.Size(30, 30);
            this.btnFilterThunder.TabIndex = 20;
            this.btnFilterThunder.Tag = "19";
            this.btnFilterThunder.UseVisualStyleBackColor = true;
            this.btnFilterThunder.Click += new System.EventHandler(this.btnFilterThunder_Click);
            // 
            // btnFilterSpellcaster
            // 
            this.btnFilterSpellcaster.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterSpellcaster.BackgroundImage")));
            this.btnFilterSpellcaster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterSpellcaster.Location = new System.Drawing.Point(61, 72);
            this.btnFilterSpellcaster.Name = "btnFilterSpellcaster";
            this.btnFilterSpellcaster.Size = new System.Drawing.Size(30, 30);
            this.btnFilterSpellcaster.TabIndex = 19;
            this.btnFilterSpellcaster.Tag = "18";
            this.btnFilterSpellcaster.UseVisualStyleBackColor = true;
            this.btnFilterSpellcaster.Click += new System.EventHandler(this.btnFilterSpellcaster_Click);
            // 
            // btnFilterPsychic
            // 
            this.btnFilterPsychic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterPsychic.BackgroundImage")));
            this.btnFilterPsychic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterPsychic.Location = new System.Drawing.Point(148, 43);
            this.btnFilterPsychic.Name = "btnFilterPsychic";
            this.btnFilterPsychic.Size = new System.Drawing.Size(30, 30);
            this.btnFilterPsychic.TabIndex = 18;
            this.btnFilterPsychic.Tag = "13";
            this.btnFilterPsychic.UseVisualStyleBackColor = true;
            this.btnFilterPsychic.Click += new System.EventHandler(this.btnFilterPsychic_Click);
            // 
            // btnFilterFiend
            // 
            this.btnFilterFiend.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterFiend.BackgroundImage")));
            this.btnFilterFiend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterFiend.Location = new System.Drawing.Point(3, 43);
            this.btnFilterFiend.Name = "btnFilterFiend";
            this.btnFilterFiend.Size = new System.Drawing.Size(30, 30);
            this.btnFilterFiend.TabIndex = 17;
            this.btnFilterFiend.Tag = "8";
            this.btnFilterFiend.UseVisualStyleBackColor = true;
            this.btnFilterFiend.Click += new System.EventHandler(this.btnFilterFiend_Click);
            // 
            // btnFilterCyberce
            // 
            this.btnFilterCyberce.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterCyberce.BackgroundImage")));
            this.btnFilterCyberce.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterCyberce.Location = new System.Drawing.Point(90, 14);
            this.btnFilterCyberce.Name = "btnFilterCyberce";
            this.btnFilterCyberce.Size = new System.Drawing.Size(30, 30);
            this.btnFilterCyberce.TabIndex = 16;
            this.btnFilterCyberce.Tag = "3";
            this.btnFilterCyberce.UseVisualStyleBackColor = true;
            this.btnFilterCyberce.Click += new System.EventHandler(this.btnFilterCyberce_Click);
            // 
            // btnFilterSeaSerpent
            // 
            this.btnFilterSeaSerpent.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterSeaSerpent.BackgroundImage")));
            this.btnFilterSeaSerpent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterSeaSerpent.Location = new System.Drawing.Point(32, 72);
            this.btnFilterSeaSerpent.Name = "btnFilterSeaSerpent";
            this.btnFilterSeaSerpent.Size = new System.Drawing.Size(30, 30);
            this.btnFilterSeaSerpent.TabIndex = 15;
            this.btnFilterSeaSerpent.Tag = "17";
            this.btnFilterSeaSerpent.UseVisualStyleBackColor = true;
            this.btnFilterSeaSerpent.Click += new System.EventHandler(this.btnFilterSeaSerpent_Click);
            // 
            // btnFilterPlant
            // 
            this.btnFilterPlant.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterPlant.BackgroundImage")));
            this.btnFilterPlant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterPlant.Location = new System.Drawing.Point(119, 43);
            this.btnFilterPlant.Name = "btnFilterPlant";
            this.btnFilterPlant.Size = new System.Drawing.Size(30, 30);
            this.btnFilterPlant.TabIndex = 14;
            this.btnFilterPlant.Tag = "12";
            this.btnFilterPlant.UseVisualStyleBackColor = true;
            this.btnFilterPlant.Click += new System.EventHandler(this.btnFilterPlant_Click);
            // 
            // btnFilterFairy
            // 
            this.btnFilterFairy.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterFairy.BackgroundImage")));
            this.btnFilterFairy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterFairy.Location = new System.Drawing.Point(206, 14);
            this.btnFilterFairy.Name = "btnFilterFairy";
            this.btnFilterFairy.Size = new System.Drawing.Size(30, 30);
            this.btnFilterFairy.TabIndex = 13;
            this.btnFilterFairy.Tag = "7";
            this.btnFilterFairy.UseVisualStyleBackColor = true;
            this.btnFilterFairy.Click += new System.EventHandler(this.btnFilterFairy_Click);
            // 
            // btnFilterRock
            // 
            this.btnFilterRock.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterRock.BackgroundImage")));
            this.btnFilterRock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterRock.Location = new System.Drawing.Point(3, 72);
            this.btnFilterRock.Name = "btnFilterRock";
            this.btnFilterRock.Size = new System.Drawing.Size(30, 30);
            this.btnFilterRock.TabIndex = 11;
            this.btnFilterRock.Tag = "16";
            this.btnFilterRock.UseVisualStyleBackColor = true;
            this.btnFilterRock.Click += new System.EventHandler(this.btnFilterRock_Click);
            // 
            // btnFilterReptile
            // 
            this.btnFilterReptile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterReptile.BackgroundImage")));
            this.btnFilterReptile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterReptile.Location = new System.Drawing.Point(206, 43);
            this.btnFilterReptile.Name = "btnFilterReptile";
            this.btnFilterReptile.Size = new System.Drawing.Size(30, 30);
            this.btnFilterReptile.TabIndex = 10;
            this.btnFilterReptile.Tag = "15";
            this.btnFilterReptile.UseVisualStyleBackColor = true;
            this.btnFilterReptile.Click += new System.EventHandler(this.btnFilterReptile_Click);
            // 
            // btnFilterPyro
            // 
            this.btnFilterPyro.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterPyro.BackgroundImage")));
            this.btnFilterPyro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterPyro.Location = new System.Drawing.Point(177, 43);
            this.btnFilterPyro.Name = "btnFilterPyro";
            this.btnFilterPyro.Size = new System.Drawing.Size(30, 30);
            this.btnFilterPyro.TabIndex = 9;
            this.btnFilterPyro.Tag = "14";
            this.btnFilterPyro.UseVisualStyleBackColor = true;
            this.btnFilterPyro.Click += new System.EventHandler(this.btnFilterPyro_Click);
            // 
            // btnFilterMachine
            // 
            this.btnFilterMachine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterMachine.BackgroundImage")));
            this.btnFilterMachine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterMachine.Location = new System.Drawing.Point(90, 43);
            this.btnFilterMachine.Name = "btnFilterMachine";
            this.btnFilterMachine.Size = new System.Drawing.Size(30, 30);
            this.btnFilterMachine.TabIndex = 8;
            this.btnFilterMachine.Tag = "11";
            this.btnFilterMachine.UseVisualStyleBackColor = true;
            this.btnFilterMachine.Click += new System.EventHandler(this.btnFilterMachine_Click);
            // 
            // btnFilterInsect
            // 
            this.btnFilterInsect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterInsect.BackgroundImage")));
            this.btnFilterInsect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterInsect.Location = new System.Drawing.Point(61, 43);
            this.btnFilterInsect.Name = "btnFilterInsect";
            this.btnFilterInsect.Size = new System.Drawing.Size(30, 30);
            this.btnFilterInsect.TabIndex = 7;
            this.btnFilterInsect.Tag = "10";
            this.btnFilterInsect.UseVisualStyleBackColor = true;
            this.btnFilterInsect.Click += new System.EventHandler(this.btnFilterInsect_Click);
            // 
            // btnFilterFish
            // 
            this.btnFilterFish.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterFish.BackgroundImage")));
            this.btnFilterFish.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterFish.Location = new System.Drawing.Point(32, 43);
            this.btnFilterFish.Name = "btnFilterFish";
            this.btnFilterFish.Size = new System.Drawing.Size(30, 30);
            this.btnFilterFish.TabIndex = 6;
            this.btnFilterFish.Tag = "9";
            this.btnFilterFish.UseVisualStyleBackColor = true;
            this.btnFilterFish.Click += new System.EventHandler(this.btnFilterFish_Click);
            // 
            // btnFilterDragon
            // 
            this.btnFilterDragon.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterDragon.BackgroundImage")));
            this.btnFilterDragon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterDragon.Location = new System.Drawing.Point(177, 14);
            this.btnFilterDragon.Name = "btnFilterDragon";
            this.btnFilterDragon.Size = new System.Drawing.Size(30, 30);
            this.btnFilterDragon.TabIndex = 5;
            this.btnFilterDragon.Tag = "6";
            this.btnFilterDragon.UseVisualStyleBackColor = true;
            this.btnFilterDragon.Click += new System.EventHandler(this.btnFilterDragon_Click);
            // 
            // btnFilterDivine
            // 
            this.btnFilterDivine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterDivine.BackgroundImage")));
            this.btnFilterDivine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterDivine.Location = new System.Drawing.Point(148, 14);
            this.btnFilterDivine.Name = "btnFilterDivine";
            this.btnFilterDivine.Size = new System.Drawing.Size(30, 30);
            this.btnFilterDivine.TabIndex = 4;
            this.btnFilterDivine.Tag = "5";
            this.btnFilterDivine.UseVisualStyleBackColor = true;
            this.btnFilterDivine.Click += new System.EventHandler(this.btnFilterDivine_Click);
            // 
            // btnFilterDinosaur
            // 
            this.btnFilterDinosaur.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterDinosaur.BackgroundImage")));
            this.btnFilterDinosaur.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterDinosaur.Location = new System.Drawing.Point(119, 14);
            this.btnFilterDinosaur.Name = "btnFilterDinosaur";
            this.btnFilterDinosaur.Size = new System.Drawing.Size(30, 30);
            this.btnFilterDinosaur.TabIndex = 3;
            this.btnFilterDinosaur.Tag = "4";
            this.btnFilterDinosaur.UseVisualStyleBackColor = true;
            this.btnFilterDinosaur.Click += new System.EventHandler(this.btnFilterDinosaur_Click);
            // 
            // btnFilterBeastWarrior
            // 
            this.btnFilterBeastWarrior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterBeastWarrior.BackgroundImage")));
            this.btnFilterBeastWarrior.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterBeastWarrior.Location = new System.Drawing.Point(61, 14);
            this.btnFilterBeastWarrior.Name = "btnFilterBeastWarrior";
            this.btnFilterBeastWarrior.Size = new System.Drawing.Size(30, 30);
            this.btnFilterBeastWarrior.TabIndex = 2;
            this.btnFilterBeastWarrior.Tag = "2";
            this.btnFilterBeastWarrior.UseVisualStyleBackColor = true;
            this.btnFilterBeastWarrior.Click += new System.EventHandler(this.btnFilterBeastWarrior_Click);
            // 
            // btnFilterBeast
            // 
            this.btnFilterBeast.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterBeast.BackgroundImage")));
            this.btnFilterBeast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterBeast.Location = new System.Drawing.Point(32, 14);
            this.btnFilterBeast.Name = "btnFilterBeast";
            this.btnFilterBeast.Size = new System.Drawing.Size(30, 30);
            this.btnFilterBeast.TabIndex = 1;
            this.btnFilterBeast.Tag = "1";
            this.btnFilterBeast.UseVisualStyleBackColor = true;
            this.btnFilterBeast.Click += new System.EventHandler(this.btnFilterBeast_Click);
            // 
            // btnFilterAqua
            // 
            this.btnFilterAqua.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFilterAqua.BackgroundImage")));
            this.btnFilterAqua.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFilterAqua.Location = new System.Drawing.Point(3, 14);
            this.btnFilterAqua.Name = "btnFilterAqua";
            this.btnFilterAqua.Size = new System.Drawing.Size(30, 30);
            this.btnFilterAqua.TabIndex = 0;
            this.btnFilterAqua.Tag = "0";
            this.btnFilterAqua.UseVisualStyleBackColor = true;
            this.btnFilterAqua.Click += new System.EventHandler(this.btnFilterAqua_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(137, 75);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(103, 20);
            this.txtSearch.TabIndex = 224;
            // 
            // btnTextSearch
            // 
            this.btnTextSearch.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTextSearch.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTextSearch.ForeColor = System.Drawing.Color.White;
            this.btnTextSearch.Location = new System.Drawing.Point(136, 96);
            this.btnTextSearch.Name = "btnTextSearch";
            this.btnTextSearch.Size = new System.Drawing.Size(106, 22);
            this.btnTextSearch.TabIndex = 225;
            this.btnTextSearch.Text = "Search by Text";
            this.btnTextSearch.UseVisualStyleBackColor = false;
            this.btnTextSearch.Click += new System.EventHandler(this.btnTextSearch_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.btnToon);
            this.groupBox8.Controls.Add(this.btnTuner);
            this.groupBox8.Controls.Add(this.btnUnion);
            this.groupBox8.Controls.Add(this.btnGemini);
            this.groupBox8.Controls.Add(this.btnSpirit);
            this.groupBox8.Controls.Add(this.btnFlip);
            this.groupBox8.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox8.Location = new System.Drawing.Point(136, 1);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(106, 74);
            this.groupBox8.TabIndex = 210;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Sec Type";
            // 
            // btnToon
            // 
            this.btnToon.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnToon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToon.ForeColor = System.Drawing.Color.Black;
            this.btnToon.Location = new System.Drawing.Point(6, 32);
            this.btnToon.Name = "btnToon";
            this.btnToon.Size = new System.Drawing.Size(47, 20);
            this.btnToon.TabIndex = 8;
            this.btnToon.Tag = "47";
            this.btnToon.Text = "Toon";
            this.btnToon.UseVisualStyleBackColor = false;
            this.btnToon.Click += new System.EventHandler(this.btnToon_Click);
            // 
            // btnTuner
            // 
            this.btnTuner.BackColor = System.Drawing.Color.Lavender;
            this.btnTuner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTuner.ForeColor = System.Drawing.Color.Black;
            this.btnTuner.Location = new System.Drawing.Point(6, 51);
            this.btnTuner.Name = "btnTuner";
            this.btnTuner.Size = new System.Drawing.Size(47, 20);
            this.btnTuner.TabIndex = 7;
            this.btnTuner.Tag = "49";
            this.btnTuner.Text = "Tuner";
            this.btnTuner.UseVisualStyleBackColor = false;
            this.btnTuner.Click += new System.EventHandler(this.btnTuner_Click);
            // 
            // btnUnion
            // 
            this.btnUnion.BackColor = System.Drawing.Color.PaleGreen;
            this.btnUnion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUnion.ForeColor = System.Drawing.Color.Black;
            this.btnUnion.Location = new System.Drawing.Point(52, 32);
            this.btnUnion.Name = "btnUnion";
            this.btnUnion.Size = new System.Drawing.Size(47, 20);
            this.btnUnion.TabIndex = 6;
            this.btnUnion.Tag = "48";
            this.btnUnion.Text = "Union";
            this.btnUnion.UseVisualStyleBackColor = false;
            this.btnUnion.Click += new System.EventHandler(this.btnUnion_Click);
            // 
            // btnGemini
            // 
            this.btnGemini.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.btnGemini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGemini.ForeColor = System.Drawing.Color.Black;
            this.btnGemini.Location = new System.Drawing.Point(52, 51);
            this.btnGemini.Name = "btnGemini";
            this.btnGemini.Size = new System.Drawing.Size(47, 20);
            this.btnGemini.TabIndex = 4;
            this.btnGemini.Tag = "50";
            this.btnGemini.Text = "Gemini";
            this.btnGemini.UseVisualStyleBackColor = false;
            this.btnGemini.Click += new System.EventHandler(this.btnGemini_Click);
            // 
            // btnSpirit
            // 
            this.btnSpirit.BackColor = System.Drawing.Color.LightCoral;
            this.btnSpirit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSpirit.ForeColor = System.Drawing.Color.Black;
            this.btnSpirit.Location = new System.Drawing.Point(52, 13);
            this.btnSpirit.Name = "btnSpirit";
            this.btnSpirit.Size = new System.Drawing.Size(47, 20);
            this.btnSpirit.TabIndex = 2;
            this.btnSpirit.Tag = "46";
            this.btnSpirit.Text = "Spirit";
            this.btnSpirit.UseVisualStyleBackColor = false;
            this.btnSpirit.Click += new System.EventHandler(this.btnSpirit_Click);
            // 
            // btnFlip
            // 
            this.btnFlip.BackColor = System.Drawing.Color.SandyBrown;
            this.btnFlip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFlip.ForeColor = System.Drawing.Color.Black;
            this.btnFlip.Location = new System.Drawing.Point(6, 13);
            this.btnFlip.Name = "btnFlip";
            this.btnFlip.Size = new System.Drawing.Size(47, 20);
            this.btnFlip.TabIndex = 1;
            this.btnFlip.Tag = "45";
            this.btnFlip.Text = "Flip";
            this.btnFlip.UseVisualStyleBackColor = false;
            this.btnFlip.Click += new System.EventHandler(this.btnFlip_Click);
            // 
            // groupSpell
            // 
            this.groupSpell.BackColor = System.Drawing.Color.Transparent;
            this.groupSpell.Controls.Add(this.btnRitualSpell);
            this.groupSpell.Controls.Add(this.btnEquipSpell);
            this.groupSpell.Controls.Add(this.btnQuickPlaySpell);
            this.groupSpell.Controls.Add(this.btnFieldSpell);
            this.groupSpell.Controls.Add(this.btnContinousSpell);
            this.groupSpell.Controls.Add(this.btnNormalSpell);
            this.groupSpell.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupSpell.ForeColor = System.Drawing.Color.Yellow;
            this.groupSpell.Location = new System.Drawing.Point(263, -2);
            this.groupSpell.Name = "groupSpell";
            this.groupSpell.Size = new System.Drawing.Size(104, 77);
            this.groupSpell.TabIndex = 207;
            this.groupSpell.TabStop = false;
            this.groupSpell.Text = "Filter by Spell";
            // 
            // btnRitualSpell
            // 
            this.btnRitualSpell.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRitualSpell.BackgroundImage")));
            this.btnRitualSpell.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRitualSpell.Location = new System.Drawing.Point(66, 43);
            this.btnRitualSpell.Name = "btnRitualSpell";
            this.btnRitualSpell.Size = new System.Drawing.Size(30, 30);
            this.btnRitualSpell.TabIndex = 6;
            this.btnRitualSpell.Tag = "30";
            this.btnRitualSpell.UseVisualStyleBackColor = true;
            this.btnRitualSpell.Click += new System.EventHandler(this.btnRitualSpell_Click);
            // 
            // btnEquipSpell
            // 
            this.btnEquipSpell.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEquipSpell.BackgroundImage")));
            this.btnEquipSpell.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEquipSpell.Location = new System.Drawing.Point(66, 14);
            this.btnEquipSpell.Name = "btnEquipSpell";
            this.btnEquipSpell.Size = new System.Drawing.Size(30, 30);
            this.btnEquipSpell.TabIndex = 5;
            this.btnEquipSpell.Tag = "27";
            this.btnEquipSpell.UseVisualStyleBackColor = true;
            this.btnEquipSpell.Click += new System.EventHandler(this.btnEquipSpell_Click);
            // 
            // btnQuickPlaySpell
            // 
            this.btnQuickPlaySpell.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuickPlaySpell.BackgroundImage")));
            this.btnQuickPlaySpell.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQuickPlaySpell.Location = new System.Drawing.Point(37, 43);
            this.btnQuickPlaySpell.Name = "btnQuickPlaySpell";
            this.btnQuickPlaySpell.Size = new System.Drawing.Size(30, 30);
            this.btnQuickPlaySpell.TabIndex = 4;
            this.btnQuickPlaySpell.Tag = "29";
            this.btnQuickPlaySpell.UseVisualStyleBackColor = true;
            this.btnQuickPlaySpell.Click += new System.EventHandler(this.btnQuickPlaySpell_Click);
            // 
            // btnFieldSpell
            // 
            this.btnFieldSpell.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFieldSpell.BackgroundImage")));
            this.btnFieldSpell.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFieldSpell.Location = new System.Drawing.Point(8, 43);
            this.btnFieldSpell.Name = "btnFieldSpell";
            this.btnFieldSpell.Size = new System.Drawing.Size(30, 30);
            this.btnFieldSpell.TabIndex = 3;
            this.btnFieldSpell.Tag = "28";
            this.btnFieldSpell.UseVisualStyleBackColor = true;
            this.btnFieldSpell.Click += new System.EventHandler(this.btnFieldSpell_Click);
            // 
            // btnContinousSpell
            // 
            this.btnContinousSpell.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnContinousSpell.BackgroundImage")));
            this.btnContinousSpell.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnContinousSpell.Location = new System.Drawing.Point(37, 14);
            this.btnContinousSpell.Name = "btnContinousSpell";
            this.btnContinousSpell.Size = new System.Drawing.Size(30, 30);
            this.btnContinousSpell.TabIndex = 2;
            this.btnContinousSpell.Tag = "26";
            this.btnContinousSpell.UseVisualStyleBackColor = true;
            this.btnContinousSpell.Click += new System.EventHandler(this.btnContinousSpell_Click);
            // 
            // btnNormalSpell
            // 
            this.btnNormalSpell.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNormalSpell.BackgroundImage")));
            this.btnNormalSpell.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNormalSpell.Location = new System.Drawing.Point(8, 14);
            this.btnNormalSpell.Name = "btnNormalSpell";
            this.btnNormalSpell.Size = new System.Drawing.Size(30, 30);
            this.btnNormalSpell.TabIndex = 1;
            this.btnNormalSpell.Tag = "25";
            this.btnNormalSpell.UseVisualStyleBackColor = true;
            this.btnNormalSpell.Click += new System.EventHandler(this.btnNormalSpell_Click);
            // 
            // groupColor
            // 
            this.groupColor.BackColor = System.Drawing.Color.Transparent;
            this.groupColor.Controls.Add(this.btnEffect);
            this.groupColor.Controls.Add(this.btnLink);
            this.groupColor.Controls.Add(this.btnPendulum);
            this.groupColor.Controls.Add(this.btnXyz);
            this.groupColor.Controls.Add(this.btnSynchro);
            this.groupColor.Controls.Add(this.btnRitual);
            this.groupColor.Controls.Add(this.btnFusion);
            this.groupColor.Controls.Add(this.btnNormal);
            this.groupColor.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupColor.ForeColor = System.Drawing.Color.Yellow;
            this.groupColor.Location = new System.Drawing.Point(6, 55);
            this.groupColor.Name = "groupColor";
            this.groupColor.Size = new System.Drawing.Size(124, 59);
            this.groupColor.TabIndex = 209;
            this.groupColor.TabStop = false;
            this.groupColor.Text = "Color";
            // 
            // btnEffect
            // 
            this.btnEffect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnEffect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEffect.Location = new System.Drawing.Point(35, 14);
            this.btnEffect.Name = "btnEffect";
            this.btnEffect.Size = new System.Drawing.Size(27, 20);
            this.btnEffect.TabIndex = 8;
            this.btnEffect.Tag = "38";
            this.btnEffect.UseVisualStyleBackColor = false;
            this.btnEffect.Click += new System.EventHandler(this.btnEffect_Click);
            // 
            // btnLink
            // 
            this.btnLink.BackColor = System.Drawing.Color.Blue;
            this.btnLink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLink.Location = new System.Drawing.Point(89, 33);
            this.btnLink.Name = "btnLink";
            this.btnLink.Size = new System.Drawing.Size(27, 20);
            this.btnLink.TabIndex = 7;
            this.btnLink.Tag = "44";
            this.btnLink.UseVisualStyleBackColor = false;
            this.btnLink.Click += new System.EventHandler(this.btnLink_Click);
            // 
            // btnPendulum
            // 
            this.btnPendulum.BackColor = System.Drawing.Color.Green;
            this.btnPendulum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPendulum.Location = new System.Drawing.Point(62, 33);
            this.btnPendulum.Name = "btnPendulum";
            this.btnPendulum.Size = new System.Drawing.Size(27, 20);
            this.btnPendulum.TabIndex = 6;
            this.btnPendulum.Tag = "43";
            this.btnPendulum.UseVisualStyleBackColor = false;
            this.btnPendulum.Click += new System.EventHandler(this.btnPendulum_Click);
            // 
            // btnXyz
            // 
            this.btnXyz.BackColor = System.Drawing.Color.Black;
            this.btnXyz.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXyz.Location = new System.Drawing.Point(35, 33);
            this.btnXyz.Name = "btnXyz";
            this.btnXyz.Size = new System.Drawing.Size(27, 20);
            this.btnXyz.TabIndex = 5;
            this.btnXyz.Tag = "42";
            this.btnXyz.UseVisualStyleBackColor = false;
            this.btnXyz.Click += new System.EventHandler(this.btnXyz_Click);
            // 
            // btnSynchro
            // 
            this.btnSynchro.BackColor = System.Drawing.Color.White;
            this.btnSynchro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSynchro.Location = new System.Drawing.Point(8, 33);
            this.btnSynchro.Name = "btnSynchro";
            this.btnSynchro.Size = new System.Drawing.Size(27, 20);
            this.btnSynchro.TabIndex = 4;
            this.btnSynchro.Tag = "41";
            this.btnSynchro.UseVisualStyleBackColor = false;
            this.btnSynchro.Click += new System.EventHandler(this.btnSynchro_Click);
            // 
            // btnRitual
            // 
            this.btnRitual.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnRitual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRitual.Location = new System.Drawing.Point(89, 14);
            this.btnRitual.Name = "btnRitual";
            this.btnRitual.Size = new System.Drawing.Size(27, 20);
            this.btnRitual.TabIndex = 3;
            this.btnRitual.Tag = "40";
            this.btnRitual.UseVisualStyleBackColor = false;
            this.btnRitual.Click += new System.EventHandler(this.btnRitual_Click);
            // 
            // btnFusion
            // 
            this.btnFusion.BackColor = System.Drawing.Color.DarkOrchid;
            this.btnFusion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFusion.Location = new System.Drawing.Point(62, 14);
            this.btnFusion.Name = "btnFusion";
            this.btnFusion.Size = new System.Drawing.Size(27, 20);
            this.btnFusion.TabIndex = 2;
            this.btnFusion.Tag = "39";
            this.btnFusion.UseVisualStyleBackColor = false;
            this.btnFusion.Click += new System.EventHandler(this.btnFusion_Click);
            // 
            // btnNormal
            // 
            this.btnNormal.BackColor = System.Drawing.Color.Yellow;
            this.btnNormal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNormal.Location = new System.Drawing.Point(8, 14);
            this.btnNormal.Name = "btnNormal";
            this.btnNormal.Size = new System.Drawing.Size(27, 20);
            this.btnNormal.TabIndex = 1;
            this.btnNormal.Tag = "37";
            this.btnNormal.UseVisualStyleBackColor = false;
            this.btnNormal.Click += new System.EventHandler(this.btnNormal_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btnCounterTrap);
            this.groupBox2.Controls.Add(this.btnContinuosTrap);
            this.groupBox2.Controls.Add(this.btnNormalTrap);
            this.groupBox2.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox2.Location = new System.Drawing.Point(262, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(105, 47);
            this.groupBox2.TabIndex = 208;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filter by Trap";
            // 
            // btnCounterTrap
            // 
            this.btnCounterTrap.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCounterTrap.BackgroundImage")));
            this.btnCounterTrap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCounterTrap.Location = new System.Drawing.Point(66, 14);
            this.btnCounterTrap.Name = "btnCounterTrap";
            this.btnCounterTrap.Size = new System.Drawing.Size(30, 30);
            this.btnCounterTrap.TabIndex = 5;
            this.btnCounterTrap.Tag = "34";
            this.btnCounterTrap.UseVisualStyleBackColor = true;
            this.btnCounterTrap.Click += new System.EventHandler(this.btnCounterTrap_Click);
            // 
            // btnContinuosTrap
            // 
            this.btnContinuosTrap.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnContinuosTrap.BackgroundImage")));
            this.btnContinuosTrap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnContinuosTrap.Location = new System.Drawing.Point(37, 14);
            this.btnContinuosTrap.Name = "btnContinuosTrap";
            this.btnContinuosTrap.Size = new System.Drawing.Size(30, 30);
            this.btnContinuosTrap.TabIndex = 2;
            this.btnContinuosTrap.Tag = "33";
            this.btnContinuosTrap.UseVisualStyleBackColor = true;
            this.btnContinuosTrap.Click += new System.EventHandler(this.btnContinuosTrap_Click);
            // 
            // btnNormalTrap
            // 
            this.btnNormalTrap.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNormalTrap.BackgroundImage")));
            this.btnNormalTrap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNormalTrap.Location = new System.Drawing.Point(8, 14);
            this.btnNormalTrap.Name = "btnNormalTrap";
            this.btnNormalTrap.Size = new System.Drawing.Size(30, 30);
            this.btnNormalTrap.TabIndex = 1;
            this.btnNormalTrap.Tag = "32";
            this.btnNormalTrap.UseVisualStyleBackColor = true;
            this.btnNormalTrap.Click += new System.EventHandler(this.btnNormalTrap_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.btnTraps);
            this.groupBox9.Controls.Add(this.btnSpells);
            this.groupBox9.Controls.Add(this.btnMonster);
            this.groupBox9.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox9.Location = new System.Drawing.Point(6, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(124, 41);
            this.groupBox9.TabIndex = 206;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Card Type";
            // 
            // btnTraps
            // 
            this.btnTraps.BackColor = System.Drawing.Color.Magenta;
            this.btnTraps.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTraps.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraps.ForeColor = System.Drawing.Color.Black;
            this.btnTraps.Location = new System.Drawing.Point(78, 14);
            this.btnTraps.Name = "btnTraps";
            this.btnTraps.Size = new System.Drawing.Size(35, 25);
            this.btnTraps.TabIndex = 5;
            this.btnTraps.Tag = "35";
            this.btnTraps.Text = "T";
            this.btnTraps.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTraps.UseVisualStyleBackColor = false;
            this.btnTraps.Click += new System.EventHandler(this.btnTraps_Click);
            // 
            // btnSpells
            // 
            this.btnSpells.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnSpells.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSpells.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpells.ForeColor = System.Drawing.Color.Black;
            this.btnSpells.Location = new System.Drawing.Point(44, 14);
            this.btnSpells.Name = "btnSpells";
            this.btnSpells.Size = new System.Drawing.Size(35, 25);
            this.btnSpells.TabIndex = 2;
            this.btnSpells.Tag = "31";
            this.btnSpells.Text = "S";
            this.btnSpells.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSpells.UseVisualStyleBackColor = false;
            this.btnSpells.Click += new System.EventHandler(this.btnSpells_Click);
            // 
            // btnMonster
            // 
            this.btnMonster.BackColor = System.Drawing.Color.Yellow;
            this.btnMonster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMonster.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonster.ForeColor = System.Drawing.Color.Black;
            this.btnMonster.Location = new System.Drawing.Point(10, 14);
            this.btnMonster.Name = "btnMonster";
            this.btnMonster.Size = new System.Drawing.Size(35, 25);
            this.btnMonster.TabIndex = 1;
            this.btnMonster.Tag = "24";
            this.btnMonster.Text = "M";
            this.btnMonster.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMonster.UseVisualStyleBackColor = false;
            this.btnMonster.Click += new System.EventHandler(this.btnMonster_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(856, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 20);
            this.label3.TabIndex = 228;
            this.label3.Text = "Set Codes:";
            // 
            // PanelTagList
            // 
            this.PanelTagList.AutoScroll = true;
            this.PanelTagList.BackColor = System.Drawing.Color.Black;
            this.PanelTagList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTagList.Location = new System.Drawing.Point(859, 27);
            this.PanelTagList.Name = "PanelTagList";
            this.PanelTagList.Size = new System.Drawing.Size(115, 292);
            this.PanelTagList.TabIndex = 230;
            // 
            // PicImage
            // 
            this.PicImage.BackColor = System.Drawing.Color.Transparent;
            this.PicImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicImage.Image = ((System.Drawing.Image)(resources.GetObject("PicImage.Image")));
            this.PicImage.Location = new System.Drawing.Point(653, 26);
            this.PicImage.Name = "PicImage";
            this.PicImage.Size = new System.Drawing.Size(200, 293);
            this.PicImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicImage.TabIndex = 229;
            this.PicImage.TabStop = false;
            // 
            // lblTCGName
            // 
            this.lblTCGName.AutoSize = true;
            this.lblTCGName.BackColor = System.Drawing.Color.Transparent;
            this.lblTCGName.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTCGName.ForeColor = System.Drawing.Color.White;
            this.lblTCGName.Location = new System.Drawing.Point(646, 321);
            this.lblTCGName.Name = "lblTCGName";
            this.lblTCGName.Size = new System.Drawing.Size(0, 16);
            this.lblTCGName.TabIndex = 231;
            // 
            // lblRariryLabel
            // 
            this.lblRariryLabel.AutoSize = true;
            this.lblRariryLabel.BackColor = System.Drawing.Color.Transparent;
            this.lblRariryLabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRariryLabel.ForeColor = System.Drawing.Color.White;
            this.lblRariryLabel.Location = new System.Drawing.Point(785, 353);
            this.lblRariryLabel.Name = "lblRariryLabel";
            this.lblRariryLabel.Size = new System.Drawing.Size(46, 16);
            this.lblRariryLabel.TabIndex = 232;
            this.lblRariryLabel.Text = "Rarity: ";
            this.lblRariryLabel.Visible = false;
            // 
            // btnNextPage
            // 
            this.btnNextPage.BackColor = System.Drawing.Color.Green;
            this.btnNextPage.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNextPage.ForeColor = System.Drawing.Color.White;
            this.btnNextPage.Location = new System.Drawing.Point(535, 514);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.Size = new System.Drawing.Size(110, 25);
            this.btnNextPage.TabIndex = 234;
            this.btnNextPage.Text = " Next Page >>";
            this.btnNextPage.UseVisualStyleBackColor = false;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // btnPreviousPage
            // 
            this.btnPreviousPage.BackColor = System.Drawing.Color.Green;
            this.btnPreviousPage.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreviousPage.ForeColor = System.Drawing.Color.White;
            this.btnPreviousPage.Location = new System.Drawing.Point(6, 514);
            this.btnPreviousPage.Name = "btnPreviousPage";
            this.btnPreviousPage.Size = new System.Drawing.Size(110, 25);
            this.btnPreviousPage.TabIndex = 233;
            this.btnPreviousPage.Text = "<< Previous Page";
            this.btnPreviousPage.UseVisualStyleBackColor = false;
            this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.Red;
            this.btnClear.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(308, 514);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(94, 25);
            this.btnClear.TabIndex = 236;
            this.btnClear.Text = "Clear Filter";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Visible = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chkCollected
            // 
            this.chkCollected.BackColor = System.Drawing.Color.Black;
            this.chkCollected.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCollected.ForeColor = System.Drawing.Color.White;
            this.chkCollected.Location = new System.Drawing.Point(177, 516);
            this.chkCollected.Name = "chkCollected";
            this.chkCollected.Size = new System.Drawing.Size(125, 22);
            this.chkCollected.TabIndex = 235;
            this.chkCollected.Text = "Your Collection";
            this.chkCollected.UseVisualStyleBackColor = false;
            this.chkCollected.CheckedChanged += new System.EventHandler(this.chkCollected_CheckedChanged);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.MonsterType);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(8, 540);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(406, 146);
            this.tabControl2.TabIndex = 237;
            // 
            // MonsterType
            // 
            this.MonsterType.BackColor = System.Drawing.Color.Purple;
            this.MonsterType.Controls.Add(this.groupBox1);
            this.MonsterType.Controls.Add(this.groupSpell);
            this.MonsterType.Controls.Add(this.groupBox2);
            this.MonsterType.ForeColor = System.Drawing.Color.White;
            this.MonsterType.Location = new System.Drawing.Point(4, 22);
            this.MonsterType.Name = "MonsterType";
            this.MonsterType.Padding = new System.Windows.Forms.Padding(3);
            this.MonsterType.Size = new System.Drawing.Size(398, 120);
            this.MonsterType.TabIndex = 0;
            this.MonsterType.Text = "Main Card Filters";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Purple;
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.txtSearch);
            this.tabPage2.Controls.Add(this.btnTextSearch);
            this.tabPage2.Controls.Add(this.groupColor);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(398, 120);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Other Filters";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.btnDivine);
            this.groupBox3.Controls.Add(this.btnWind);
            this.groupBox3.Controls.Add(this.btnFire);
            this.groupBox3.Controls.Add(this.btnWater);
            this.groupBox3.Controls.Add(this.btnEarth);
            this.groupBox3.Controls.Add(this.btnLight);
            this.groupBox3.Controls.Add(this.btnDark);
            this.groupBox3.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox3.Location = new System.Drawing.Point(248, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(128, 83);
            this.groupBox3.TabIndex = 226;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Card Attribute";
            // 
            // btnDivine
            // 
            this.btnDivine.BackColor = System.Drawing.Color.White;
            this.btnDivine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDivine.BackgroundImage")));
            this.btnDivine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDivine.Location = new System.Drawing.Point(76, 46);
            this.btnDivine.Name = "btnDivine";
            this.btnDivine.Size = new System.Drawing.Size(30, 30);
            this.btnDivine.TabIndex = 8;
            this.btnDivine.Tag = "25";
            this.btnDivine.UseVisualStyleBackColor = false;
            this.btnDivine.Click += new System.EventHandler(this.btnDivine_Click);
            // 
            // btnWind
            // 
            this.btnWind.BackColor = System.Drawing.Color.White;
            this.btnWind.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnWind.BackgroundImage")));
            this.btnWind.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnWind.Location = new System.Drawing.Point(47, 46);
            this.btnWind.Name = "btnWind";
            this.btnWind.Size = new System.Drawing.Size(30, 30);
            this.btnWind.TabIndex = 7;
            this.btnWind.Tag = "25";
            this.btnWind.UseVisualStyleBackColor = false;
            this.btnWind.Click += new System.EventHandler(this.btnWind_Click);
            // 
            // btnFire
            // 
            this.btnFire.BackColor = System.Drawing.Color.White;
            this.btnFire.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFire.BackgroundImage")));
            this.btnFire.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFire.Location = new System.Drawing.Point(17, 46);
            this.btnFire.Name = "btnFire";
            this.btnFire.Size = new System.Drawing.Size(30, 30);
            this.btnFire.TabIndex = 6;
            this.btnFire.Tag = "25";
            this.btnFire.UseVisualStyleBackColor = false;
            this.btnFire.Click += new System.EventHandler(this.btnFire_Click);
            // 
            // btnWater
            // 
            this.btnWater.BackColor = System.Drawing.Color.White;
            this.btnWater.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnWater.BackgroundImage")));
            this.btnWater.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnWater.Location = new System.Drawing.Point(94, 17);
            this.btnWater.Name = "btnWater";
            this.btnWater.Size = new System.Drawing.Size(30, 30);
            this.btnWater.TabIndex = 5;
            this.btnWater.Tag = "25";
            this.btnWater.UseVisualStyleBackColor = false;
            this.btnWater.Click += new System.EventHandler(this.btnWater_Click);
            // 
            // btnEarth
            // 
            this.btnEarth.BackColor = System.Drawing.Color.White;
            this.btnEarth.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEarth.BackgroundImage")));
            this.btnEarth.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEarth.Location = new System.Drawing.Point(64, 17);
            this.btnEarth.Name = "btnEarth";
            this.btnEarth.Size = new System.Drawing.Size(30, 30);
            this.btnEarth.TabIndex = 4;
            this.btnEarth.Tag = "25";
            this.btnEarth.UseVisualStyleBackColor = false;
            this.btnEarth.Click += new System.EventHandler(this.btnEarth_Click);
            // 
            // btnLight
            // 
            this.btnLight.BackColor = System.Drawing.Color.White;
            this.btnLight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLight.BackgroundImage")));
            this.btnLight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLight.Location = new System.Drawing.Point(34, 17);
            this.btnLight.Name = "btnLight";
            this.btnLight.Size = new System.Drawing.Size(30, 30);
            this.btnLight.TabIndex = 3;
            this.btnLight.Tag = "25";
            this.btnLight.UseVisualStyleBackColor = false;
            this.btnLight.Click += new System.EventHandler(this.btnLight_Click);
            // 
            // btnDark
            // 
            this.btnDark.BackColor = System.Drawing.Color.White;
            this.btnDark.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDark.BackgroundImage")));
            this.btnDark.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDark.Location = new System.Drawing.Point(4, 17);
            this.btnDark.Name = "btnDark";
            this.btnDark.Size = new System.Drawing.Size(30, 30);
            this.btnDark.TabIndex = 2;
            this.btnDark.Tag = "25";
            this.btnDark.UseVisualStyleBackColor = false;
            this.btnDark.Click += new System.EventHandler(this.btnDark_Click);
            // 
            // lblRarity
            // 
            this.lblRarity.AutoSize = true;
            this.lblRarity.BackColor = System.Drawing.Color.Transparent;
            this.lblRarity.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRarity.ForeColor = System.Drawing.Color.Plum;
            this.lblRarity.Location = new System.Drawing.Point(829, 353);
            this.lblRarity.Name = "lblRarity";
            this.lblRarity.Size = new System.Drawing.Size(111, 16);
            this.lblRarity.TabIndex = 239;
            this.lblRarity.Text = "Prismatic Gold Rare";
            this.lblRarity.Visible = false;
            // 
            // lblMarketPrice
            // 
            this.lblMarketPrice.AutoSize = true;
            this.lblMarketPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblMarketPrice.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarketPrice.ForeColor = System.Drawing.Color.White;
            this.lblMarketPrice.Location = new System.Drawing.Point(726, 337);
            this.lblMarketPrice.Name = "lblMarketPrice";
            this.lblMarketPrice.Size = new System.Drawing.Size(53, 16);
            this.lblMarketPrice.TabIndex = 241;
            this.lblMarketPrice.Text = "99999.99";
            this.lblMarketPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMarketPrice.Visible = false;
            // 
            // lblMarketPricelabel
            // 
            this.lblMarketPricelabel.AutoSize = true;
            this.lblMarketPricelabel.BackColor = System.Drawing.Color.Transparent;
            this.lblMarketPricelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarketPricelabel.ForeColor = System.Drawing.Color.White;
            this.lblMarketPricelabel.Location = new System.Drawing.Point(651, 337);
            this.lblMarketPricelabel.Name = "lblMarketPricelabel";
            this.lblMarketPricelabel.Size = new System.Drawing.Size(76, 16);
            this.lblMarketPricelabel.TabIndex = 240;
            this.lblMarketPricelabel.Text = "Market Price:";
            this.lblMarketPricelabel.Visible = false;
            // 
            // lblMedianPrice
            // 
            this.lblMedianPrice.AutoSize = true;
            this.lblMedianPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblMedianPrice.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedianPrice.ForeColor = System.Drawing.Color.White;
            this.lblMedianPrice.Location = new System.Drawing.Point(726, 353);
            this.lblMedianPrice.Name = "lblMedianPrice";
            this.lblMedianPrice.Size = new System.Drawing.Size(53, 16);
            this.lblMedianPrice.TabIndex = 243;
            this.lblMedianPrice.Text = "99999.99";
            this.lblMedianPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMedianPrice.Visible = false;
            // 
            // lblMedianPricelabel
            // 
            this.lblMedianPricelabel.AutoSize = true;
            this.lblMedianPricelabel.BackColor = System.Drawing.Color.Transparent;
            this.lblMedianPricelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedianPricelabel.ForeColor = System.Drawing.Color.White;
            this.lblMedianPricelabel.Location = new System.Drawing.Point(651, 353);
            this.lblMedianPricelabel.Name = "lblMedianPricelabel";
            this.lblMedianPricelabel.Size = new System.Drawing.Size(79, 16);
            this.lblMedianPricelabel.TabIndex = 242;
            this.lblMedianPricelabel.Text = "Median Price:";
            this.lblMedianPricelabel.Visible = false;
            // 
            // listSetGroups
            // 
            this.listSetGroups.BackColor = System.Drawing.Color.Black;
            this.listSetGroups.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listSetGroups.ForeColor = System.Drawing.Color.White;
            this.listSetGroups.FormattingEnabled = true;
            this.listSetGroups.ItemHeight = 15;
            this.listSetGroups.Items.AddRange(new object[] {
            "Booster Packs",
            "Special Edition Boxes",
            "Starter Decks",
            "Structure Decks",
            "Tins",
            "SPEED DUEL",
            "Duelist Packs",
            "Duel Terminal Cards",
            "Others",
            "Magazines, Books, Comics",
            "Tournaments",
            "Promotional Cards",
            "Video Game Bundles"});
            this.listSetGroups.Location = new System.Drawing.Point(457, 556);
            this.listSetGroups.Name = "listSetGroups";
            this.listSetGroups.Size = new System.Drawing.Size(147, 79);
            this.listSetGroups.TabIndex = 244;
            this.listSetGroups.SelectedIndexChanged += new System.EventHandler(this.listSetGroups_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(455, 538);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 16);
            this.label2.TabIndex = 245;
            this.label2.Text = "Select Set Group";
            // 
            // listSetlist
            // 
            this.listSetlist.BackColor = System.Drawing.Color.Black;
            this.listSetlist.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listSetlist.ForeColor = System.Drawing.Color.White;
            this.listSetlist.FormattingEnabled = true;
            this.listSetlist.ItemHeight = 15;
            this.listSetlist.Location = new System.Drawing.Point(653, 411);
            this.listSetlist.Name = "listSetlist";
            this.listSetlist.Size = new System.Drawing.Size(320, 274);
            this.listSetlist.TabIndex = 246;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(653, 389);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 20);
            this.label4.TabIndex = 247;
            this.label4.Text = "Select Set";
            // 
            // btnFilterSet
            // 
            this.btnFilterSet.BackColor = System.Drawing.Color.Green;
            this.btnFilterSet.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFilterSet.ForeColor = System.Drawing.Color.White;
            this.btnFilterSet.Location = new System.Drawing.Point(882, 385);
            this.btnFilterSet.Name = "btnFilterSet";
            this.btnFilterSet.Size = new System.Drawing.Size(90, 25);
            this.btnFilterSet.TabIndex = 248;
            this.btnFilterSet.Text = "Show Set";
            this.btnFilterSet.UseVisualStyleBackColor = false;
            this.btnFilterSet.Click += new System.EventHandler(this.btnFilterSet_Click);
            // 
            // btnMissingCards
            // 
            this.btnMissingCards.BackColor = System.Drawing.Color.Green;
            this.btnMissingCards.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMissingCards.ForeColor = System.Drawing.Color.White;
            this.btnMissingCards.Location = new System.Drawing.Point(405, 514);
            this.btnMissingCards.Name = "btnMissingCards";
            this.btnMissingCards.Size = new System.Drawing.Size(130, 25);
            this.btnMissingCards.TabIndex = 249;
            this.btnMissingCards.Text = "Missing Card Images URLs";
            this.btnMissingCards.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMissingCards.UseVisualStyleBackColor = false;
            this.btnMissingCards.Visible = false;
            this.btnMissingCards.Click += new System.EventHandler(this.btnMissingCards_Click);
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.BackColor = System.Drawing.Color.Transparent;
            this.lblCode.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCode.ForeColor = System.Drawing.Color.White;
            this.lblCode.Location = new System.Drawing.Point(829, 337);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(74, 16);
            this.lblCode.TabIndex = 251;
            this.lblCode.Text = "LOTD-EN999";
            this.lblCode.Visible = false;
            // 
            // lblCodelabel
            // 
            this.lblCodelabel.AutoSize = true;
            this.lblCodelabel.BackColor = System.Drawing.Color.Transparent;
            this.lblCodelabel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodelabel.ForeColor = System.Drawing.Color.White;
            this.lblCodelabel.Location = new System.Drawing.Point(785, 337);
            this.lblCodelabel.Name = "lblCodelabel";
            this.lblCodelabel.Size = new System.Drawing.Size(40, 16);
            this.lblCodelabel.TabIndex = 250;
            this.lblCodelabel.Text = "Code:";
            this.lblCodelabel.Visible = false;
            // 
            // btnValueReport
            // 
            this.btnValueReport.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnValueReport.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValueReport.ForeColor = System.Drawing.Color.Black;
            this.btnValueReport.Location = new System.Drawing.Point(444, 664);
            this.btnValueReport.Name = "btnValueReport";
            this.btnValueReport.Size = new System.Drawing.Size(169, 25);
            this.btnValueReport.TabIndex = 252;
            this.btnValueReport.Text = "Run Collection Value Report";
            this.btnValueReport.UseVisualStyleBackColor = false;
            this.btnValueReport.Click += new System.EventHandler(this.btnValueReport_Click);
            // 
            // btnSetPriceReport
            // 
            this.btnSetPriceReport.BackColor = System.Drawing.Color.Green;
            this.btnSetPriceReport.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetPriceReport.ForeColor = System.Drawing.Color.White;
            this.btnSetPriceReport.Location = new System.Drawing.Point(741, 385);
            this.btnSetPriceReport.Name = "btnSetPriceReport";
            this.btnSetPriceReport.Size = new System.Drawing.Size(135, 25);
            this.btnSetPriceReport.TabIndex = 257;
            this.btnSetPriceReport.Text = "Open Set Pricing Report";
            this.btnSetPriceReport.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSetPriceReport.UseVisualStyleBackColor = false;
            this.btnSetPriceReport.Visible = false;
            this.btnSetPriceReport.Click += new System.EventHandler(this.btnSetPriceReport_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(444, 637);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 25);
            this.button1.TabIndex = 260;
            this.button1.Text = "Set Search Tool";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Collector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(980, 690);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSetPriceReport);
            this.Controls.Add(this.btnValueReport);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.lblCodelabel);
            this.Controls.Add(this.btnMissingCards);
            this.Controls.Add(this.btnFilterSet);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listSetlist);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listSetGroups);
            this.Controls.Add(this.lblMedianPrice);
            this.Controls.Add(this.lblMedianPricelabel);
            this.Controls.Add(this.lblMarketPrice);
            this.Controls.Add(this.lblMarketPricelabel);
            this.Controls.Add(this.lblRarity);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.chkCollected);
            this.Controls.Add(this.btnNextPage);
            this.Controls.Add(this.btnPreviousPage);
            this.Controls.Add(this.lblRariryLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PanelTagList);
            this.Controls.Add(this.PicImage);
            this.Controls.Add(this.lblTCGName);
            this.Controls.Add(this.lblFilterTittle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Collector";
            this.Text = "Yu-Gi-Oh Card Collector - Complete Edition";
            this.tabControl1.ResumeLayout(false);
            this.TabCardView.ResumeLayout(false);
            this.TabStats.ResumeLayout(false);
            this.BoxMonsterStats.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupSpell.ResumeLayout(false);
            this.groupColor.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicImage)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.MonsterType.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TabCardView;
        private System.Windows.Forms.GroupBox GroupCardView;
        private System.Windows.Forms.TabPage TabStats;
        private System.Windows.Forms.GroupBox BoxOtherStats;
        private System.Windows.Forms.GroupBox BoxTrapsStats;
        private System.Windows.Forms.GroupBox BoxSpellsStats;
        private System.Windows.Forms.GroupBox BoxMonsterStats;
        private System.Windows.Forms.TabPage TabBoosterPacks;
        private System.Windows.Forms.TabPage TabSpecialEditonBoxes;
        private System.Windows.Forms.TabPage TabStsarterDecks;
        private System.Windows.Forms.TabPage TabStructureDecks;
        private System.Windows.Forms.TabPage TabTins;
        private System.Windows.Forms.TabPage TabSPDuel;
        private System.Windows.Forms.TabPage TabDP;
        private System.Windows.Forms.TabPage TabDT;
        private System.Windows.Forms.TabPage TabOthers;
        private System.Windows.Forms.Label lblFilterTittle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnFilterZombie;
        private System.Windows.Forms.Button btnFilterWyrm;
        private System.Windows.Forms.Button btnFilterWingedBeast;
        private System.Windows.Forms.Button btnFilterWarrior;
        private System.Windows.Forms.Button btnFilterThunder;
        private System.Windows.Forms.Button btnFilterSpellcaster;
        private System.Windows.Forms.Button btnFilterPsychic;
        private System.Windows.Forms.Button btnFilterFiend;
        private System.Windows.Forms.Button btnFilterCyberce;
        private System.Windows.Forms.Button btnFilterSeaSerpent;
        private System.Windows.Forms.Button btnFilterPlant;
        private System.Windows.Forms.Button btnFilterFairy;
        private System.Windows.Forms.Button btnFilterRock;
        private System.Windows.Forms.Button btnFilterReptile;
        private System.Windows.Forms.Button btnFilterPyro;
        private System.Windows.Forms.Button btnFilterMachine;
        private System.Windows.Forms.Button btnFilterInsect;
        private System.Windows.Forms.Button btnFilterFish;
        private System.Windows.Forms.Button btnFilterDragon;
        private System.Windows.Forms.Button btnFilterDivine;
        private System.Windows.Forms.Button btnFilterDinosaur;
        private System.Windows.Forms.Button btnFilterBeastWarrior;
        private System.Windows.Forms.Button btnFilterBeast;
        private System.Windows.Forms.Button btnFilterAqua;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnTextSearch;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnToon;
        private System.Windows.Forms.Button btnTuner;
        private System.Windows.Forms.Button btnUnion;
        private System.Windows.Forms.Button btnGemini;
        private System.Windows.Forms.Button btnSpirit;
        private System.Windows.Forms.Button btnFlip;
        private System.Windows.Forms.GroupBox groupSpell;
        private System.Windows.Forms.Button btnRitualSpell;
        private System.Windows.Forms.Button btnEquipSpell;
        private System.Windows.Forms.Button btnQuickPlaySpell;
        private System.Windows.Forms.Button btnFieldSpell;
        private System.Windows.Forms.Button btnContinousSpell;
        private System.Windows.Forms.Button btnNormalSpell;
        private System.Windows.Forms.GroupBox groupColor;
        private System.Windows.Forms.Button btnEffect;
        private System.Windows.Forms.Button btnLink;
        private System.Windows.Forms.Button btnPendulum;
        private System.Windows.Forms.Button btnXyz;
        private System.Windows.Forms.Button btnSynchro;
        private System.Windows.Forms.Button btnRitual;
        private System.Windows.Forms.Button btnFusion;
        private System.Windows.Forms.Button btnNormal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCounterTrap;
        private System.Windows.Forms.Button btnContinuosTrap;
        private System.Windows.Forms.Button btnNormalTrap;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btnTraps;
        private System.Windows.Forms.Button btnSpells;
        private System.Windows.Forms.Button btnMonster;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel PanelTagList;
        private System.Windows.Forms.PictureBox PicImage;
        private System.Windows.Forms.Label lblTCGName;
        private System.Windows.Forms.Label lblRariryLabel;
        private System.Windows.Forms.Button btnNextPage;
        private System.Windows.Forms.Button btnPreviousPage;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.CheckBox chkCollected;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage MonsterType;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblRarity;
        private System.Windows.Forms.Label lblMarketPrice;
        private System.Windows.Forms.Label lblMarketPricelabel;
        private System.Windows.Forms.Label lblMedianPrice;
        private System.Windows.Forms.Label lblMedianPricelabel;
        private System.Windows.Forms.ListBox listSetGroups;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listSetlist;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFilterSet;
        private System.Windows.Forms.Button btnMissingCards;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblCodelabel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDivine;
        private System.Windows.Forms.Button btnWind;
        private System.Windows.Forms.Button btnFire;
        private System.Windows.Forms.Button btnWater;
        private System.Windows.Forms.Button btnEarth;
        private System.Windows.Forms.Button btnLight;
        private System.Windows.Forms.Button btnDark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage TabMBC;
        private System.Windows.Forms.TabPage TabTournaments;
        private System.Windows.Forms.TabPage TabPromo;
        private System.Windows.Forms.TabPage TabVideoGames;
        private System.Windows.Forms.Button btnValueReport;
        private System.Windows.Forms.Button btnSetPriceReport;
        private System.Windows.Forms.Button button1;
    }
}

