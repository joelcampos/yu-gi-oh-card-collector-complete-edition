﻿//Joel Campos
//12/10/2021
//Collector Form Class

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public partial class Collector : Form
    {
        #region Constructors
        public Collector(StartScreen startForm)
        {
            InitializeComponent();
            startForm.UpdateLoading(10);
            DataBase.LoadDataBase();
            startForm.UpdateLoading(20);
            DataBase.LoadSetsNames();
            startForm.UpdateLoading(30);
            DataBase.LoadSets();
            startForm.UpdateLoading(40);
            DataBase.SortAllSetPacksContents();
            startForm.UpdateLoading(50);
            SaveDataManager.ReadSaveFile();
            startForm.UpdateLoading(60);
            InitializeCardView();
            startForm.UpdateLoading(70);
            InitializeMasterStatsComponents();
            startForm.UpdateLoading(80);
            InitializeSetStatsComponents();
            startForm.UpdateLoading(90);

            listSetGroups.SetSelected(0, true);

            //Save the url of this card is missing an image
            for(int x = 0; x < DataBase.MainList.Count; x++)
            {
                string cardname = DataBase.MainList[x].Name;
                if (!File.Exists(Directory.GetCurrentDirectory() + "\\images\\cards\\" + DataBase.MainList[x].IDInterger + ".jpg")) { missingcardlist.Add("https://images.ygoprodeck.com/images/cards/" + DataBase.MainList[x].IDInterger + ".jpg"); }

            }
            if (missingcardlist.Count > 0) { btnMissingCards.Visible = true; }
            startForm.UpdateLoading(100);

        }
        #endregion

        #region Public Functions
        public void ReenableReportButton()
        {
            btnValueReport.Enabled = true;
        }

        public void ReenableSetReportButton()
        {
            btnSetPriceReport.Enabled = true;
        }
        #endregion

        #region Initialize Diplay and Load page
        private void InitializeCardView()
        {
            _ActiveList = DataBase.MainList;
            lblFilterTittle.Text = "All Cards : " + _ActiveList.Count + " cards.";
            int imageID = 0;
            int Y_Location = 15;
            for (int x = 0; x < 5; x++)
            {
                int X_Location = 10;
                for (int y = 0; y < 9; y++)
                {
                    //Initialize the border box Image
                    Panel CardBox = new Panel();
                    GroupCardView.Controls.Add(CardBox);
                    CardBox.Location = new Point(X_Location, Y_Location);
                    CardBox.BorderStyle = BorderStyle.FixedSingle;
                    CardBox.Size = new Size(65, 85);
                    _CardPanelList.Add(CardBox);

                    //Initialize the Card Collection Icon
                    PictureBox CardIcon = new PictureBox();
                    CardBox.Controls.Add(CardIcon);
                    CardIcon.Location = new Point(1, 1);
                    CardIcon.BorderStyle = BorderStyle.FixedSingle;
                    CardIcon.Size = new Size(20, 20);
                    CardIcon.SizeMode = PictureBoxSizeMode.StretchImage;
                    CardIcon.Tag = imageID;
                    //CardIcon.Visible = false;
                    _IconImageList.Add(CardIcon);

                    //Initialize the card Image
                    PictureBox CardImage = new PictureBox();
                    CardBox.Controls.Add(CardImage);
                    CardImage.Location = new Point(1, 1);
                    CardImage.BorderStyle = BorderStyle.FixedSingle;
                    CardImage.Size = new Size(61, 81);
                    CardImage.SizeMode = PictureBoxSizeMode.StretchImage;
                    CardImage.Tag = imageID;
                    CardImage.Click += new EventHandler(this.Image_click);
                    _CardImageList.Add(CardImage);

                    //Initialize the Rarity Icon
                    PictureBox CardRarity = new PictureBox();
                    CardBox.Controls.Add(CardRarity);
                    CardRarity.Location = new Point(9, 62);
                    CardRarity.BorderStyle = BorderStyle.FixedSingle;
                    CardRarity.Size = new Size(43, 15);
                    CardRarity.SizeMode = PictureBoxSizeMode.StretchImage;
                    CardRarity.Visible = false;
                    CardRarity.BringToFront();
                    _CardRaritiesList.Add(CardRarity);

                    X_Location += 67;
                    imageID++;
                }
                Y_Location += 87;
            }

            LoadPage();
        }
        void Image_click(object sender, EventArgs e)
        {
            //clear the card border of the previous card clicked
            _CardPanelList[_PreviousCardInViewIndex].BackColor = Color.Black;

            //Generate the Picture Box object
            PictureBox thisPictureBox = (PictureBox)sender;

            //Determine the index of this card in relation to the entire active list
            int index = ((_CurrentPage * 45) - 45) + (int)thisPictureBox.Tag;

            //Set the card in view border as green
            _CardPanelList[(int)thisPictureBox.Tag].BackColor = Color.LawnGreen;

            //Save this Picture Box tag as the "previous card" for the next click
            _PreviousCardInViewIndex = (int)thisPictureBox.Tag;

            //Set this Card Object reference
            if (_IsSetPackViewON)
            {
                _CardObjectInView = _ActiveSetList[index].BaseCard;
            }
            else
            {
                _CardObjectInView = _ActiveList[index];
            }

            //Clear the current Main Image and replace with this new card in view
            if (PicImage.Image != null) { PicImage.Image.Dispose(); }
            PicImage.Image = ImageServer.CardImage(_CardObjectInView.IDInterger);

            //Show the Name in the UI
            lblTCGName.Text = "TCG Name: " + _CardObjectInView.Name;

            //Initialize this card's tags
            InitializeTags(_CardObjectInView);

            //Show the rariry and prices if in set view
            if (_IsSetPackViewON)
            {
                SingleCardObject thisSinlgeCard = _ActiveSetList[index];
                lblRariryLabel.Visible = true;
                lblRarity.Visible = true;
                lblRarity.Text = thisSinlgeCard.Rarity.ToString();
                switch(thisSinlgeCard.Rarity)
                {
                    case "Common": lblRarity.ForeColor = Color.White; break;
                    case "Rare": lblRarity.ForeColor = Color.PaleGreen; break;
                    case "Ultra Rare": lblRarity.ForeColor = Color.Moccasin; break;
                    case "Ultimate Rare": lblRarity.ForeColor = Color.HotPink; break;
                    case "Gold Rare": lblRarity.ForeColor = Color.Gold; break;
                    case "Hobby": lblRarity.ForeColor = Color.MediumPurple; break;
                    case "Millennium Secret Rare": lblRarity.ForeColor = Color.Goldenrod; break;
                    case "Platinum Secret Rare": lblRarity.ForeColor = Color.LightSkyBlue; break;
                    case "Starfoil": lblRarity.ForeColor = Color.BlueViolet; break;
                    case "Shattefoil": lblRarity.ForeColor = Color.MediumTurquoise; break;
                    case "Super Rare": lblRarity.ForeColor = Color.SteelBlue; break;
                    case "Secret Rare": lblRarity.ForeColor = Color.Pink; break;
                    case "Ghost Rare": lblRarity.ForeColor = Color.PowderBlue; break;
                    case "Premium Gold Rare": lblRarity.ForeColor = Color.DarkGoldenrod; break;
                    case "Gold Secret": lblRarity.ForeColor = Color.Yellow; break;
                    case "Platinum Rare": lblRarity.ForeColor = Color.Aqua; break;
                    case "COLLECTOR'S RARE": lblRarity.ForeColor = Color.RosyBrown; break;
                    case "Mosaic Rare": lblRarity.ForeColor = Color.DarkViolet; break;
                    case "Prismatic Secret Rare": lblRarity.ForeColor = Color.Plum; break;
                    case "Ultra Rare (Pharaoh's Rare)": lblRarity.ForeColor = Color.DarkRed; break;
                    default: lblRarity.ForeColor = Color.White; break;
                }


                lblMarketPricelabel.Visible = true;
                lblMarketPrice.Visible = true;
                lblMarketPrice.Text = "$" + thisSinlgeCard.MarketPrice.ToString();
                if(thisSinlgeCard.MarketPrice < 1)
                {
                    lblMarketPrice.ForeColor = Color.White;
                }
                else if(thisSinlgeCard.MarketPrice < 5)
                {
                    lblMarketPrice.ForeColor = Color.LightGreen;
                }
                else if (thisSinlgeCard.MarketPrice < 50)
                {
                    lblMarketPrice.ForeColor = Color.HotPink;
                }
                else
                {
                    lblMarketPrice.ForeColor = Color.Gold;
                }
                lblMedianPricelabel.Visible = true;
                lblMedianPrice.Visible = true;
                lblMedianPrice.Text = "$" + thisSinlgeCard.MedianPrice.ToString();
                if (thisSinlgeCard.MarketPrice < 1)
                {
                    lblMedianPrice.ForeColor = Color.White;
                }
                else if (thisSinlgeCard.MarketPrice < 5)
                {
                    lblMedianPrice.ForeColor = Color.LightGreen;
                }
                else if (thisSinlgeCard.MarketPrice < 50)
                {
                    lblMedianPrice.ForeColor = Color.HotPink;
                }
                else
                {
                    lblMedianPrice.ForeColor = Color.Gold;
                }
                lblCodelabel.Visible = true;
                lblCode.Visible = true;
                lblCode.Text = thisSinlgeCard.Code;
            }
            else
            {
                lblRariryLabel.Visible = false;
                lblRarity.Visible = false;
                lblMarketPricelabel.Visible = false;
                lblMarketPrice.Visible = false;
                lblMedianPricelabel.Visible = false;
                lblMedianPrice.Visible = false;
                lblCodelabel.Visible = false;
                lblCode.Visible = false;
            }
        }
        private void InitializeTags(CardObject thisCard)
        {
            //Clear all existing tags
            for (int x = 0; x < _TagLabelList.Count; x++) { _TagLabelList[x].Dispose(); }
            _TagLabelList.Clear();

            int Y_Location = 1;
            for (int x = 0; x < thisCard.Sets.Count; x++)
            {
                string ActiveSetCode = thisCard.Sets[x].Code;

                Label tagLabel = new Label();
                PanelTagList.Controls.Add(tagLabel);
                tagLabel.Location = new Point(1, Y_Location);
                tagLabel.BorderStyle = BorderStyle.FixedSingle;
                tagLabel.Size = new Size(93, 20);
                tagLabel.AutoSize = false;
                tagLabel.Text = thisCard.Sets[x].Code;
                tagLabel.TextAlign = ContentAlignment.MiddleLeft;
                tagLabel.ForeColor = Color.White;
                tagLabel.Tag = x;
                tagLabel.Click += new EventHandler(this.SetCodeLabel_clicked);
                _TagLabelList.Add(tagLabel);

                //Set Color base on if it is obtained or not
                if (thisCard.Sets[x].Obtained)
                {
                    tagLabel.ForeColor = Color.Red;
                }
                else
                {
                    tagLabel.ForeColor = Color.White;
                }

                Y_Location += 20;
            }

            //Resize the TagList Container
            int width = 115;
            if (thisCard.Sets.Count < 15) { width = 97; }
            int height = 292;
            if (thisCard.Sets.Count < 15) { height = ((thisCard.Sets.Count * 20) + 4); }
            PanelTagList.Size = new Size(width, height);
        }
        private void SetCodeLabel_clicked(object sender, EventArgs e)
        {
            Label thisLabel = (Label)sender;
            int codeIndex = (int)thisLabel.Tag;

            //flip thie obtained status
            _CardObjectInView.FlipObtainedStatus(codeIndex);

            if (thisLabel.ForeColor == Color.White) 
            { 
                thisLabel.ForeColor = Color.Red;
                SaveDataManager.AddCard(_CardObjectInView, _CardObjectInView.Sets[codeIndex].Code);
            }
            else 
            { 
                thisLabel.ForeColor = Color.White;
                SaveDataManager.RemoveCard(_CardObjectInView, _CardObjectInView.Sets[codeIndex].Code);
            }

            //Update Stats
            StatsManager.UpdateStat(_CardObjectInView.Type);
            StatsManager.UpdateSetStat(_CardObjectInView.Sets[codeIndex].Name);

            //Rewrite save file
            SaveDataManager.WriteSaveFile();
        }
        private void LoadPage()
        {
            GroupCardView.Text = "Page: " + _CurrentPage;

            int iterator = (_CurrentPage * 45) - 45;

            //Save a ref of the card list that is active
            List<CardObject> activelist = new List<CardObject>();
            if (_IsSetPackViewON) 
            {
                List<CardObject> tmplist = new List<CardObject>();
                for (int x = 0; x < _ActiveSetList.Count; x++)
                {
                    tmplist.Add(_ActiveSetList[x].BaseCard);
                }
                activelist = tmplist; 
            }
            else { activelist = _ActiveList; }

            for (int x = 0; x < _CardImageList.Count; x++)
            {
                //If the itearator reached past the last card in the Card DB vanish the card from view
                if (iterator + x >= activelist.Count)
                {
                    _CardPanelList[x].Visible = false;
                }
                else
                {
                    //Show Card Image
                    _CardPanelList[x].Visible = true;

                    //Get the ref to the card that wil be shown in this card image
                    CardObject thisCard = activelist[iterator + x];

                    if (_IsYouCollectionON)
                    {
                        //Show card if obtained, otherwise show the back of the card
                        if (_IsSetPackViewON)
                        {
                            //If the current single card is obtained, show card, otherwise show the back of the card
                            SingleCardObject thisSingleCard = _ActiveSetList[iterator + x];
                            if (thisSingleCard.Obtained)
                            {
                                if (_CardImageList[x].Image != null) { _CardImageList[x].Image.Dispose(); }
                                _CardImageList[x].Image = ImageServer.CardImage(thisSingleCard.IDInterger);
                            }
                            else
                            {
                                if (_CardImageList[x].Image != null) { _CardImageList[x].Image.Dispose(); }
                                _CardImageList[x].Image = Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\cards\\Card Back.png");
                            }
                        }
                        else
                        {
                            CardObject thisCardObject = activelist[iterator + x];
                            if (thisCardObject.Obtained)
                            {
                                if (_CardImageList[x].Image != null) { _CardImageList[x].Image.Dispose(); }
                                _CardImageList[x].Image = ImageServer.CardImage(thisCard.IDInterger);
                            }
                            else
                            {
                                if (_CardImageList[x].Image != null) { _CardImageList[x].Image.Dispose(); }
                                _CardImageList[x].Image = Image.FromFile(Directory.GetCurrentDirectory() + "\\images\\cards\\Card Back.png");
                            }
                        }
                    }
                    else
                    {
                        if (_IsSetPackViewON)
                        {
                            SingleCardObject thisSingleCard = _ActiveSetList[iterator + x];
                            if (_CardImageList[x].Image != null) { _CardImageList[x].Image.Dispose(); }
                            _CardImageList[x].Image = ImageServer.CardImage(thisSingleCard.IDInterger);
                        }
                        else
                        {
                            CardObject thisCardObject = activelist[iterator + x];
                            if (_CardImageList[x].Image != null) { _CardImageList[x].Image.Dispose(); }
                            _CardImageList[x].Image = ImageServer.CardImage(thisCard.IDInterger);
                        }
                    }

                    //Show the collection icon
                    if (_IsSetPackViewON)
                    {
                        //Show checkmark or nothing.
                        SingleCardObject thisSingleCard = _ActiveSetList[iterator + x];
                        if (thisSingleCard.Obtained)
                        {
                            _IconImageList[x].Visible = true;
                            if (_IconImageList[x].Image != null) { _IconImageList[x].Image.Dispose(); }
                            _IconImageList[x].Image = ImageServer.CheckMark();
                        }
                        else
                        {
                            _IconImageList[x].Visible = false;
                        }

                        //Show rarity icon
                        if (thisSingleCard.Rarity != "Common")
                        {
                            _CardRaritiesList[x].Visible = true;
                            if (_CardRaritiesList[x].Image != null) { _CardRaritiesList[x].Image.Dispose(); }
                            _CardRaritiesList[x].Image = ImageServer.Rarity(thisSingleCard.Rarity);
                        }
                        else
                        {
                            _CardRaritiesList[x].Visible = false;
                        }

                    }
                    else
                    {
                        //Show the collection icon
                        int TotalSetCodes = activelist[iterator + x].Sets.Count;
                        int SetCodesCount = activelist[iterator + x].SetsObtained;
                        if (TotalSetCodes == SetCodesCount)
                        {
                            _IconImageList[x].Visible = true;
                            if (_IconImageList[x].Image != null) { _IconImageList[x].Image.Dispose(); }
                            _IconImageList[x].Image = ImageServer.StarIcon();
                        }
                        else if (SetCodesCount > 0)
                        {
                            _IconImageList[x].Visible = true;
                            if (_IconImageList[x].Image != null) { _IconImageList[x].Image.Dispose(); }
                            _IconImageList[x].Image = ImageServer.ExclamationMark();
                        }
                        else
                        {
                            _IconImageList[x].Visible = false;
                        }

                        //vanish rarity icon
                        _CardRaritiesList[x].Visible = false;
                    }
                }
            }
        }
        #endregion

        #region Stats Components Initializers
        private void InitializeMasterStatsComponents()
        {
            GroupBox thisGroupBox = BoxMonsterStats;
            List<string> monsterGroupList = new List<string>();
            monsterGroupList.Add("Aqua");
            monsterGroupList.Add("Beast");
            monsterGroupList.Add("Beast-Warrior");
            monsterGroupList.Add("Cyberse");
            monsterGroupList.Add("Divine-Beast");
            monsterGroupList.Add("Dinosaur");
            monsterGroupList.Add("Dragon");
            monsterGroupList.Add("Fairy");
            monsterGroupList.Add("Fiend");
            monsterGroupList.Add("Fish");
            monsterGroupList.Add("Insect");
            monsterGroupList.Add("Machine");
            monsterGroupList.Add("Plant");
            monsterGroupList.Add("Psychic");
            monsterGroupList.Add("Pyro");
            monsterGroupList.Add("Reptile");
            monsterGroupList.Add("Rock");
            monsterGroupList.Add("Sea Serpent");
            monsterGroupList.Add("Spellcaster");
            monsterGroupList.Add("Thunder");
            monsterGroupList.Add("Warrior");
            monsterGroupList.Add("Winged Beast");
            monsterGroupList.Add("Wyrm");
            monsterGroupList.Add("Zombie");
            monsterGroupList.Add("All Monsters");

            int Y_Location = 14;
            for(int x = 0; x < monsterGroupList.Count; x++)
            {
                InitializeLabelsRow(thisGroupBox, Y_Location, monsterGroupList[x]);
                Y_Location += 14;
            }

            Y_Location += 16;
            List<string> AllGroupsGroupList = new List<string>();
            AllGroupsGroupList.Add("All Monsters");
            AllGroupsGroupList.Add("All Spells");
            AllGroupsGroupList.Add("All Traps");
            AllGroupsGroupList.Add("All Cards");
            for (int x = 0; x < AllGroupsGroupList.Count; x++)
            {
                InitializeLabelsRow(thisGroupBox, Y_Location, AllGroupsGroupList[x]);
                Y_Location += 15;
            }

            thisGroupBox = BoxSpellsStats;
            List<string> spellGroupList = new List<string>();
            spellGroupList.Add("Normal Spells");
            spellGroupList.Add("Continuous Spells");
            spellGroupList.Add("Equip Spells");
            spellGroupList.Add("Quick-Play Spells");
            spellGroupList.Add("Field Spells");
            spellGroupList.Add("Ritual Spells");
            spellGroupList.Add("All Spells");

            Y_Location = 14;
            for (int x = 0; x < spellGroupList.Count; x++)
            {
                InitializeLabelsRow(thisGroupBox, Y_Location, spellGroupList[x]);
                Y_Location += 15;
            }

            thisGroupBox = BoxTrapsStats;
            List<string> trapsGroupList = new List<string>();
            trapsGroupList.Add("Normal Traps");
            trapsGroupList.Add("Continuous Traps");
            trapsGroupList.Add("Counter Traps");
            trapsGroupList.Add("All Traps");

            Y_Location = 14;
            for (int x = 0; x < trapsGroupList.Count; x++)
            {
                InitializeLabelsRow(thisGroupBox, Y_Location, trapsGroupList[x]);
                Y_Location += 15;
            }

            thisGroupBox = BoxOtherStats;
            List<string> otherGroupList = new List<string>();
            otherGroupList.Add("Normal Monsters");
            otherGroupList.Add("Effect Monsters");
            otherGroupList.Add("Fusion Monsters");
            otherGroupList.Add("Ritual Monsters");
            otherGroupList.Add("Synchro Monsters");
            otherGroupList.Add("Xyz Monsters");
            otherGroupList.Add("Pendulum Monsters");
            otherGroupList.Add("Link Monsters");
            otherGroupList.Add("Flip Monsters");
            otherGroupList.Add("Spirit Monsters");
            otherGroupList.Add("Toon Monsters");
            otherGroupList.Add("Union Monsters");
            otherGroupList.Add("Tuner Monsters");
            otherGroupList.Add("Gemini Monsters");

            Y_Location = 14;
            for (int x = 0; x < otherGroupList.Count; x++)
            {
                InitializeLabelsRow(thisGroupBox, Y_Location, otherGroupList[x]);
                Y_Location += 15;
            }
        }
        private void InitializeLabelsRow(GroupBox Box, int GroupNameLabelYPoint, string group)
        {
            //get data
            int obtainedCount = DataBase.GetListObtainedCount(group);
            int totalCount = DataBase.GetCardTypeList(group).Count;

            int Ysize = 14;

            //Initialize components               
            //Monster Type Label
            Label GroupNameLabel = new Label();
            Box.Controls.Add(GroupNameLabel);
            GroupNameLabel.Text = group;
            GroupNameLabel.BorderStyle = BorderStyle.FixedSingle;
            GroupNameLabel.AutoSize = false;
            GroupNameLabel.Size = new Size(79, Ysize);
            GroupNameLabel.Location = new Point(10, GroupNameLabelYPoint);

            //Obtained label
            Label ObtainedLabel = new Label();
            Box.Controls.Add(ObtainedLabel);
            ObtainedLabel.Text = obtainedCount.ToString();
            ObtainedLabel.BorderStyle = BorderStyle.FixedSingle;
            ObtainedLabel.AutoSize = false;
            ObtainedLabel.Size = new Size(35, Ysize);
            ObtainedLabel.TextAlign = ContentAlignment.MiddleRight;
            ObtainedLabel.Location = new Point(89, GroupNameLabelYPoint);

            //"/" label
            Label LashLabel = new Label();
            Box.Controls.Add(LashLabel);
            LashLabel.Text = "/";
            LashLabel.BorderStyle = BorderStyle.FixedSingle;
            LashLabel.AutoSize = false;
            LashLabel.Size = new Size(14, Ysize);
            LashLabel.Location = new Point(124, GroupNameLabelYPoint);

            //Base Total label
            Label BaseTotalLabel = new Label();
            Box.Controls.Add(BaseTotalLabel);
            BaseTotalLabel.Text = totalCount.ToString();
            BaseTotalLabel.BorderStyle = BorderStyle.FixedSingle;
            BaseTotalLabel.AutoSize = false;
            BaseTotalLabel.Size = new Size(35, Ysize);
            BaseTotalLabel.TextAlign = ContentAlignment.MiddleRight;
            BaseTotalLabel.Location = new Point(138, GroupNameLabelYPoint);


            double obtainedAmount = obtainedCount;
            double baseAmount = totalCount;
            double percentage = 0;
            if (totalCount > 0) { percentage = (obtainedAmount / baseAmount) * 100; }


            //Percentage label
            Label PercentageLabel = new Label();
            Box.Controls.Add(PercentageLabel);
            PercentageLabel.Text = (int)percentage + "%";
            PercentageLabel.BorderStyle = BorderStyle.FixedSingle;
            PercentageLabel.AutoSize = false;
            PercentageLabel.Size = new Size(40, Ysize);
            PercentageLabel.TextAlign = ContentAlignment.MiddleRight;
            PercentageLabel.Location = new Point(173, GroupNameLabelYPoint);

            ProgressBar bar = new ProgressBar();
            Box.Controls.Add(bar);
            bar.Value = (int)percentage;
            bar.Size = new Size(85, 15);
            bar.Location = new Point(213, GroupNameLabelYPoint);
            //ModifyProgressBar.UpdateColor(bar);

            StatsManager.AddRow(GroupNameLabel, ObtainedLabel, LashLabel, BaseTotalLabel, PercentageLabel, bar);
            //StatsManager.UpdateStat(group);
            if (percentage <= 33)
            {
                PercentageLabel.ForeColor = Color.Red;
            }
            else if (percentage <= 66)
            {
                PercentageLabel.ForeColor = Color.Yellow;
            }
            else if (percentage <= 99)
            {
                PercentageLabel.ForeColor = Color.Lime;
            }
            else
            {
                PercentageLabel.ForeColor = Color.Aqua;
            }

            //Pack all the labels for the update color function
            //List<Label> labels = new List<Label>();
            //labels.Add(ObtainedLabel);
            //labels.Add(PercentageLabel);
            //labels.Add(GroupNameLabel);
            //labels.Add(LashLabel);
            //labels.Add(BaseTotalLabel);
        }
        #endregion

        #region Set Stats Compenent Initializers
        private void InitializeSetStatsComponents()
        {
            InitializePackStatsComponents("Booster Packs", TabBoosterPacks);
            InitializePackStatsComponents("Special Edition Boxes", TabSpecialEditonBoxes);
            InitializePackStatsComponents("Starter Decks", TabStsarterDecks);
            InitializePackStatsComponents("Structure Decks", TabStructureDecks);
            InitializePackStatsComponents("Tins", TabTins);
            InitializePackStatsComponents("SPEED DUEL", TabSPDuel);
            InitializePackStatsComponents("Duelist Packs", TabDP);
            InitializePackStatsComponents("Duel Terminal Cards", TabDT);
            InitializePackStatsComponents("Others", TabOthers);
            InitializePackStatsComponents("Magazines, Books, Comics", TabMBC);
            InitializePackStatsComponents("Tournaments", TabTournaments);
            InitializePackStatsComponents("Promotional Cards", TabPromo);
            InitializePackStatsComponents("Video Game Bundles", TabVideoGames);
        }
        private void InitializePackStatsComponents(string group, TabPage page)
        {
            int PackNameLabelYPoint = 15;
            int TotalObtained = 0;
            int TotalInPack = 0;

            List<SetPack> thisGroupOfSets = DataBase.GetSetsFromGroup(group);
            

            for (int x = 0; x < thisGroupOfSets.Count; x++)
            {
                //Determine data for set
                string name = thisGroupOfSets[x].Name;
                string year = thisGroupOfSets[x].Year;
                string code = thisGroupOfSets[x].Code;
                int obtained = thisGroupOfSets[x].CardsObtained;
                int totalCount = thisGroupOfSets[x].TotalCount;

                //Initialize components               
                //Prefix Label
                Label packprefix = new Label();
                page.Controls.Add(packprefix);
                packprefix.Text = code;
                packprefix.ForeColor = Color.White;
                packprefix.BorderStyle = BorderStyle.FixedSingle;
                packprefix.AutoSize = false;
                packprefix.Size = new Size(40, 15);
                packprefix.Location = new Point(11, PackNameLabelYPoint);

                //Year label
                Label packyear = new Label();
                page.Controls.Add(packyear);
                packyear.Text = year;
                packyear.ForeColor = Color.White;
                packyear.BorderStyle = BorderStyle.FixedSingle;
                packyear.AutoSize = false;
                packyear.Size = new Size(37, 15);
                packyear.Location = new Point(52, PackNameLabelYPoint);

                //Name label
                Label packname = new Label();
                page.Controls.Add(packname);
                packname.Text = name;
                packname.ForeColor = Color.White;
                packname.BorderStyle = BorderStyle.FixedSingle;
                packname.AutoSize = false;
                packname.Size = new Size(240, 15);
                packname.Location = new Point(90, PackNameLabelYPoint);

                double percentage = 0;
                if (totalCount > 0) { percentage = (obtained/totalCount) * 100; }

                //Obtained label
                Label Obtained = new Label();
                page.Controls.Add(Obtained);
                Obtained.Text = obtained.ToString();
                Obtained.ForeColor = Color.White;
                Obtained.BorderStyle = BorderStyle.FixedSingle;
                Obtained.TextAlign = ContentAlignment.MiddleRight;
                Obtained.AutoSize = false;
                Obtained.Size = new Size(38, 15);
                Obtained.Location = new Point(331, PackNameLabelYPoint);
                TotalObtained += obtained;

                // / label
                Label dash = new Label();
                page.Controls.Add(dash);
                dash.Text = "/";
                dash.ForeColor = Color.White;
                dash.BorderStyle = BorderStyle.FixedSingle;
                dash.AutoSize = false;
                dash.Size = new Size(12, 15);
                dash.Location = new Point(370, PackNameLabelYPoint);

                //Total label
                Label Total = new Label();
                page.Controls.Add(Total);
                Total.Text = totalCount.ToString();
                Total.ForeColor = Color.White;
                Total.BorderStyle = BorderStyle.FixedSingle;
                Total.TextAlign = ContentAlignment.MiddleRight;
                Total.AutoSize = false;
                Total.Size = new Size(42, 15);
                Total.Location = new Point(383, PackNameLabelYPoint);
                TotalInPack += totalCount;

                //Percentage label
                Label percelabel = new Label();
                page.Controls.Add(percelabel);
                percelabel.Text = (int)percentage + "%";
                percelabel.ForeColor = Color.White;
                percelabel.BorderStyle = BorderStyle.FixedSingle;
                percelabel.AutoSize = false;
                percelabel.Size = new Size(40, 15);
                percelabel.Location = new Point(426, PackNameLabelYPoint);

                ProgressBar bar = new ProgressBar();
                page.Controls.Add(bar);
                bar.Value = (int)percentage;
                bar.Size = new Size(150, 15);
                bar.Location = new Point(463, PackNameLabelYPoint);
                //ModifyProgressBar.UpdateColor(bar);

                //Move Y lower to create a new row
                PackNameLabelYPoint += 15;

                StatsManager.AddSetRow(group, packprefix, packyear, packname, Obtained, dash, Total, percelabel, bar);
                StatsManager.UpdateSetStat(name);
            }

            //Add One more line for totals
            //Initialize components               
            //Prefix Label
            Label xpackprefix = new Label();
            page.Controls.Add(xpackprefix);
            xpackprefix.Text = "XXXX";
            xpackprefix.ForeColor = Color.Red;
            xpackprefix.BorderStyle = BorderStyle.FixedSingle;
            xpackprefix.AutoSize = false;
            xpackprefix.Size = new Size(40, 15);
            xpackprefix.Location = new Point(11, PackNameLabelYPoint);

            //Year label
            Label xpackyear = new Label();
            page.Controls.Add(xpackyear);
            xpackyear.Text = "----";
            xpackyear.ForeColor = Color.Red;
            xpackyear.BorderStyle = BorderStyle.FixedSingle;
            xpackyear.AutoSize = false;
            xpackyear.Size = new Size(37, 15);
            xpackyear.Location = new Point(52, PackNameLabelYPoint);

            //Name label
            Label xpackname = new Label();
            page.Controls.Add(xpackname);
            xpackname.Text = "Totals:";
            xpackname.ForeColor = Color.Red;
            xpackname.BorderStyle = BorderStyle.FixedSingle;
            xpackname.AutoSize = false;
            xpackname.Size = new Size(240, 15);
            xpackname.Location = new Point(90, PackNameLabelYPoint);

            //Obtained label
            Label xObtained = new Label();
            page.Controls.Add(xObtained);
            xObtained.Text = TotalObtained.ToString();
            xObtained.ForeColor = Color.Red;
            xObtained.BorderStyle = BorderStyle.FixedSingle;
            xObtained.TextAlign = ContentAlignment.MiddleRight;
            xObtained.AutoSize = false;
            xObtained.Size = new Size(38, 15);
            xObtained.Location = new Point(331, PackNameLabelYPoint);

            // / label
            Label xdash = new Label();
            page.Controls.Add(xdash);
            xdash.Text = "/";
            xdash.ForeColor = Color.Red;
            xdash.BorderStyle = BorderStyle.FixedSingle;
            xdash.AutoSize = false;
            xdash.Size = new Size(12, 15);
            xdash.Location = new Point(370, PackNameLabelYPoint);

            //Total label
            Label xTotal = new Label();
            page.Controls.Add(xTotal);
            xTotal.Text = TotalInPack.ToString();
            xTotal.ForeColor = Color.Red;
            xTotal.BorderStyle = BorderStyle.FixedSingle;
            xTotal.TextAlign = ContentAlignment.MiddleRight;
            xTotal.AutoSize = false;
            xTotal.Size = new Size(42, 15);
            xTotal.Location = new Point(383, PackNameLabelYPoint);

            //Percentage label
            double xpercentage = ((double)TotalObtained / (double)TotalInPack) * 100;
            Label xpercelabel = new Label();
            page.Controls.Add(xpercelabel);
            xpercelabel.Text = (int)xpercentage + "%";
            xpercelabel.ForeColor = Color.Red;
            xpercelabel.BorderStyle = BorderStyle.FixedSingle;
            xpercelabel.AutoSize = false;
            xpercelabel.Size = new Size(40, 15);
            xpercelabel.Location = new Point(426, PackNameLabelYPoint);

            ProgressBar xbar = new ProgressBar();
            page.Controls.Add(xbar);
            xbar.Value = (int)xpercentage;
            xbar.Size = new Size(150, 15);
            xbar.Location = new Point(463, PackNameLabelYPoint);
            //ModifyProgressBar.UpdateColor(xbar);

            StatsManager.AddSetRow(group, xpackprefix, xpackyear, xpackname, xObtained, xdash, xTotal, xpercelabel, xbar);
        }
        #endregion

        #region Data
        private List<CardObject> _ActiveList;
        private List<SingleCardObject> _ActiveSetList;
        List<string> missingcardlist = new List<string>();

        private int _CurrentPage = 1;
        private CardObject _CardObjectInView = null;
        private int _PreviousCardInViewIndex = 0;
        private bool _IsSetPackViewON = false;
        private bool _IsYouCollectionON = false;
        private string _currentSetName = "none";


        private List<Panel> _CardPanelList = new List<Panel>();
        private List<PictureBox> _IconImageList = new List<PictureBox>();
        private List<PictureBox> _CardImageList = new List<PictureBox>();
        private List<PictureBox> _CardRaritiesList = new List<PictureBox>();
        private List<Label> _TagLabelList = new List<Label>();
        #endregion

        #region Internal System Functions
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }
        #endregion

        #region Previous/Next Buttons
        private void btnPreviousPage_Click(object sender, EventArgs e)
        {
            int activeListCount = 0;
            if (_IsSetPackViewON) { activeListCount = _ActiveSetList.Count; }
            else { activeListCount = _ActiveList.Count; }
            int lastpage = (activeListCount / 45) + 1;

            if (_CurrentPage == 1) { _CurrentPage = lastpage; }
            else { _CurrentPage--; }

            GroupCardView.Text = "Page: " + _CurrentPage;
            LoadPage();
        }
        private void btnNextPage_Click(object sender, EventArgs e)
        {
            int activeListCount = 0;
            if (_IsSetPackViewON) { activeListCount = _ActiveSetList.Count; }
            else { activeListCount = _ActiveList.Count; }
            int lastpage = (activeListCount / 45) + 1;

            if (_CurrentPage == lastpage) { _CurrentPage = 1; }
            else { _CurrentPage++; }

            GroupCardView.Text = "Page: " + _CurrentPage;
            LoadPage();
        }
        #endregion

        #region General UI listeners
        private void listSetGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            string setGroupSelected = (string)listSetGroups.SelectedItem;

            List<string> thisGroupNameList = DataBase.GetSetNameList(setGroupSelected);
            listSetlist.Items.Clear();

            for (int x = 0; x < thisGroupNameList.Count; x++)
            {
                listSetlist.Items.Add(thisGroupNameList[x]);
            }

            listSetlist.SetSelected(0, true);

        }
        private void btnFilterSet_Click(object sender, EventArgs e)
        {
            string fullline = listSetlist.SelectedItem.ToString();
            int index = fullline.IndexOf("-");
            string linenoyear = fullline.Substring(index +2);
            index = linenoyear.IndexOf("-");
            string setname = linenoyear.Substring(index + 2);

            _currentSetName = setname;
            _ActiveSetList = DataBase.GetSetCardList(setname);
            lblFilterTittle.Text = "Set " + setname + " - " + _ActiveSetList.Count + " cards.";
            _CurrentPage = 1;
            _IsSetPackViewON = true;
            LoadPage();
            btnClear.Visible = true;
            btnSetPriceReport.Visible = true;
        }
        private void btnMissingCards_Click(object sender, EventArgs e)
        {
            Form popupform = new Form();
            popupform.Size = new Size(500, 600);
            TextBox box = new TextBox();
            popupform.Controls.Add(box);
            box.Multiline = true;
            box.Location = new Point(0, 0);
            box.Size = new Size(500, 600);

            for(int x = 0; x < missingcardlist.Count; x++)
            {
                box.Text += missingcardlist[x] + "\r\n";
            }

            popupform.Show();
        }
        #endregion

        #region Main Filters
        private void FilterByCardType(string group)
        {
            _ActiveList = DataBase.GetCardTypeList(group);
            lblFilterTittle.Text = "Group " + group + " - " + _ActiveList.Count + " cards.";
            _CurrentPage = 1;
            _IsSetPackViewON = false;
            LoadPage();
            btnClear.Visible = true;
            btnSetPriceReport.Visible = false;
        }
        private void btnFilterAqua_Click(object sender, EventArgs e)
        {
            FilterByCardType("Aqua");
        }
        private void btnFilterBeast_Click(object sender, EventArgs e)
        {
            FilterByCardType("Beast");
        }
        private void btnFilterBeastWarrior_Click(object sender, EventArgs e)
        {
            FilterByCardType("Beast-Warrior");
        }
        private void btnFilterCyberce_Click(object sender, EventArgs e)
        {
            FilterByCardType("Cyberse");
        }
        private void btnFilterDinosaur_Click(object sender, EventArgs e)
        {
            FilterByCardType("Dinosaur");
        }
        private void btnFilterDivine_Click(object sender, EventArgs e)
        {
            FilterByCardType("Divine-Beast");
        }
        private void btnFilterDragon_Click(object sender, EventArgs e)
        {
            FilterByCardType("Dragon");
        }
        private void btnFilterFairy_Click(object sender, EventArgs e)
        {
            FilterByCardType("Fairy");
        }
        private void btnFilterFiend_Click(object sender, EventArgs e)
        {
            FilterByCardType("Fiend");
        }
        private void btnFilterFish_Click(object sender, EventArgs e)
        {
            FilterByCardType("Fish");
        }
        private void btnFilterInsect_Click(object sender, EventArgs e)
        {
            FilterByCardType("Insect");
        }
        private void btnFilterMachine_Click(object sender, EventArgs e)
        {
            FilterByCardType("Machine");
        }
        private void btnFilterPlant_Click(object sender, EventArgs e)
        {
            FilterByCardType("Plant");
        }
        private void btnFilterPsychic_Click(object sender, EventArgs e)
        {
            FilterByCardType("Psychic");
        }
        private void btnFilterPyro_Click(object sender, EventArgs e)
        {
            FilterByCardType("Pyro");
        }
        private void btnFilterReptile_Click(object sender, EventArgs e)
        {
            FilterByCardType("Reptile");
        }
        private void btnFilterRock_Click(object sender, EventArgs e)
        {
            FilterByCardType("Rock");
        }
        private void btnFilterSeaSerpent_Click(object sender, EventArgs e)
        {
            FilterByCardType("Sea Serpent");
        }
        private void btnFilterSpellcaster_Click(object sender, EventArgs e)
        {
            FilterByCardType("Spellcaster");
        }
        private void btnFilterThunder_Click(object sender, EventArgs e)
        {
            FilterByCardType("Thunder");
        }
        private void btnFilterWarrior_Click(object sender, EventArgs e)
        {
            FilterByCardType("Warrior");
        }
        private void btnFilterWingedBeast_Click(object sender, EventArgs e)
        {
            FilterByCardType("Winged Beast");
        }
        private void btnFilterWyrm_Click(object sender, EventArgs e)
        {
            FilterByCardType("Wyrm");
        }
        private void btnFilterZombie_Click(object sender, EventArgs e)
        {
            FilterByCardType("Zombie");
        }
        private void btnNormalSpell_Click(object sender, EventArgs e)
        {
            FilterByCardType("Normal Spells");
        }
        private void btnContinousSpell_Click(object sender, EventArgs e)
        {
            FilterByCardType("Continuous Spells");
        }
        private void btnEquipSpell_Click(object sender, EventArgs e)
        {
            FilterByCardType("Equip Spells");
        }
        private void btnFieldSpell_Click(object sender, EventArgs e)
        {
            FilterByCardType("Field Spells");
        }
        private void btnQuickPlaySpell_Click(object sender, EventArgs e)
        {
            FilterByCardType("Quick-Play Spells");
        }
        private void btnRitualSpell_Click(object sender, EventArgs e)
        {
            FilterByCardType("Ritual Spells");
        }
        private void btnNormalTrap_Click(object sender, EventArgs e)
        {
            FilterByCardType("Normal Traps");
        }
        private void btnContinuosTrap_Click(object sender, EventArgs e)
        {
            FilterByCardType("Continuous Traps");
        }
        private void btnCounterTrap_Click(object sender, EventArgs e)
        {
            FilterByCardType("Counter Traps");
        }
        private void btnMonster_Click(object sender, EventArgs e)
        {
            FilterByCardType("All Monsters");
        }
        private void btnSpells_Click(object sender, EventArgs e)
        {
            FilterByCardType("All Spells");
        }
        private void btnTraps_Click(object sender, EventArgs e)
        {
            FilterByCardType("All Traps");
        }
        private void btnNormal_Click(object sender, EventArgs e)
        {
            FilterByCardType("Normal Monsters");
        }
        private void btnEffect_Click(object sender, EventArgs e)
        {
            FilterByCardType("Effect Monsters");
        }
        private void btnFusion_Click(object sender, EventArgs e)
        {
            FilterByCardType("Fusion Monsters");
        }
        private void btnRitual_Click(object sender, EventArgs e)
        {
            FilterByCardType("Ritual Monsters");
        }
        private void btnSynchro_Click(object sender, EventArgs e)
        {
            FilterByCardType("Synchro Monsters");
        }
        private void btnXyz_Click(object sender, EventArgs e)
        {
            FilterByCardType("Xyz Monsters");
        }
        private void btnPendulum_Click(object sender, EventArgs e)
        {
            FilterByCardType("Pendulum Monsters");
        }
        private void btnLink_Click(object sender, EventArgs e)
        {
            FilterByCardType("Link Monsters");
        }
        private void btnFlip_Click(object sender, EventArgs e)
        {
            FilterByCardType("Flip Monsters");
        }
        private void btnSpirit_Click(object sender, EventArgs e)
        {
            FilterByCardType("Spirit Monsters");
        }
        private void btnToon_Click(object sender, EventArgs e)
        {
            FilterByCardType("Toon Monsters");
        }
        private void btnUnion_Click(object sender, EventArgs e)
        {
            FilterByCardType("Union Monsters");
        }
        private void btnTuner_Click(object sender, EventArgs e)
        {
            FilterByCardType("Tuner Monsters");
        }
        private void btnGemini_Click(object sender, EventArgs e)
        {
            FilterByCardType("Gemini Monsters");
        }
        private void btnDark_Click(object sender, EventArgs e)
        {
            FilterByCardType("Dark");
        }
        private void btnLight_Click(object sender, EventArgs e)
        {
            FilterByCardType("Light");
        }
        private void btnEarth_Click(object sender, EventArgs e)
        {
            FilterByCardType("Earth");
        }
        private void btnWater_Click(object sender, EventArgs e)
        {
            FilterByCardType("Water");
        }
        private void btnFire_Click(object sender, EventArgs e)
        {
            FilterByCardType("Fire");
        }
        private void btnWind_Click(object sender, EventArgs e)
        {
            FilterByCardType("Wind");
        }
        private void btnDivine_Click(object sender, EventArgs e)
        {
            FilterByCardType("Divine");
        }
        #endregion

        #region other listeners
        private void btnClear_Click(object sender, EventArgs e)
        {
            _ActiveList = DataBase.MainList;
            lblFilterTittle.Text = "All Cards : " + _ActiveList.Count + " cards.";
            _CurrentPage = 1;
            _IsSetPackViewON = false;
            LoadPage();
            btnClear.Visible = false;
            btnSetPriceReport.Visible = false;
        }

        private void btnTextSearch_Click(object sender, EventArgs e)
        {
            if(txtSearch.Text != "")
            {
                string searchterm = txtSearch.Text;
                _ActiveList = DataBase.GetCardsWithString(searchterm);
                lblFilterTittle.Text = "Search: " + searchterm + " - " + _ActiveList.Count + " cards.";
                _CurrentPage = 1;
                _IsSetPackViewON = false;
                LoadPage();
                btnClear.Visible = true;
                btnSetPriceReport.Visible = false;
            }
        }

        private void chkCollected_CheckedChanged(object sender, EventArgs e)
        {
            _IsYouCollectionON = chkCollected.Checked;
            LoadPage();
        }

        private void btnValueReport_Click(object sender, EventArgs e)
        {
            btnValueReport.Enabled = false;
            ValueReport thisReport = new ValueReport(this);
            thisReport.Show();
        }
        #endregion

        private void btnSetPriceReport_Click(object sender, EventArgs e)
        {
            btnSetPriceReport.Enabled = false;

            SetValueReport thisReport = new SetValueReport(_currentSetName, _ActiveSetList, this);
            thisReport.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SetSearcher ss = new SetSearcher();
            ss.Show();
        }
    }
}
