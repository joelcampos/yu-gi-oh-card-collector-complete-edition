﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public static class StatsManager
    {
        public static void AddRow(Label name, Label obtained, Label dash, Label total, Label percentage, ProgressBar bar)
        {
            _rows.Add(new MasterStatRow(name, obtained, dash, total, percentage, bar));
        }
        public static void AddSetRow(string group, Label code, Label year, Label name, Label obtained, Label dash, Label total, Label percentage, ProgressBar bar)
        {
            switch(group)
            {
                case "Booster Packs": _boosterRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Special Edition Boxes": _spBoxesRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Starter Decks": _starterDecksRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Structure Decks": _structureDecksRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Tins": _tinsRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "SPEED DUEL": _spDuelRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Duelist Packs": _DPRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Duel Terminal Cards": _DTRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Others": _otherRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Magazines, Books, Comics": _MBCRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Tournaments": _tournamentsRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Promotional Cards": _promosRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
                case "Video Game Bundles": _videoGamesRows.Add(new SetStatRow(code, year, name, obtained, dash, total, percentage, bar)); break;
            }
        }


        public static void UpdateStat(string type)
        {
            List<string> groupsAffected = new List<string>();

            bool isAMonster = false;
            if (type.Contains("Aqua")) { groupsAffected.Add("Aqua"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Beast")) { groupsAffected.Add("Beast"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Beast-Warrior")) { groupsAffected.Add("Beast-Warrior"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Cyberse")) { groupsAffected.Add("Cyberse"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Dinosaur")) { groupsAffected.Add("Dinosaur"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Dragon")) { groupsAffected.Add("Dragon"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Fairy")) { groupsAffected.Add("Fairy"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Fiend")) { groupsAffected.Add("Fiend"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Fish")) { groupsAffected.Add("Fish"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Insect")) { groupsAffected.Add("Insect"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Machine")) { groupsAffected.Add("Machine"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Plant")) { groupsAffected.Add("Plant"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Psychic")) { groupsAffected.Add("Psychic"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Pyro")) { groupsAffected.Add("Pyro"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Reptile")) { groupsAffected.Add("Reptile"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Rock")) { groupsAffected.Add("Rock"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Sea Serpent")) { groupsAffected.Add("Sea Serpent"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Spellcaster")) { groupsAffected.Add("Spellcaster"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Thunder")) { groupsAffected.Add("Thunder"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Warrior")) { groupsAffected.Add("Warrior"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Winged-Beast")) { groupsAffected.Add("Winged-Beast"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Wyrm")) { groupsAffected.Add("Wyrm"); groupsAffected.Add("All Monsters"); isAMonster = true; }
            if (type.Contains("Zombie")) { groupsAffected.Add("Zombie"); groupsAffected.Add("All Monsters"); isAMonster = true; }

            if (isAMonster && type.Contains("Normal")) { groupsAffected.Add("Normal Monsters"); }
            if (isAMonster && type.Contains("Effect")) { groupsAffected.Add("Effect Monsters"); }
            if (isAMonster && type.Contains("Fusion")) { groupsAffected.Add("Fusion Monsters"); }
            if (isAMonster && type.Contains("Ritual")) { groupsAffected.Add("Ritual Monsters"); }
            if (isAMonster && type.Contains("Synchro")) { groupsAffected.Add("Synchro Monsters"); }
            if (isAMonster && type.Contains("Xyz")) { groupsAffected.Add("Xyz Monsters"); }
            if (isAMonster && type.Contains("Pendulum")) { groupsAffected.Add("Pendulum Monsters"); }
            if (isAMonster && type.Contains("Link")) { groupsAffected.Add("Link Monsters"); }

            if (isAMonster && type.Contains("Flip")) { groupsAffected.Add("Flip Monsters"); }
            if (isAMonster && type.Contains("Spirit")) { groupsAffected.Add("Spirit Monsters"); }
            if (isAMonster && type.Contains("Toon")) { groupsAffected.Add("Toon Monsters"); }
            if (isAMonster && type.Contains("Union")) { groupsAffected.Add("Union Monsters"); }
            if (isAMonster && type.Contains("Tuner")) { groupsAffected.Add("Tuner Monsters"); }
            if (isAMonster && type.Contains("Gemini")) { groupsAffected.Add("Gemini Monsters"); }

            //bool isASpell = false;
            //if (!type.Contains("Spellcaster") && type.Contains("Spell")) { isASpell = true; }
            /*if (isASpell && type.Contains("Normal")) { groupsAffected.Add("Normal Spells"); groupsAffected.Add("All Spells"); }
            if (isASpell && type.Contains("Continuous")) { groupsAffected.Add("Continuous Spells"); groupsAffected.Add("All Spells"); }
            if (isASpell && type.Contains("Equip")) { groupsAffected.Add("Equip Spells"); groupsAffected.Add("All Spells"); }
            if (isASpell && type.Contains("Field")) { groupsAffected.Add("Field Spells"); groupsAffected.Add("All Spells"); }
            if (isASpell && type.Contains("Quick-Play")) { groupsAffected.Add("Quick-Play Spells"); groupsAffected.Add("All Spells"); }
            if (isASpell && type.Contains("Ritual")) { groupsAffected.Add("Ritual Spells"); groupsAffected.Add("All Spells"); }*/
            if (type.Contains("Normal Spell")) { groupsAffected.Add("Normal Spells"); groupsAffected.Add("All Spells"); }
            if (type.Contains("Continuous Spell")) { groupsAffected.Add("Continuous Spells"); groupsAffected.Add("All Spells"); }
            if (type.Contains("Equip Spell")) { groupsAffected.Add("Equip Spells"); groupsAffected.Add("All Spells"); }
            if (type.Contains("Field Spell")) { groupsAffected.Add("Field Spells"); groupsAffected.Add("All Spells"); }
            if (type.Contains("Quick-Play Spell")) { groupsAffected.Add("Quick-Play Spells"); groupsAffected.Add("All Spells"); }
            if (type.Contains("Ritual Spell")) { groupsAffected.Add("Ritual Spells"); groupsAffected.Add("All Spells"); }

            bool isATrap = false;
            if (type.Contains("Trap")) { isATrap = true; }
            if (isATrap && type.Contains("Normal")) { groupsAffected.Add("Normal Traps"); groupsAffected.Add("All Traps"); }
            if (isATrap && type.Contains("Continuous")) { groupsAffected.Add("Continuous Traps"); groupsAffected.Add("All Traps"); }
            if (isATrap && type.Contains("Counter")) { groupsAffected.Add("Counter Traps"); groupsAffected.Add("All Traps"); }

            /*int obtained = DataBase.GetListObtainedCount("Aqua");
            int obtainedAllMonster = DataBase.GetListObtainedCount("All Monsters");
            int obtainedAllCards = DataBase.GetListObtainedCount("All Cards");

            _rows[0].UpdateValues(obtained);
            _rows[23].UpdateValues(obtainedAllMonster);
            _rows[24].UpdateValues(obtainedAllMonster);
            _rows[27].UpdateValues(obtainedAllCards);*/

            for(int x = 0; x < groupsAffected.Count; x++)
            {
                int obtained = DataBase.GetListObtainedCount(groupsAffected[x]);

                int index = -1;
                switch (groupsAffected[x])
                {
                    case "Aqua": index = 0; break;
                    case "Beast": index = 1; break;
                    case "Beast-Warrior": index = 2; break;
                    case "Cyberse": index = 3; break;
                    case "Divine-Beast": index = 4; break;
                    case "Dinosaur": index = 5; break;
                    case "Dragon": index = 6; break;
                    case "Fairy": index = 7; break;
                    case "Fiend": index = 8; break;
                    case "Fish": index = 9; break;
                    case "Insect": index = 10; break;
                    case "Machine": index = 11; break;
                    case "Plant": index = 12; break;
                    case "Psychic": index = 13; break;
                    case "Pyro": index = 14; break;
                    case "Reptile": index = 15; break;
                    case "Rock": index = 16; break;
                    case "Sea Serpent": index = 17; break;
                    case "Spellcaster": index = 18; break;
                    case "Thunder": index = 19; break;
                    case "Warrior": index = 20; break;
                    case "Winged-Beast": index = 21; break;
                    case "Wyrm": index = 22; break;
                    case "Zombie": index = 23; break;
                    case "All Monsters": index = 24; break;
                    //case "All Monsters": index = 25; break;
                    //case "All Spells": index = 26; break;
                    //case "All Traps": index = 27; break;
                    //case "All Cards": index = 28; break;
                    case "Normal Spells": index = 29; break;
                    case "Continuous Spells": index = 30; break;
                    case "Equip Spells": index = 31; break;                       
                    case "Quick-Play Spells": index = 32; break;
                    case "Field Spells": index = 33; break;
                    case "Ritual Spells": index = 34; break;
                    case "All Spells": index = 35; break;
                    case "Normal Traps": index = 36; break;
                    case "Continuous Traps": index = 37; break;
                    case "Counter Traps": index = 38; break;
                    case "All Traps": index = 39; break;

                    case "Normal Monsters": index = 40; break;
                    case "Effect Monsters": index = 41; break;
                    case "Fusion Monsters": index = 42; break;
                    case "Ritual Monsters": index = 43; break;
                    case "Synchro Monsters": index = 44; break;
                    case "Xyz Monsters": index = 45; break;
                    case "Pendulum Monsters": index = 46; break;
                    case "Link Monsters": index = 47; break;
                    case "Flip Monsters": index = 48; break;
                    case "Spirit Monsters": index = 49; break;
                    case "Toon Monsters": index = 50; break;
                    case "Union Monsters": index = 51; break;
                    case "Tuner Monsters": index = 52; break;
                    case "Gemini Monsters": index = 53; break;
                }

                _rows[index].UpdateValues(obtained);

                if (groupsAffected[x] == "All Monsters") { _rows[25].UpdateValues(obtained); }
                if (groupsAffected[x] == "All Spells") { _rows[26].UpdateValues(obtained); }
                if (groupsAffected[x] == "All Traps") { _rows[27].UpdateValues(obtained); }
            }

            int obtainedAllCards = DataBase.GetListObtainedCount("All Cards");
            _rows[28].UpdateValues(obtainedAllCards);
        }
        public static void UpdateSetStat(string setName)
        {
            string group = DataBase.GetSetGroupOf(setName);
            int index = DataBase.GetSetCardIndex(setName);
            List<SetPack> sets = DataBase.GetSetsFromGroup(group);
            SetPack thispack = sets[index];

            switch(group)
            {
                case "Booster Packs":  _boosterRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Special Edition Boxes":  _spBoxesRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Starter Decks":  _starterDecksRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Structure Decks":  _structureDecksRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Tins":  _tinsRows[index].UpdateValues(thispack.CardsObtained); break;
                case "SPEED DUEL":  _spDuelRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Duelist Packs":  _DPRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Duel Terminal Cards":  _DTRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Others":  _otherRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Magazines, Books, Comics":  _MBCRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Tournaments":  _tournamentsRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Promotional Cards":  _promosRows[index].UpdateValues(thispack.CardsObtained); break;
                case "Video Game Bundles":  _videoGamesRows[index].UpdateValues(thispack.CardsObtained); break;
            }
            
        }


        private static List<MasterStatRow> _rows = new List<MasterStatRow>();
        private static List<SetStatRow> _boosterRows = new List<SetStatRow>();
        private static List<SetStatRow> _spBoxesRows = new List<SetStatRow>();
        private static List<SetStatRow> _starterDecksRows = new List<SetStatRow>();
        private static List<SetStatRow> _structureDecksRows = new List<SetStatRow>();
        private static List<SetStatRow> _tinsRows = new List<SetStatRow>();
        private static List<SetStatRow> _spDuelRows = new List<SetStatRow>();
        private static List<SetStatRow> _DPRows = new List<SetStatRow>();
        private static List<SetStatRow> _DTRows = new List<SetStatRow>();
        private static List<SetStatRow> _otherRows = new List<SetStatRow>();
        private static List<SetStatRow> _MBCRows = new List<SetStatRow>();
        private static List<SetStatRow> _promosRows = new List<SetStatRow>();
        private static List<SetStatRow> _tournamentsRows = new List<SetStatRow>();
        private static List<SetStatRow> _videoGamesRows = new List<SetStatRow>();

        public class MasterStatRow
        {
            public MasterStatRow(Label name, Label obtained, Label dash, Label total, Label percentage, ProgressBar bar)
            {
                _nameLabel = name;
                _obtainedLabel = obtained;
                _dashLabel = dash;
                _totalLabel = total;
                _percentageLabel = percentage;
                _bar = bar;
            }

            public void UpdateValues(int newObtained)
            {
                _obtainedLabel.Text = newObtained.ToString();
                int totalCount = Convert.ToInt32(_totalLabel.Text);

                double dObtained = (double)newObtained;
                double dTotal = (double)totalCount;
                double percentage = 0;
                if (totalCount > 0) { percentage = (dObtained / dTotal) * 100; }

                _percentageLabel.Text = ((int)percentage) + "%";
                _bar.Value = (int)percentage;

                //update coloring
                if (percentage <= 33)
                {
                    _percentageLabel.ForeColor = Color.Red;
                }
                else if (percentage <= 66)
                {
                    _percentageLabel.ForeColor = Color.Yellow;
                }
                else if (percentage <= 99)
                {
                    _percentageLabel.ForeColor = Color.Lime;
                }
                else
                {
                    _percentageLabel.ForeColor = Color.Aqua;
                }
            }

            private Label _nameLabel = new Label();
            private Label _obtainedLabel = new Label();
            private Label _dashLabel = new Label();
            private Label _totalLabel = new Label();
            private Label _percentageLabel = new Label();
            private ProgressBar _bar = new ProgressBar();
        }
        public class SetStatRow
        {
            public SetStatRow(Label code, Label year, Label name, Label obtained, Label dash, Label total, Label percentage, ProgressBar bar)
            {
                _codeLebel = code;
                _yearLebel = year;
                _nameLabel = name;
                _obtainedLabel = obtained;
                _dashLabel = dash;
                _totalLabel = total;
                _percentageLabel = percentage;
                _bar = bar;
            }

            public void UpdateValues(int newObtained)
            {
                _obtainedLabel.Text = newObtained.ToString();
                int totalCount = Convert.ToInt32(_totalLabel.Text);

                double dObtained = (double)newObtained;
                double dTotal = (double)totalCount;
                double percentage = 0;
                if (totalCount > 0) { percentage = (dObtained / dTotal) * 100; }

                _percentageLabel.Text = ((int)percentage) + "%";
                _bar.Value = (int)percentage;

                //update coloring
                if(percentage <= 33)
                {
                    _percentageLabel.ForeColor = Color.Red;
                }
                else if (percentage <= 66)
                {
                    _percentageLabel.ForeColor = Color.Yellow;
                }
                else if (percentage <= 99)
                {
                    _percentageLabel.ForeColor = Color.Lime;
                }
                else
                {
                    _percentageLabel.ForeColor = Color.Aqua;
                }
            }

            private Label _codeLebel = new Label();
            private Label _yearLebel = new Label();
            private Label _nameLabel = new Label();
            private Label _obtainedLabel = new Label();
            private Label _dashLabel = new Label();
            private Label _totalLabel = new Label();
            private Label _percentageLabel = new Label();
            private ProgressBar _bar = new ProgressBar();
        }
    }
}
