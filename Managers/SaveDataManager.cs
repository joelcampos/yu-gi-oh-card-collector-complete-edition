﻿//Joel Campos
//12/21/2021
//SaveDataManager Class

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public static class SaveDataManager
    {
        public static void ReadSaveFile()
        {
            StreamReader SR_SaveFile = new StreamReader(
                Directory.GetCurrentDirectory() + "\\Save Data\\Save File.txt");

            //String that hold the data of one line of the txt file
            string line = "";

            //Read the first line for the total cards registered
            line = SR_SaveFile.ReadLine();
            int cardCount = Convert.ToInt32(line);

            _cardsSaved.Add(line);

            for(int x = 0; x < cardCount; x++)
            {
                line = SR_SaveFile.ReadLine();

                if (line == null) { throw new Exception("Save file was tried to be read for more cards that it contains, verify the amount in the first line of savedata.txt"); }

                //Separator used by Split()
                string[] separator = new string[] { "|" };
                //Array of tokens (Size will be rewritten when Split() is called)
                string[] tokens = new string[1];
                //Split the data
                tokens = line.Split(separator, StringSplitOptions.None);

                //Format: "CODE-EN001|00000000|Card Name|Card Type

                string code = tokens[0];
                string id = tokens[1];

                //find the card
                CardObject thisCardObject = DataBase.GetCardObjectWithID(id);
                thisCardObject.FlipObtainedStatus(code);

                //re add the line
                _cardsSaved.Add(line);
            }

            SR_SaveFile.Close();

            /*StreamReader SR_SaveFile2 = new StreamReader(
                Directory.GetCurrentDirectory() + "\\Save Data\\tags.txt");
            string line2 = "";
            line2 = SR_SaveFile2.ReadLine();
            int cardCount2 = Convert.ToInt32(line2);

            List<string> newsavedatalist = new List<string>();
            newsavedatalist.Add(line2);

            for (int x = 0; x < cardCount2; x++)
            {
                line2 = SR_SaveFile2.ReadLine();
                string[] separator = new string[] { "|" };
                string[] tokens = new string[1];
                tokens = line2.Split(separator, StringSplitOptions.None);
                string code = tokens[0];
                CardObject thisCard = DataBase.GetCardObjectWithSetCode(code);
                if(thisCard != null)
                {
                    string data = code + "|" + thisCard.ID + "|" + thisCard.Name + "|" + thisCard.Type;
                    newsavedatalist.Add(data);
                }
            }

            File.WriteAllLines(Directory.GetCurrentDirectory() + "\\Save Data\\New Save File.txt", newsavedatalist);*/
        }
        public static void WriteSaveFile()
        {
            File.WriteAllLines(Directory.GetCurrentDirectory() + "\\Save Data\\Save File.txt", _cardsSaved);
        }

        public static void AddCard(CardObject thisCard, string code)
        {
            _cardsSaved.Add(code + "|" +thisCard.ID + "|" + thisCard.Name + "|" + thisCard.Type);
            _cardsSaved[0] = (_cardsSaved.Count - 1).ToString();
        }

        public static void RemoveCard(CardObject thisCard, string code)
        {
            _cardsSaved.Remove(code + "|" + thisCard.ID + "|" + thisCard.Name + "|" + thisCard.Type);
            _cardsSaved[0] = (_cardsSaved.Count - 1).ToString();
        }

        private static List<string> _cardsSaved = new List<string>();
    }
}