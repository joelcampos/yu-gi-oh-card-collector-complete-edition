﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public partial class ValueReport : Form
    {
        public ValueReport(Collector mainform)
        {
            InitializeComponent();
            refMainForm = mainform;

            double totalMarketObtained = 0;
            double totalMarketALL = 0;
            double totalMedianObtained = 0;
            double totalMedianALL = 0;
            for (int x = 0; x < DataBase.MainList.Count; x++)
            {
                CardObject thiscard = DataBase.MainList[x];

                for(int y = 0; y < thiscard.Sets.Count; y++)
                {
                    totalMarketALL += thiscard.Sets[y].MarketPrice;
                    totalMedianALL += thiscard.Sets[y].MedianPrice;
                    _totalmarketprices.Add(new PriceItem(thiscard.Name, thiscard.Sets[y].Code, thiscard.Sets[y].Rarity, thiscard.Sets[y].Name, thiscard.Sets[y].MarketPrice));
                    _totalmedianprices.Add(new PriceItem(thiscard.Name, thiscard.Sets[y].Code, thiscard.Sets[y].Rarity, thiscard.Sets[y].Name, thiscard.Sets[y].MedianPrice));

                    if(thiscard.Sets[y].Obtained)
                    {
                        totalMarketObtained += thiscard.Sets[y].MarketPrice;
                        totalMedianObtained += thiscard.Sets[y].MedianPrice;
                        _obtainedmarketprices.Add(new PriceItem(thiscard.Name, thiscard.Sets[y].Code, thiscard.Sets[y].Rarity, thiscard.Sets[y].Name, thiscard.Sets[y].MarketPrice));
                        _obtainedmedianprices.Add(new PriceItem(thiscard.Name, thiscard.Sets[y].Code, thiscard.Sets[y].Rarity, thiscard.Sets[y].Name, thiscard.Sets[y].MedianPrice));
                    }
                }
            }

            lblMarketTotalValue.Text = "$" + totalMarketObtained.ToString();
            lblMarketTotalValueALL.Text = "$" + totalMarketALL.ToString();

            lblMedianCollection.Text = "$" + totalMedianObtained.ToString();
            lblMedianALL.Text = "$" + totalMedianALL.ToString();

            _obtainedmarketprices.Sort();
            _obtainedmedianprices.Sort();
            _totalmarketprices.Sort();
            _totalmedianprices.Sort();

            //Fill the lists
            for(int x = 0; x < _obtainedmarketprices.Count; x++)
            {
                string Codetab = "\t";
                if (_obtainedmarketprices[x].Code.Length < 8) { Codetab = "\t\t"; }
                string nametab = "\t\t\t";
                //if (_obtainedmarketprices[x].Name.Length >= 15) { nametab = "\t\t\t"; }
                if (_obtainedmarketprices[x].Name.Length >= 19) { nametab = "\t\t"; }
                if (_obtainedmarketprices[x].Name.Length <= 8) { nametab = "\t\t\t\t"; }
                string line = "$" + _obtainedmarketprices[x].PriceString + "\t" + _obtainedmarketprices[x].Code + Codetab + _obtainedmarketprices[x].Name + nametab + _obtainedmarketprices[x].SetName + "\t" + _obtainedmarketprices[x].Rarity;
                listMarketPricelist.Items.Add(line);
            }

            for (int x = 0; x < _obtainedmedianprices.Count; x++)
            {
                string Codetab = "\t";
                if (_obtainedmedianprices[x].Code.Length < 8) { Codetab = "\t\t"; }
                string nametab = "\t\t\t";
                //if (_obtainedmarketprices[x].Name.Length >= 15) { nametab = "\t\t\t"; }
                if (_obtainedmedianprices[x].Name.Length >= 19) { nametab = "\t\t"; }
                if (_obtainedmedianprices[x].Name.Length <= 8) { nametab = "\t\t\t\t"; }
                string line = "$" + _obtainedmedianprices[x].PriceString + "\t" + _obtainedmedianprices[x].Code + Codetab + _obtainedmedianprices[x].Name + nametab + _obtainedmedianprices[x].SetName + "\t" + _obtainedmedianprices[x].Rarity;
                listMedianPricelist.Items.Add(line);
            }
        }

        #region Internal System Functions
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            refMainForm.ReenableReportButton();
        }
        #endregion

        private Collector refMainForm = null;
        private List<PriceItem> _obtainedmarketprices = new List<PriceItem>();
        private List<PriceItem> _obtainedmedianprices = new List<PriceItem>();
        private List<PriceItem> _totalmarketprices = new List<PriceItem>();
        private List<PriceItem> _totalmedianprices = new List<PriceItem>();

        public class PriceItem : IComparable
        {
            public PriceItem(string name, string code, string rarity, string setName, double price)
            {
                _cardName = name;
                _cardCode = code;
                _cardRarity = rarity;
                _cardSetname = setName;
                _price = price;

            }

            public string Name { get { return _cardName; } }
            public string Code { get { return _cardCode; } }
            public string Rarity { get { return _cardRarity; } }
            public string SetName { get { return _cardSetname; } }
            public string PriceString { get { return _price.ToString(); } }

            private string _cardName = "";
            private string _cardCode = "";
            private string _cardRarity = "";
            private string _cardSetname = "";
            private double _price = 0;

            public int CompareTo(object obj)
            {
                PriceItem otherPriceItem = obj as PriceItem;
                //return _price.CompareTo(otherPriceItem._price);
                return otherPriceItem._price.CompareTo(_price);
            }
        }
    }
}
