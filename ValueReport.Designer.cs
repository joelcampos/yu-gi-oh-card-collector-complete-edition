﻿
namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    partial class ValueReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValueReport));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TabGeneralMarketPrice = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMarketTotalValue = new System.Windows.Forms.Label();
            this.listMarketPricelist = new System.Windows.Forms.ListBox();
            this.lblMarketTotalValueALL = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMedianALL = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listMedianPricelist = new System.Windows.Forms.ListBox();
            this.lblMedianCollection = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.TabGeneralMarketPrice.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TabGeneralMarketPrice);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 426);
            this.tabControl1.TabIndex = 0;
            // 
            // TabGeneralMarketPrice
            // 
            this.TabGeneralMarketPrice.BackColor = System.Drawing.Color.Black;
            this.TabGeneralMarketPrice.Controls.Add(this.lblMarketTotalValueALL);
            this.TabGeneralMarketPrice.Controls.Add(this.label3);
            this.TabGeneralMarketPrice.Controls.Add(this.listMarketPricelist);
            this.TabGeneralMarketPrice.Controls.Add(this.lblMarketTotalValue);
            this.TabGeneralMarketPrice.Controls.Add(this.label1);
            this.TabGeneralMarketPrice.Location = new System.Drawing.Point(4, 22);
            this.TabGeneralMarketPrice.Name = "TabGeneralMarketPrice";
            this.TabGeneralMarketPrice.Padding = new System.Windows.Forms.Padding(3);
            this.TabGeneralMarketPrice.Size = new System.Drawing.Size(768, 400);
            this.TabGeneralMarketPrice.TabIndex = 0;
            this.TabGeneralMarketPrice.Text = "Market Price";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Black;
            this.tabPage2.Controls.Add(this.lblMedianALL);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.listMedianPricelist);
            this.tabPage2.Controls.Add(this.lblMedianCollection);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(768, 400);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Median Price";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Total Cost of Collection:";
            // 
            // lblMarketTotalValue
            // 
            this.lblMarketTotalValue.AutoSize = true;
            this.lblMarketTotalValue.BackColor = System.Drawing.Color.Transparent;
            this.lblMarketTotalValue.ForeColor = System.Drawing.Color.White;
            this.lblMarketTotalValue.Location = new System.Drawing.Point(154, 8);
            this.lblMarketTotalValue.Name = "lblMarketTotalValue";
            this.lblMarketTotalValue.Size = new System.Drawing.Size(88, 13);
            this.lblMarketTotalValue.TabIndex = 1;
            this.lblMarketTotalValue.Text = "$1000000000.00";
            // 
            // listMarketPricelist
            // 
            this.listMarketPricelist.BackColor = System.Drawing.Color.Black;
            this.listMarketPricelist.ForeColor = System.Drawing.Color.White;
            this.listMarketPricelist.FormattingEnabled = true;
            this.listMarketPricelist.Location = new System.Drawing.Point(8, 41);
            this.listMarketPricelist.Name = "listMarketPricelist";
            this.listMarketPricelist.Size = new System.Drawing.Size(754, 355);
            this.listMarketPricelist.TabIndex = 2;
            // 
            // lblMarketTotalValueALL
            // 
            this.lblMarketTotalValueALL.AutoSize = true;
            this.lblMarketTotalValueALL.BackColor = System.Drawing.Color.Transparent;
            this.lblMarketTotalValueALL.ForeColor = System.Drawing.Color.White;
            this.lblMarketTotalValueALL.Location = new System.Drawing.Point(155, 25);
            this.lblMarketTotalValueALL.Name = "lblMarketTotalValueALL";
            this.lblMarketTotalValueALL.Size = new System.Drawing.Size(88, 13);
            this.lblMarketTotalValueALL.TabIndex = 4;
            this.lblMarketTotalValueALL.Text = "$1000000000.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Total Cost of DB";
            // 
            // lblMedianALL
            // 
            this.lblMedianALL.AutoSize = true;
            this.lblMedianALL.BackColor = System.Drawing.Color.Transparent;
            this.lblMedianALL.ForeColor = System.Drawing.Color.White;
            this.lblMedianALL.Location = new System.Drawing.Point(156, 23);
            this.lblMedianALL.Name = "lblMedianALL";
            this.lblMedianALL.Size = new System.Drawing.Size(88, 13);
            this.lblMedianALL.TabIndex = 9;
            this.lblMedianALL.Text = "$1000000000.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(7, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Total Cost of DB";
            // 
            // listMedianPricelist
            // 
            this.listMedianPricelist.BackColor = System.Drawing.Color.Black;
            this.listMedianPricelist.ForeColor = System.Drawing.Color.White;
            this.listMedianPricelist.FormattingEnabled = true;
            this.listMedianPricelist.Location = new System.Drawing.Point(9, 39);
            this.listMedianPricelist.Name = "listMedianPricelist";
            this.listMedianPricelist.Size = new System.Drawing.Size(754, 355);
            this.listMedianPricelist.TabIndex = 7;
            // 
            // lblMedianCollection
            // 
            this.lblMedianCollection.AutoSize = true;
            this.lblMedianCollection.BackColor = System.Drawing.Color.Transparent;
            this.lblMedianCollection.ForeColor = System.Drawing.Color.White;
            this.lblMedianCollection.Location = new System.Drawing.Point(155, 6);
            this.lblMedianCollection.Name = "lblMedianCollection";
            this.lblMedianCollection.Size = new System.Drawing.Size(88, 13);
            this.lblMedianCollection.TabIndex = 6;
            this.lblMedianCollection.Text = "$1000000000.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(6, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Total Cost of Collection:";
            // 
            // ValueReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ValueReport";
            this.Text = "Colection Value Report - Prices from TCGPlayer.com";
            this.tabControl1.ResumeLayout(false);
            this.TabGeneralMarketPrice.ResumeLayout(false);
            this.TabGeneralMarketPrice.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TabGeneralMarketPrice;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox listMarketPricelist;
        private System.Windows.Forms.Label lblMarketTotalValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMarketTotalValueALL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMedianALL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listMedianPricelist;
        private System.Windows.Forms.Label lblMedianCollection;
        private System.Windows.Forms.Label label6;
    }
}