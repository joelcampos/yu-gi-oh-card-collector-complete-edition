﻿
namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    partial class SetSearcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetSearcher));
            this.lblCodeSearchOutput = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblCodeSearch = new System.Windows.Forms.Label();
            this.txtCodeSearch = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblCodeSearchOutput
            // 
            this.lblCodeSearchOutput.BackColor = System.Drawing.Color.Black;
            this.lblCodeSearchOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodeSearchOutput.Font = new System.Drawing.Font("Corbel", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodeSearchOutput.ForeColor = System.Drawing.Color.White;
            this.lblCodeSearchOutput.Location = new System.Drawing.Point(12, 41);
            this.lblCodeSearchOutput.Name = "lblCodeSearchOutput";
            this.lblCodeSearchOutput.Size = new System.Drawing.Size(383, 170);
            this.lblCodeSearchOutput.TabIndex = 265;
            this.lblCodeSearchOutput.Text = "Enter a Set CODE and hit search";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(166, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 25);
            this.button1.TabIndex = 264;
            this.button1.Text = "Code Search";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblCodeSearch
            // 
            this.lblCodeSearch.AutoSize = true;
            this.lblCodeSearch.BackColor = System.Drawing.Color.Transparent;
            this.lblCodeSearch.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodeSearch.ForeColor = System.Drawing.Color.Yellow;
            this.lblCodeSearch.Location = new System.Drawing.Point(15, 9);
            this.lblCodeSearch.Name = "lblCodeSearch";
            this.lblCodeSearch.Size = new System.Drawing.Size(40, 16);
            this.lblCodeSearch.TabIndex = 263;
            this.lblCodeSearch.Text = "Code:";
            // 
            // txtCodeSearch
            // 
            this.txtCodeSearch.Location = new System.Drawing.Point(59, 7);
            this.txtCodeSearch.Name = "txtCodeSearch";
            this.txtCodeSearch.Size = new System.Drawing.Size(101, 20);
            this.txtCodeSearch.TabIndex = 262;
            // 
            // SetSearcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(407, 220);
            this.Controls.Add(this.lblCodeSearchOutput);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblCodeSearch);
            this.Controls.Add(this.txtCodeSearch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetSearcher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Card Collector: Set Search Tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCodeSearchOutput;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblCodeSearch;
        private System.Windows.Forms.TextBox txtCodeSearch;
    }
}