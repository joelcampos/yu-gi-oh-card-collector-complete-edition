*** Setting ***
Library				Selenium2Library
Library				String
Library				Collections
Library				OperatingSystem
Library 			DateTime

Suite Setup 		YGO_Setup
# Suite Teardown		Close All Browsers

Test Setup			YGO_Go to Card Search
# Test Teardown  	 	YGO_Go to Card Search


*** Variables ***
${CARDLISTURL}								https://www.db.yugioh-card.com/yugiohdb/card_list.action
${CARDSEARCHURL}							https://www.db.yugioh-card.com/yugiohdb/card_search.action
${CURRENT_PAGE}								1
${CURRENT_FILTER_RESULTS}					0
${MASTER_CARD_COUNT}						0
${FILTER_CARD_COUNT}						0

${ARGPAGE}
${ARGNUMBER}								0
${FILE_PATH}				${EXECDIR}/database/Normal Traps.txt

*** Test Case ***
TC-YGO-001 Scan Each Type
	####
	# @{MonsterTypes}=		Create List		Aqua	Beast	Beast-Warrior	Cyberse	Dinosaur	Divine-Beast	Dragon	Fairy	Fiend	Insect	Fish	Machine	Plant	Psychic	Pyro	Reptile	Rock	Sea Serpent	Spellcaster	Thunder	Warrior	Winged Beast	Wyrm	Zombie
	@{MonsterTypes}=		Create List		Divine-Beast			#Insect	Fish	Machine	Plant	Psychic	Pyro	Reptile	Rock	Sea Serpent	Spellcaster	Thunder	Warrior	Winged Beast	Wyrm	Zombie
	
	# ${type}					Set Variable	Aqua
	@{MasterCardList}=			Create List
	:FOR	${type}	IN		@{MonsterTypes}
	\	Click Monster Type			${type}
	\	Click Search
	\	Click View 100 Items
	\	Set Current Filter Total Results
	\	${FilterCardList}=	Scan Entire Filter Group		${type}
	\	@{MasterCardList}=	Combine Lists		${MasterCardList}		${FilterCardList}
	\	YGO_Go to Card Search
	\	Log To Console		Filter Group ${type} Finished
	
	#//Create the master card list file
	${filestring}=			Set Variable			${MASTER_CARD_COUNT}\n
	:FOR	${cardinfo}	IN		@{MasterCardList}
	\		${filestring}=		Set Variable		${filestring}${cardinfo}\n
	
	Create File				${EXECDIR}/MasterCardList.txt		${filestring}
	#####
	
TC-YGO-002 Scan prices
	####
	${FILE_CONTENT}=		Get File								${FILE_PATH}
	@{LINES}=				Split To Lines							${FILE_CONTENT}
	
	${cardcount}=			Convert To Integer	${LINES}[0]
	${loopIterations}=		Evaluate			${cardcount} + 1
	
	@{NewCardList}=			Create List
	
	:FOR	${i}	IN RANGE		1						${loopIterations}
	\		@{LineItems}=			Split String		${LINES}[${i}]		|
	\		${LineNoPrices}=		Get Line Without Prices				${LineItems}
	\		@{LineItemsNoPrices}=	Split String		${LineNoPrices}		|
	\		@{LineItemsNoPrices}=	Split String		${LineNoPrices}		|
	\		${cardname}=			Set Variable		${LineItems}[1]
	\		${cardname}= 			Replace String		${cardname}			"		${EMPTY}
	\		${nameUpdated}=			Run Keyword And Return Status		Should Contain		${cardname}		(Updated from:
	\		${cardname}=			Run Keyword If		${nameUpdated}	Fetch From Left		${cardname}			${space}(Updated from:		ELSE		Set Variable	${cardname}
	\		${prices}=				ProDeck_Get All Set Prices			${LineItemsNoPrices}	${cardname}
	\		${NEWLINE}=				Set Variable			${LineNoPrices}|${prices}
	\		Append To List			${NewCardList}			${NEWLINE}
	\		Create Price List		${NewCardList}			${cardcount}
	#####

TC-YGO-003 Scan Sets
	####
	Go To				https://www.db.yugioh-card.com/yugiohdb/card_list.action
	Wait Until Element Is Visible		//h1[.='Card Lists']
	
	#//Do the products
	Click Element				//span[.='Products']
	Sleep						3s
	
	@{XpathsList}=					Create List
	Append To List					${XpathsList}			//div[@id="list_title_1"]
	Append To List					${XpathsList}			//div[@id="list_title_2"]
	Append To List					${XpathsList}			//div[@id="list_title_3"]
	Append To List					${XpathsList}			//div[@id="list_title_4"]
	Append To List					${XpathsList}			//div[@id="list_title_5"]
	Append To List					${XpathsList}			//div[@id="list_title_20"]
	Append To List					${XpathsList}			//div[@id="list_title_60"]
	Append To List					${XpathsList}			//div[@id="list_title_70"]
	Append To List					${XpathsList}			//div[@id="list_title_80"]
	
	@{nameList}=					Create List
	Append To List					${nameList}				Booster Packs
	Append To List					${nameList}				Special Edition Boxes
	Append To List					${nameList}				Starter Decks
	Append To List					${nameList}				Structure Decks
	Append To List					${nameList}				Tins
	Append To List					${nameList}				SPEED DUEL
	Append To List					${nameList}				Duelist Packs
	Append To List					${nameList}				Duel Termina Cards
	Append To List					${nameList}				Others
	
	
	:FOR	${i}	IN RANGE		0		9
	\				Scan Set Group			${XpathsList}[${i}]				${nameList}[${i}]
	
	#//Do the bundles
	Click Element				//span[.='Perks/Bundles']
	Sleep						3s
	
	@{XpathsList2}=					Create List
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][1]
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][2]
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][3]
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][4]
	
	@{nameList2}=					Create List
	Append To List					${nameList2}				Magazines, Books, Comics
	Append To List					${nameList2}				Tournaments
	Append To List					${nameList2}				Promotional Cards
	Append To List					${nameList2}				Video Game Bundles
	
	:FOR	${i}	IN RANGE		0		4
	\				Scan Set Group2			${XpathsList2}[${i}]				${nameList2}[${i}]
	#####

TC-YGO-004 Scan IDs
	####
	${FILE_CONTENT}=		Get File								${FILE_PATH}
	@{LINES}=				Split To Lines							${FILE_CONTENT}
	
	${cardcount}=			Convert To Integer	${LINES}[0]
	${loopIterations}=		Evaluate			${cardcount} + 1
	
	@{NewCardList}=			Create List
	
	:FOR	${i}	IN RANGE		1		${loopIterations}
	\		@{LineItems}=			Split String		${LINES}[${i}]		|
	\		${cardname}=			Set Variable		${LineItems}[0]
	\		${sets}=				Set Variable		${LineItems}[7]
	\		${cardname}=			Replace String		${cardname}			"		${EMPTY}
	\		${cardname}=			Run Keyword if		"${cardname}"=="Slime Toad (Updated from: Frog the Jam)"		Set Variable	Slime Toad		ELSE		Set Variable		${cardname}
	\		${cardname}=			Run Keyword if		"${cardname}"=="Vampiric Koala (Updated from: Vampire Koala)"		Set Variable	Vampiric Koala		ELSE		Set Variable		${cardname}
	\		Go To					https://db.ygoprodeck.com/
	\		${nameUpdated}=			Run Keyword And Return Status		Should Contain		${cardname}		(Updated from:
	\		${cardname}=			Run Keyword If		${nameUpdated}	Fetch From Left		${cardname}			${space}(Updated from:		ELSE		Set Variable	${cardname}
	\		${SearchSuccess}=		Run Keyword And Return Status		ProDeck_Search Card		${cardname}
	\		Run Keyword If			${SearchSuccess}		Sleep					1s
	\		Run Keyword If			${SearchSuccess}		Wait Until Element Is Visible		//span[@class="card-set-id"]
	\		${cardid}=				Run Keyword If			${SearchSuccess}		Get Text		//span[@class="card-set-id"]			ELSE	Set Variable		null
	\		${cardid}=				Run Keyword If			${SearchSuccess}		Fetch From Right		${cardid}		:${space}		ELSE	Set Variable		null
	\		${cardid}=				Run Keyword If			${SearchSuccess}		Fetch From Right		${cardid}		:${space}		ELSE	Set Variable		null
	\		${prices}=				Get Zero Prices		${sets}
	\		${NEWLINE}=			Set Variable			${cardid}|${LINES}[${i}]|${prices}
	\		Append To List		${NewCardList}			${NEWLINE}
	\		Create Price List	${NewCardList}			${cardcount}
	#####

*** Keywords ***
Get Zero Prices
	####
	[Arguments]							${sets}
	${prices}=			Set Variable	${EMPTY}
	:FOR	${i}	IN RANGE		0		${sets}
	\		${prices}=				Set Variable		${prices}|$0.00|$0.00
	[return]						${prices}
	#####
	
Get Line Without Prices
	####
	[Arguments]			${LineItems}
	${setsAmount}=			Set Variable		${LineItems}[8]
	${lastIndex}=			Evaluate		${setsAmount}*4
	${lastIndex}=			Evaluate		9 + ${lastIndex}
	
	${returnString}=		Set Variable	${LineItems}[0]
	
	:FOR	${i}	IN RANGE		1		${lastIndex}
	\		${returnString}=		Set Variable	${returnString}|${LineItems}[${i}]
	
	[return]						${returnString}
	#####

YGO_Setup
	####
	Open Browser
	#//Change to view as list
	#Click Monster Type			Aqua
	#Click Search
	#Click View As List
	#####
	
Open Browser
	####
	Set Selenium Timeout				15
	${chrome_options}=    				Evaluate    sys.modules['selenium.webdriver.chrome.options'].Options()    sys, selenium.webdriver.chrome.options
    Create Webdriver    				Chrome
	Go To								${CARDSEARCHURL}
	Set Window Position					${WIN_POS}		0
	Maximize Browser Window
	#####
	
YGO_Go to Card Search
	####
	Go To								${CARDSEARCHURL}
	#####
	
Click Monster Type
	####
	[Arguments]							${type}
	Run Keyword If		"${type}"=="Warrior"						Click Element		//a[@class="species button_off species_4_en"]
	Run Keyword If		"${type}"=="Beast"							Click Element		//a[@class="species button_off species_6_en"]
	Run Keyword If		"${type}"!="Warrior" and "${type}"!="Beast"	Click Element		//a[contains(text(), "${type}")]
	#####
	
Click Search
	####
	Click Element						//a/b[.='Search']
	Wait Until Element Is Visible		//strong[contains(text(), "Results")]
	#####
	
Click View As List
	####
	Click Element		//span[.='View as Gallery']
	Click Element		//ul/li/a[.='View as List']
	Sleep				3s
	#####
	
Click View 100 Items
	####
	Click Element		//span[.='Show 10 items per page.']
	Click Element		//ul/li/a[.='Show 100 items per page.']
	Sleep				3s
	#####
	
Extract Result Amount
	####
	[Arguments]			${string}
	${extracted}=		Fetch From Right		${string}		of
	${int}=				Convert To Integer		${extracted}
	[Return]			${int}
	#####
	
Is This The Last Page
	####
	${x100}=			Evaluate		${CURRENT_PAGE} * 100
	${bool}=			Evaluate		${x100} > ${CURRENT_FILTER_RESULTS}
	[Return]			${bool}
	#####
	
Get Last Page Result Amount
	####
	${x100}=			Evaluate		${CURRENT_PAGE} * 100
	${x100}=			Evaluate		${x100} - 100
	${rem}=				Evaluate		${CURRENT_FILTER_RESULTS} - ${x100}
	${rem}=				Evaluate		${rem} + 2
	[Return]			${rem}
	#####
	
Set Current Filter Total Results
	####
	#//Set the current filter Total Results
	${results}=			Get Text					//div[@class="page_num_title"]/div/strong
	${results}=			Extract Result Amount		${results}
	Set Suite Variable	${CURRENT_FILTER_RESULTS}	${results}
	#####
	
Scan Current Page
	####
	[Arguments]			${CardList}		${CurrentPageCards}		${Group}
	
	# ${StartIndex}=		Run Keyword if		${ARGNUMBER}!=1		Set Variable	${ARGNUMBER}	ELSE	Set Variable	1
	# ${StartIndex}=		Evaluate			${StartIndex} + 1
	
	# Set Suite Variable		${ARGNUMBER}		1
	
	:FOR	${i}	IN RANGE		2	${CurrentPageCards}
	\		${name}=				Get Text			//div[@id="search_result"]//table/tbody/tr[${i}]/td[2]/b	
	\		${attribute}=			Get Text			//div[@id="search_result"]//table/tbody/tr[${i}]/td[3]	
	\		${type}=				Get Text			//div[@id="search_result"]//table/tbody/tr[${i}]/td[4]	
	\		${levelranklinl}=		Get Text			//div[@id="search_result"]//table/tbody/tr[${i}]/td[5]	
	\		${atk}=					Get Text			//div[@id="search_result"]//table/tbody/tr[${i}]/td[6]	
	\		${def}=					Get Text			//div[@id="search_result"]//table/tbody/tr[${i}]/td[7]	
	\		${pend}=				Get Text			//div[@id="search_result"]//table/tbody/tr[${i}]/td[8]
	#//Click on the card and extract all the sets this card belongs to.
	\		Click Element			//div[@id="search_result"]//table/tbody/tr[${i}]
	\		${maintab}=				Select Window			NEW
	# \		Wait Until Element Is Visible		//h1[contains(text(), "${name}")]
	\		Sleep					1s
	\		${setlistString}		${codesList}=			Extract Card Set List
	\		Select Window			${maintab}
	# \		${pricelist}=			Extract Card Set Prices		${name}		${codesList}
	# \		${thiscard}=			Set Variable		${name}|${attribute}|${type}|${levelranklinl}|${atk}|${def}|${pend}|${setlistString}|${pricelist}
	\		${thiscard}=			Set Variable		${name}|${attribute}|${type}|${levelranklinl}|${atk}|${def}|${pend}|${setlistString}
	\		Append To List								${CardList}		${thiscard}
	\		${tmp}=					Evaluate			${MASTER_CARD_COUNT} + 1
	\		Set Suite Variable		${MASTER_CARD_COUNT}	${tmp}
	\		Create Group File		${CardList}			${Group}
	# \		Log To Console			Card Scanned: 		${FILTER_CARD_COUNT}/${CURRENT_FILTER_RESULTS}
	
	[Return]				${CardList}
	#####
	
Click Next Page
	####
	Click Element		//span/a[.='»']
	Sleep				3s
	#//change page #
	${tmp}=	Evaluate		${CURRENT_PAGE} + 1
	Set Suite Variable		${CURRENT_PAGE}			${tmp}	
	#####
	
Scan Entire Filter Group
	####
	[Arguments]			${type}
	@{CardList}=						Create List
	:FOR	${i}	IN RANGE		1	20
	\		${IsLastPage}=				Is This The Last Page
	\		${CurrentPageCards}=		Run Keyword If		${IsLastPage}	Get Last Page Result Amount		ELSE		Set Variable	102
	\		${CardList}=				Scan Current Page				${CardList}			${CurrentPageCards}			${type}
	# \		Create Group File			${CardList}
	\		Run Keyword If				${IsLastPage}==False		Click Next Page
	\		Exit For Loop If			${IsLastPage}
	
	
	# ${ScanAmountMatched}=				Evaluate		${MASTER_CARD_COUNT}==${CURRENT_FILTER_RESULTS}
	# Run Keyword If 						${ScanAmountMatched}==False		Fail		Scanned cards didnt match the filter result.
	
	# ${filestring}=			Set Variable			${CURRENT_FILTER_RESULTS}\n
	# :FOR	${cardinfo}	IN		@{CardList}
	# \		${filestring}=		Set Variable		${filestring}${cardinfo}\n
	
	# Create File				${EXECDIR}/${type}.txt		${filestring}
	
	#//Clear vars
	Set Suite Variable		${CURRENT_PAGE}				1
	Set Suite Variable		${CURRENT_FILTER_RESULTS}	0
	Set Suite Variable		${FILTER_CARD_COUNT}		0
	[Return]				${CardList}
	#####
	
Create Group File
	####
	[Arguments]				${CardList}				${type}
	${filestring}=			Set Variable			${CURRENT_FILTER_RESULTS}\n
	:FOR	${cardinfo}	IN		@{CardList}
	\		${filestring}=		Set Variable		${filestring}${cardinfo}\n
	
	Create File				${EXECDIR}/${type}.txt		${filestring}
	#####
	
Extract Card Set List
	####
	${rows}=			Get Matching Xpath Count		//div[@id="pack_list"]//tbody/tr
	${setsRealAmount}	Evaluate				${rows} - 1
	${rows}=			Evaluate				${rows} + 1
	@{SetList}=			Create List			
	@{CodesList}=		Create List			
	:FOR	${i}	IN RANGE		2	${rows}
	\		${date}=		Get Text			//div[@id="pack_list"]//tbody/tr[${i}]/td[1]
	\		${code}=		Get Text			//div[@id="pack_list"]//tbody/tr[${i}]/td[2]
	\		${set}=			Get Text			//div[@id="pack_list"]//tbody/tr[${i}]/td[3]/b
	\		${HasRarity}=		Run Keyword And Return Status		Get Element Attribute			//div[@id="pack_list"]//tbody/tr[${i}]/td[4]/img@alt
	\		${rarity}=		Run Keyword If		${HasRarity}		Get Element Attribute			//div[@id="pack_list"]//tbody/tr[${i}]/td[4]/img@alt	ELSE	Set Variable	Common
	\		${setstring}=	Set Variable		${date}|${code}|${set}|${rarity}
	\		Append To List			${SetList}		${setstring}
	\		Append To List			${CodesList}	${code}
	
	${completestring}=			Set Variable			${setsRealAmount}
	@{SetListStringList}=		Create List				${setsRealAmount}
	:FOR	${line}	IN		@{SetList}
	\		${completestring}=		Set Variable				${completestring}|${line}
	\		Append To List			${SetListStringList}		${line}

	close window
	[Return]				${completestring}		${CodesList}
	#####
	
Extract Card Set Prices
	####
	[Arguments]		${name}		${setcodelist}
	
	${setsAmount}=		Get Length		${setcodelist}
	
	Execute Javascript    window.open('')
	${maintab}=				Select Window			NEW
	Go To					https://www.tcgplayer.com/
	
	@{priceslist}		Create List
	
	${MarketPrice}=		Set Variable	$0.00
	${MedianPrice}=		Set Variable	$0.00
	
	:FOR	${code}	IN		@{setcodelist}
	# :FOR	${i}	IN RANGE		0	${setsAmount}
	\		Input Text				//input[@name="acInput"]		${name}
	\		Sleep					2s
	\		Click Element			//a[@class="autocomplete-rec autocomplete-rec--product-line"]/div/div[.=" in YuGiOh "]
	\		Sleep					2s
	\		Mouse Over				//input[@name="acInput"]
	\		${MultipleSetResults}=		Run Keyword And Return Status	Page Should Contain Element			//h1[@class="search-result-count"]
	\		${MarketPrice}=		Run Keyword If		${MultipleSetResults}==False		Get Text				//ul[@class="price-points__rows"]/li[1]/span[@class="price"]
	\		${MedianPrice}=		Run Keyword If		${MultipleSetResults}==False		Get Text				//ul[@class="price-points__rows"]/li[3]/span[@class="price"]
	\		Run Keyword If		${MultipleSetResults}==False		Append To List		${priceslist}		${MarketPrice}
	\		Run Keyword If		${MultipleSetResults}==False		Append To List		${priceslist}		${MedianPrice}
	\		Exit For Loop If		${MultipleSetResults}==False
	#//Else scan the results page for the result with the code
	\		${MarketPrice}		${MedianPrice}=		Find Card Set In TCGPlayer			${code}
	\		Append To List		${priceslist}		${MarketPrice}
	\		Append To List		${priceslist}		${MedianPrice}
	
	
	${pricesting}=			Set Variable			${EMPTY}
	:FOR	${line}	IN		@{priceslist}
	\		${pricesting}=		Set Variable		${pricesting}|${line}
	
	close window
	Select Window			${maintab}
	[Return]				${pricesting}	
	#####
	
Find Card Set In TCGPlayer
	####
	[Arguments]		${code}
	${currentPage}=			Set Variable						1
	${pagesamount}=			Get Matching Xpath Count			//div[@class="search-layout__pagination"]//button

	${MarketPrice}=		Set Variable	$0.00
	${MedianPrice}=		Set Variable	$0.00

	:FOR	${i}	IN RANGE		0	${pagesamount}
	\		${pageresults}=			Get Matching Xpath Count			//Section[@class="search-results"]/div//a/section/section[2]/span[3]
	\		${found}=				Scan TCG Player Page			${code}			${pageresults}
	\		${MarketPrice}=		Run Keyword If		${found}		Get Text				//ul[@class="price-points__rows"]/li[1]/span[@class="price"]
	\		${MedianPrice}=		Run Keyword If		${found}		Get Text				//ul[@class="price-points__rows"]/li[3]/span[@class="price"]
	\		Exit For Loop If		${found}
	#//Else go to next page
	\		Run Keyword If			${currentPage}!=${pagesamount}		Click Element			//div[@class="search-layout__pagination"]/div/div[2]/div[3]
	\		Run Keyword If			${currentPage}!=${pagesamount}		${currentPage}=			Evaluate		${currentPage} + 1
	\		Run Keyword If			${currentPage}!=${pagesamount}		Sleep					1s
	[Return]				${MarketPrice}			${MedianPrice}
	#####
	
Scan TCG Player Page
	####
	[Arguments]		${code}		${pageresults}
	${pageresults}=			Evaluate		${pageresults} + 1
	
	${found}=				Set Variable	False
	${indexFound}=			Set Variable	-1
	
	:FOR	${i}	IN RANGE		1	${pageresults}
	\		${resultcode}=			Get Text			//Section[@class="search-results"]/div[${i}]//a/section/section[2]/span[3]
	\		${Matched}=				Evaluate			"#${code}"=="${resultcode}"
	\		${found}=			Run Keyword If		${Matched}		Set Variable		${Matched}		ELSE		Set Variable	${found}		
	\		${indexFound}=		Run Keyword If		${Matched}		Set Variable		${i}		ELSE		Set Variable	${indexFound}		
	\		Exit For Loop If		${found}
	
	Run Keyword If		${found}	Click Element							//Section[@class="search-results"]/div[${indexFound}]//a/section/section[2]/span[3]
	Run Keyword If		${found}	Wait Until Element Is Visible			//ul[@class="price-points__rows"]/li[1]/span[@class="price"]
	Run Keyword If		${found}==False		Click Element					//a[@class="marketplace-header__logo is-active"]
	
	[Return]		${found}
	#####
	
ProDeck_Search Card
	####
	[Arguments]		${name}
	Input Text		//input[@id="searchcards"]		${name}
	Wait Until Element Is Visible			//ul[@id="ui-id-1"]/li/div
	#Click Element	//div[.='${name}']
	#Sleep				1s
	${results}=			Get Matching Xpath Count		//ul[@id="ui-id-1"]/li/div
	${loopAmount}=		Evaluate						${results} + 1
	
	${index}=			Set Variable					-1
	:FOR	${i}	IN RANGE		1	${loopAmount}
	\		${resultText}=			Get Text			//ul[@id="ui-id-1"]/li[${i}]/div
	\		${index}=				Run Keyword If		"${resultText}"=="${name}"		Set Variable		${i}		ELSE	Set Variable	${index}
	\		Exit For Loop If		"${index}"!="-1"
	
	#//Click result
	Click Element			//ul[@id="ui-id-1"]/li[${index}]/div
	#####
	
ProDeck_Get All Set Prices
	####
	[Arguments]				${LineItems}		${cardname}
	# ${cardname}=			Set Variable		${LineItems}[1]
	${setsAmounts}=			Set Variable		${LineItems}[8]
	@{SetsPrices}=			Create List
	${SetIterator}=			Set Variable			11
	:FOR	${i}	IN RANGE		0	${setsAmounts}
	\		${setName}=				Set Variable		${LineItems}[${SetIterator}]
	\		${codeIterator}=		Evaluate			${SetIterator} - 1
	\		${code}=				Set Variable		${LineItems}[${codeIterator}]
	\		${isMRL}=				Run Keyword And Return Status		Should Contain		${code}		MRL-
	\		${isANGU}=				Run Keyword And Return Status		Should Contain		${code}		ANGU-EN
	\		${setName}=				Run Keyword If		${isMRL}		Set Variable		Magic Ruler		ELSE		Set Variable		${setName}	
	\		${setName}=				Run Keyword If		${isANGU}		Set Variable		ancient-guardians		ELSE		Set Variable		${setName}	
	\		${nameUpdated}=			Run Keyword And Return Status		Should Contain		${cardname}		(Updated from:
	\		${cardname}=			Run Keyword If		${nameUpdated}	Fetch From Left		${cardname}			${space}(Updated from:		ELSE		Set Variable	${cardname}
	\		${ediiedName}=			Parse Card Name		${cardname}
	\		${ediiedset}=			Parse Set Name		${setName}
	\		${Nameaddendum}=		Get Name Addemdum	${code}
	\		${Skip}=				Is Code In Skip List	${code}
	\		${MarketPrice}			${MedianPrice}=		Run Keyword If		"${Skip}"=="False"		Go To TCG And Get Prices		${ediiedset}		${ediiedName}	${Nameaddendum}		${code}
	\		${MarketPrice}=			Run Keyword If		"${Skip}"=="True"		Set Variable		$0.00		ELSE	Set Variable		${MarketPrice}
	\		${MedianPrice}=			Run Keyword If		"${Skip}"=="True"		Set Variable		$0.00		ELSE	Set Variable		${MedianPrice}
	\		Append To List			${SetsPrices}		${MarketPrice}
	\		Append To List			${SetsPrices}		${MedianPrice}
	\		${SetIterator}			Evaluate			${SetIterator} + 4
	
	#//Create the complete string using all the prices in the list
	${completestring}=				Set Variable				${EMPTY}
	:FOR	${price}	IN			@{SetsPrices}
	\		${completestring}=		Set Variable				${completestring}|${price}
	
	[Return]				${completestring}
	#####
	
Parse Card Name
	####
	[Arguments]				${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Slime Toad (Updated from: Frog the Jam)"		Set Variable	Slime Toad		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Twin Long Rods #2"		Set Variable	Twin Long Rods 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Mystical Sheep #1"		Set Variable	Mystical Sheep 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Mystical Sheep #2"		Set Variable	Mystical Sheep 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Nekogal #1"		Set Variable	Nekogal 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Nekogal #2"		Set Variable	Nekogal 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Crawling Dragon #1"		Set Variable	Crawling Dragon 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Crawling Dragon #2"		Set Variable	Crawling Dragon 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Luster Dragon #2"		Set Variable	Luster Dragon 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Key Mace #2"		Set Variable	Key Mace 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Skull Knight #2"		Set Variable	Skull Knight 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Winged Dragon, Guardian of the Fortress #1"		Set Variable	Winged Dragon, Guardian of the Fortress 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Winged Dragon, Guardian of the Fortress #2"		Set Variable	Winged Dragon, Guardian of the Fortress 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Tyhone #1"		Set Variable	Tyhone 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Tyhone #2"		Set Variable	Tyhone 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Flying Kamakiri #1"		Set Variable	Flying Kamakiri 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Flying Kamakiri #2"		Set Variable	Flying Kamakiri 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Jinzo #7"		Set Variable	Jinzo 7		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Steel Ogre Grotto #1"		Set Variable	Steel Ogre Grotto 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Steel Ogre Grotto #2"		Set Variable	Steel Ogre Grotto 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Darkfire Soldier #1"		Set Variable	Darkfire Soldier 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Darkfire Soldier #2"		Set Variable	Darkfire Soldier 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Morphing Jar #2"		Set Variable	Morphing Jar 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Rock Ogre Grotto #1"		Set Variable	Rock Ogre Grotto 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Rock Ogre Grotto #2"		Set Variable	Rock Ogre Grotto 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Mushroom Man #2"		Set Variable	Mushroom Man 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="M-Warrior #1"		Set Variable	M-Warrior 1		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="M-Warrior #2"		Set Variable	M-Warrior 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Sasuke Samurai #2"		Set Variable	Sasuke Samurai 2		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Sasuke Samurai #3"		Set Variable	Sasuke Samurai 3	ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Sasuke Samurai #4"		Set Variable	Sasuke Samurai 4	ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Fiend Reflection #2"		Set Variable	Fiend Reflection 2	ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Black Skull Dragon"		Set Variable	B. Skull Dragon		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Red-Eyes Black Dragon"		Set Variable	Red Eyes B. Dragon		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Meteor Black Dragon"		Set Variable	Meteor B. Dragon		ELSE		Set Variable		${cardname}
	${cardname}=			Run Keyword if		"${cardname}"=="Malefic Red-Eyes Black Dragon"		Set Variable	Malefic Red-Eyes B. Dragon		ELSE		Set Variable		${cardname}
	# ${cardname}=			Run Keyword if		"${cardname}"=="Sephylon, the Ultimate Timelord"		Set Variable	Sephylon, the Ultimate Time lord		ELSE		Set Variable		${cardname}
	${ediiedName}=			Replace String		${cardname}					${space}-${space}		-
	${ediiedName}=			Replace String		${ediiedName}				&				and
	${ediiedName}=			Replace String		${ediiedName}				${space}		-
	${ediiedName}=			Replace String		${ediiedName}				!				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				/				-
	${ediiedName}=			Replace String		${ediiedName}				%				pct
	${ediiedName}=			Replace String		${ediiedName}				?				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				@				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				:				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				,				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				ü				u
	${ediiedName}=			Replace String		${ediiedName}				Ü				u
	${ediiedName}=			Replace String		${ediiedName}				ú				u
	${ediiedName}=			Replace String		${ediiedName}				.				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				'				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				☆				-
	${ediiedName}=			Replace String		${ediiedName}				・RE				RE
	${ediiedName}=			Replace String		${ediiedName}				・SK				SK
	${ediiedName}=			Replace String		${ediiedName}				・Y				Y
	${ediiedName}=			Replace String		${ediiedName}				é				e
	${ediiedName}=			Replace String		${ediiedName}				ñ				n
	${ediiedName}=			Replace String		${ediiedName}				★				-
	${ediiedName}=			Replace String		${ediiedName}				α				${EMPTY}
	${ediiedName}=			Replace String		${ediiedName}				β				-beta
	${ediiedName}=			Replace String		${ediiedName}				Ω				omega
	${ediiedName}=			Convert To Lower Case							${ediiedName}
	[Return]				${ediiedName}
	#####
	
Parse Set Name
	####
	[Arguments]				${setName}
	${ediiedset}=			Convert To Lower Case							${setName}
	${ediiedset}=			EditedNameList Conversion						${ediiedset}
	${ediiedset}=			Replace String		${ediiedset}		${space}		-
	${ediiedset}=			Replace String		${ediiedset}		:				${EMPTY}
	${ediiedset}=			Replace String		${ediiedset}		'				${EMPTY}
	${ediiedset}=			Replace String		${ediiedset}		!				${EMPTY}
	[Return]				${ediiedset}
	#####
	
Get Name Addemdum
	####
	[Arguments]				${code}
	${Nameaddendum}=		Set Variable		${EMPTY}
	
	${isYGLDA}=			Run Keyword And Return Status		Should Contain		${code}		YGLD-ENA
	${Nameaddendum}=	Run Keyword If		${isYGLDA}		Set Variable		-a	ELSE	Set Variable		${Nameaddendum}
	
	${Nameaddendum}=		Run Keyword If		"${code}"=="YGLD-ENA00"		Set Variable		${EMPTY}	ELSE	Set Variable		${Nameaddendum} 
	
	${isYGLDB}=			Run Keyword And Return Status		Should Contain		${code}		YGLD-ENB
	${Nameaddendum}=	Run Keyword If		${isYGLDB}		Set Variable		-b	ELSE	Set Variable		${Nameaddendum}
	
	${isYGLDC}=			Run Keyword And Return Status		Should Contain		${code}		YGLD-ENC
	${Nameaddendum}=	Run Keyword If		${isYGLDC}		Set Variable		-c	ELSE	Set Variable		${Nameaddendum}
	
	${isNECH-ENS}=		Run Keyword And Return Status		Should Contain		${code}		NECH-ENS
	${Nameaddendum}=	Run Keyword If		${isNECH-ENS}		Set Variable		-se	ELSE	Set Variable		${Nameaddendum}
	
	${isDL09}=		Run Keyword And Return Status		Should Contain		${code}		DL09-EN
	${Nameaddendum}=	Run Keyword If		${isDL09}		Set Variable		-silver			ELSE	Set Variable		${Nameaddendum}
	
	${isDL11}=		Run Keyword And Return Status		Should Contain		${code}		DL11-EN
	${Nameaddendum}=	Run Keyword If		${isDL11}		Set Variable		-red			ELSE	Set Variable		${Nameaddendum}
	
	${isDL12}=		Run Keyword And Return Status		Should Contain		${code}		DL12-EN
	${Nameaddendum}=	Run Keyword If		${isDL12}		Set Variable		-green			ELSE	Set Variable		${Nameaddendum}
	
	${isDL13}=		Run Keyword And Return Status		Should Contain		${code}		DL13-EN
	${Nameaddendum}=	Run Keyword If		${isDL13}		Set Variable		-green			ELSE	Set Variable		${Nameaddendum}
	
	${isDL14}=		Run Keyword And Return Status		Should Contain		${code}		DL14-EN
	${Nameaddendum}=	Run Keyword If		${isDL14}		Set Variable		-blue			ELSE	Set Variable		${Nameaddendum}
	
	${isDL15}=		Run Keyword And Return Status		Should Contain		${code}		DL15-EN
	${Nameaddendum}=	Run Keyword If		${isDL15}		Set Variable		-green			ELSE	Set Variable		${Nameaddendum}
	
	${isDL16}=		Run Keyword And Return Status		Should Contain		${code}		DL16-EN
	${Nameaddendum}=	Run Keyword If		${isDL16}		Set Variable		-green			ELSE	Set Variable		${Nameaddendum}
	
	${isDL17}=		Run Keyword And Return Status		Should Contain		${code}		DL17-EN
	${Nameaddendum}=	Run Keyword If		${isDL17}		Set Variable		-green			ELSE	Set Variable		${Nameaddendum}
	
	${isDL18}=		Run Keyword And Return Status		Should Contain		${code}		DL18-EN
	${Nameaddendum}=	Run Keyword If		${isDL18}		Set Variable		-green			ELSE	Set Variable		${Nameaddendum}
	
	# ${isMVP1-ENSV}=		Run Keyword And Return Status		Should Contain		${code}		MVP1-ENSV
	# ${Nameaddendum}=	Run Keyword If		${isMVP1-ENSV}		Set Variable		-ultra-rare	ELSE	Set Variable		${Nameaddendum}
	
	# ${isMVP1-ENG}=		Run Keyword And Return Status		Should Contain		${code}		MVP1-ENG
	# ${Nameaddendum}=	Run Keyword If		${isMVP1-ENG}		Set Variable		-gold-rare	ELSE	Set Variable		${Nameaddendum}
	
	${Nameaddendum}=		Run Keyword If		"${code}"=="CROS-ENSP1"		Set Variable		-cros-ensp1		ELSE	Set Variable		${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN132"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CMC-EN001"		Set Variable		-capsule-monster-coliseum		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CMC-EN002"		Set Variable		-capsule-monster-coliseum		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CMC-EN003"		Set Variable		-capsule-monster-coliseum		ELSE	Set Variable	${Nameaddendum}
	#${Nameaddendum}=		Run Keyword If		"${code}"=="YGLD-ENA08"		Set Variable		-a		ELSE	Set Variable	${Nameaddendum}
	#${Nameaddendum}=		Run Keyword If		"${code}"=="YGLD-ENB10"		Set Variable		-b		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN127"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN121"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN117"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN122"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN123"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN124"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN125"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN126"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN120"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN118"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN046"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN014"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-EN049"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN083"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN059"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YDT1-EN001"		Set Variable		-5ds-duel-transer		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DB2-EN003"		Set Variable		-db2-en003		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DL18-EN002"		Set Variable		-green-dl18		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YGLD-ENC00"		Set Variable		${EMPTY}		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YGLD-ENB00"		Set Variable		${EMPTY}		ELSE	Set Variable	${Nameaddendum}
	
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN001"		Set Variable		-purple		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN023"		Set Variable		-blue		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN101"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN015"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN088"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN076"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN045"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN112"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN111"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN069"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN068"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN024"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN049"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN077"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDS1-EN117"		Set Variable		-green		ELSE	Set Variable	${Nameaddendum}
	
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN130"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN116"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN129"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN119"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN128"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN055"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN131"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP03-EN203"		Set Variable		-shatterfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP01-EN026"		Set Variable		-starfoil-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN047"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN048"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN044"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN046"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN052"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN042"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN065"		Set Variable		-ultra		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN024"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN048"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SHSP-EN053"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TLM-EN015"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN047"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="STON-EN024"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DUEA-EN051"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-EN051"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SHSP-EN052"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN078"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-EN072"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-EN000"		Set Variable		-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LCJW-EN293"		Set Variable		-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS11"		Set Variable		-se		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS12"		Set Variable		-se		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="EXVC-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TSHD-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ANPR-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PHSW-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LVAL-ENDE1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LVAL-ENDE2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LVAL-ENDE3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SOVR-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DUEA-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DUEA-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DUEA-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DUEA-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CIBR-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="REDU-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="REDU-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="REDU-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="REDU-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="STOR-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="STOR-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="STOR-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="STOR-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SHSP-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SHSP-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SHSP-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SHSP-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="FLOD-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="FLOD-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="FLOD-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="FLOD-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ORCS-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ORCS-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ORCS-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ORCS-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GENF-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MGED-EN141"		Set Variable		-teal-original-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GFTP-EN131"		Set Variable		-ghost-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GFTP-EN132"		Set Variable		-ghost-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GFTP-EN130"		Set Variable		-ghost-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GFTP-EN128"		Set Variable		-ghost-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GFTP-EN129"		Set Variable		-ghost-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SDSB-EN041"		Set Variable		-common		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DB2-EN008"		Set Variable		-db2-en008		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="NECH-ENSP1"		Set Variable		-sneak-peak		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="EXFO-ENSP1"		Set Variable		-sneak-peek		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JOTL-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DREV-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DREV-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DREV-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DREV-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PRIO-ENDE1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PRIO-ENDE2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PRIO-ENDE3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABPF-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABPF-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABPF-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABPF-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="RGBT-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="RGBT-ENSP2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="RGBT-ENSP3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="RGBT-ENSP4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	# ${Nameaddendum}=		Run Keyword If		"${code}"=="NECH-ENS01"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS04"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS01"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS02"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS03"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS05"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS06"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS07"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS08"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS09"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS10"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS11"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS12"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS13"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SECE-ENS14"		Set Variable		-se				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SHVI-ENSP1"		Set Variable		-shvi-ensp1		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DOCS-ENSP1"		Set Variable		-ensp1			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BOSH-ENSP1"		Set Variable		-ensp1			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YCSW-EN007"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YCSW-EN008"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LVAL-EN000"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PTDN-ENSP1"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PTDN-ENSP2"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PTDN-ENSP3"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PTDN-ENSP4"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YCSW-EN009"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LIOV-EN100"		Set Variable		-starlight-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ETCO-EN100"		Set Variable		-starlight-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PHRA-EN100"		Set Variable		-starlight-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BODE-EN100"		Set Variable		-starlight-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ROTD-EN100"		Set Variable		-starlight-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GFTP-EN129"		Set Variable		-ghost-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENSV4"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENSV3"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENSV2"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENSV6"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENSV7"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENSV8"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MOV2-EN001"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ABYR-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MAGO-EN039"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MAGO-EN018"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MAGO-EN041"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MAGO-EN016"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MAGO-EN006"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LCGX-EN002"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LCGX-EN004"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LCGX-EN007"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MAGO-EN004"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YCSW-EN011"		Set Variable		-sr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LCKC-EN001"		Set Variable		-version-4		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENG55"		Set Variable		-gold-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVP1-ENG54"		Set Variable		-gold-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDK2-ENK01"		Set Variable		-version-2		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDK2-ENK01"		Set Variable		-version-2		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JMPS-EN002"		Set Variable		-jmps-en002		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JUMP-EN068"		Set Variable		-jump-en068		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PCK-001"		Set Variable		-power-of-chaos-kaiba-the-revenge		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PCK-002"		Set Variable		-power-of-chaos-kaiba-the-revenge		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PCK-003"		Set Variable		-power-of-chaos-kaiba-the-revenge		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PCK-004"		Set Variable		-power-of-chaos-kaiba-the-revenge		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="FMR-001"		Set Variable		-forbidden-memories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="FMR-002"		Set Variable		-forbidden-memories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="FMR-003"		Set Variable		-forbidden-memories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DUEA-EN004"		Set Variable		-secret-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BODE-EN007"		Set Variable		-secret-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SP17-EN044"		Set Variable		-starfoil		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DL12-EN008"		Set Variable		-purple		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SDCR-EN003"		Set Variable		-black		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DDS-001"		Set Variable		-dark-duel-stories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DDS-002"		Set Variable		-dark-duel-stories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DDS-003"		Set Variable		-dark-duel-stories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DDS-004"		Set Variable		-dark-duel-stories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DDS-005"		Set Variable		-dark-duel-stories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DDS-006"		Set Variable		-dark-duel-stories		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JOTL-EN047"		Set Variable		-secret		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DAMA-EN100"		Set Variable		-starlight-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BLVO-EN100"		Set Variable		-starlight-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TDGS-EN040"		Set Variable		-ghost		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TDGS-EN040"		Set Variable		-utr		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="KICO-EN064"		Set Variable		-secret-pharaohs-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="KICO-EN063"		Set Variable		-secret-pharaohs-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="KICO-EN065"		Set Variable		-secret-pharaohs-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MVPC-EN001"		Set Variable		-blu-ray-dvd-promo		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GBI-002"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PTDN-EN082"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DLG1-EN110"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BODE-EN051"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CBLZ-EN086"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JMP-EN004"		Set Variable		-jmp-en004		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JMPS-EN004"		Set Variable		-jmps-en004		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP02-EN125"		Set Variable		-mosaic-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP02-EN126"		Set Variable		-mosaic-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BP02-EN127"		Set Variable		-mosaic-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="PRIO-EN000"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TDGS-ENSP1"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LODT-ENSP1"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CSOC-ENSP1"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CSOC-ENSP2"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CSOC-ENSP3"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CSOC-ENSP4"		Set Variable		-super-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LC06-EN001"		Set Variable		-lc06-en001		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="WCS-EN502"		Set Variable		-lc06-en001		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="DB2-EN004"		Set Variable		-db2-en004		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GX04-EN001"		Set Variable		-gx-tag-force-2		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GX04-EN002"		Set Variable		-gx-tag-force-2		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GX04-EN003"		Set Variable		-gx-tag-force-2		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GX04-EN004"		Set Variable		-gx-tag-force-2		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TSC-001"		Set Variable		-the-sacred-cards		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TSC-002"		Set Variable		-the-sacred-cards		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TSC-003"		Set Variable		-the-sacred-cards		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TSC-004"		Set Variable		-the-sacred-cards		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TF04-EN001"		Set Variable		-5ds-tag-force-4		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="RYMP-EN059"		Set Variable		-rymp-en059		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LCGX-EN182"		Set Variable		-alternate-art		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YCSW-EN005"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LTGY-ENSP1"		Set Variable		-ultra-rare		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JMPS-EN007"		Set Variable		-jmps-en007		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="JMPS-EN003"		Set Variable		-jmps-en003		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LART-EN019"		Set Variable		-2020		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MFC-105"		Set Variable		-reprint-artwork		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TDIL-ENSP1"		Set Variable		-ensp1		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SS01-ENC01"		Set Variable		-common		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LC06-EN002"		Set Variable		-lc06-en002		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ROD-EN001"		Set Variable		-reshef-of-destruction	ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ROD-EN002"		Set Variable		-reshef-of-destruction	ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="ROD-EN003"		Set Variable		-reshef-of-destruction	ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="GAOV-ENSP1"		Set Variable		-gaov-ensp1				ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BLAR-EN000"		Set Variable		-astral			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="BODE-EN012"		Set Variable		-secret-rare			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MRD-008"		Set Variable		-new-artwork			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CORE-ENSP1"		Set Variable		-core-ensp1			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="COTD-ENSP1"		Set Variable		-cotd-ensp1			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="CYHO-ENSP1"		Set Variable		-sneak-peek			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LEHD-ENC15"		Set Variable		-common			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LOB-052"		Set Variable		-magic			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LC06-EN004"		Set Variable		-lc06-en004			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LEHD-ENA23"		Set Variable		-a			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LEHD-ENB19"		Set Variable		-b			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LEHD-ENC16"		Set Variable		-c			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SS04-ENA18"		Set Variable		-a			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SS04-ENB22"		Set Variable		-b			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LDK2-ENJ26"		Set Variable		-common		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SS03-ENA22"		Set Variable		-a		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SS03-ENB24"		Set Variable		-b		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TF04-EN001"		Set Variable		-5ds-tag-force-4		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TF04-EN002"		Set Variable		-5ds-tag-force-4		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TF04-EN003"		Set Variable		-5ds-tag-force-4		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="TF04-EN004"		Set Variable		-5ds-tag-force-4		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YDT1-EN001"		Set Variable		-5ds-duel-transer		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YDT1-EN002"		Set Variable		-5ds-duel-transer		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YDT1-EN003"		Set Variable		-5ds-duel-transer		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="YDT1-EN004"		Set Variable		-5ds-duel-transer		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="RATE-ENSP1"		Set Variable		-rate-ensp1		ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LCKC-EN046"		Set Variable		-version-2	ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="SDZW-EN032"		Set Variable		-sdzw-en032			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="LC06-EN005"		Set Variable		-lc06-en005			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="MACR-ENSP1"		Set Variable		-macr-ensp1			ELSE	Set Variable	${Nameaddendum}
	${Nameaddendum}=		Run Keyword If		"${code}"=="INOV-ENSP1"		Set Variable		-inov-ensp1			ELSE	Set Variable	${Nameaddendum}
	[Return]				${Nameaddendum}
	#####
	
EditedNameList Conversion
	####
	[Arguments]				${setname}
	${newName}=				Set Variable			None
	${newName}=				Run Keyword If			"${setname}"=="promotional cards 2"							Set Variable		McDonald's Promo Series 2					ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="realm of the sea emperor structure deck"		Set Variable		Structure Deck: Realm of the Sea Emperor	ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="rise of the dragon lords structure deck"		Set Variable		Structure Deck: Rise of the Dragon Lords	ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="emperor of darkness structure deck"			Set Variable		Structure Deck: Emperor of Darkness	ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="the dark emperor structure deck"				Set Variable		Structure Deck: The Dark Emperor	ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="realm of light structure deck"				Set Variable		Structure Deck: Realm of Light				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="rise of the true dragons structure deck"				Set Variable		Structure Deck: rise of the true dragons				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="structure deck : yugi muto"					Set Variable		Structure Deck: Yugi Muto				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="structure deck : seto kaiba"					Set Variable		Structure Deck: seto kaiba				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="structure deck - marik -"					Set Variable		Structure Deck marik				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="master of pendulum structure deck"			Set Variable		Structure Deck: Master of Pendulum				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="onslaught of the fire kings structure deck"	Set Variable		Structure Deck: Onslaught of the Fire Kings				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="geargia rampage structure deck"				Set Variable		Structure Deck: Geargia Rampage				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="structure deck : cyberse link"				Set Variable		structure deck: cyberse link				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="machine reactor structure deck"				Set Variable		structure deck: machine reactor				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="dinosmasher’s fury structure deck"			Set Variable		Structure Deck: Dinosmasher's Fury				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="cyber dragon revolution structure deck"			Set Variable		Structure Deck: cyber dragon revolution				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="dragunity legion structure deck"			Set Variable		Structure Deck: dragunity legion				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="synchron extreme structure deck"			Set Variable		Structure Deck: synchron extreme				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="pendulum domination structure deck"			Set Variable		Structure Deck: pendulum domination				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="structure deck : lair of darkness"			Set Variable		structure deck lair of darkness				ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="lost sanctuary structure deck"			Set Variable		structure deck lost sanctuary			ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="warriors' strike structure deck"			Set Variable		structure deck warriors strike			ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="hero strike structure deck"			Set Variable		structure deck hero strike		ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="gates of the underworld structure deck"			Set Variable		structure deck gates of the underworld		ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="zombie world structure deck"			Set Variable		structure deck zombie world		ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="machina mayhem structure deck"			Set Variable		structure deck machina mayhem		ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="cyber dragon revolution structure deck"			Set Variable		structure deck cyber dragon revolution		ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="spellcaster's command structure deck"			Set Variable		structure deck spellcasters command		ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="samurai warlords structure deck"			Set Variable		structure deck samurai warlords		ELSE	Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="structure deck dinosaur's rage special edition"			Set Variable		structure deck dinosaur's rage			ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack one"								Set Variable		Astral Pack 1								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack two"								Set Variable		Astral Pack 2								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack three"							Set Variable		Astral Pack 3								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack four"							Set Variable		Astral Pack 4								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack five"							Set Variable		Astral Pack 5								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack six"								Set Variable		Astral Pack 6								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack seven"							Set Variable		Astral Pack 7								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="astral pack eight"							Set Variable		Astral Pack 8								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="5d's starter deck 2008"						Set Variable		5D's 2008 Starter Deck								ELSE	Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="2020 tin of lost memories mega pack"			Set Variable		2020 Tin of Lost Memories			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal 5a"							Set Variable		Duel Terminal 5			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal 5b"							Set Variable		Duel Terminal 5			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal 6a"							Set Variable		Duel Terminal 6			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal 6b"							Set Variable		Duel Terminal 6			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal 7a"							Set Variable		Duel Terminal 7			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal 7b"							Set Variable		Duel Terminal 7			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal - preview wave 2"					Set Variable		Duel Terminal-Preview			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 3 yugi's world mega pack"	Set Variable		Legendary Collection 3: Yugi's World			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection kaiba mega pack"			Set Variable		Legendary Collection Kaiba			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 4 joey's world mega pack"	Set Variable		Legendary Collection 4: Joey's World			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="saga of blue-eyes white dragon structure deck"	Set Variable		Structure Deck: Saga of Blue-Eyes White Dragon			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="dark crisis (reprint)"						Set Variable		dark crisis			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - jaden yuki -"						Set Variable		duelist-pack-1-jaden-yuki	ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - jaden yuki 2 -"						Set Variable		duelist-pack-5-jaden-yuki-2	ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - jaden yuki 3 -"						Set Variable		duelist-pack-6-jaden-yuki-3	ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - kaiba -"						Set Variable		Duelist Pack: Kaiba		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - yugi -"						Set Variable		Duelist Pack: Yugi		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - jesse anderson -"				Set Variable		Duelist Pack 7: Jesse Anderson		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - jaden yuki 1 -"				Set Variable		Duelist Pack 3: Jaden Yuki 1		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - jaden yuki 2 -"				Set Variable		Duelist Pack 3: Jaden Yuki 2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - yusei -"				Set Variable		Duelist Pack 8: Yusei fudo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - yusei 2 -"				Set Variable		Duelist Pack 9: Yusei 2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - yusei 3 -"				Set Variable		Duelist Pack 10: Yusei 3		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - zane truesdale -"				Set Variable		duelist-pack-4-zane-truesdale		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - chazz princeton -"				Set Variable		Duelist Pack 2: Chazz Princeton		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - crow -"				Set Variable		Duelist Pack 11: Crow		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - dimensional guardians -"				Set Variable		duelist-pack-dimensional-guardians		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="flames of destruction special edition"		Set Variable		flames of destruction		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="crossed souls sneak peek"					Set Variable		Crossed Souls		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="battle pack 2 war of the giants round 2"		Set Variable		battle-pack-2-war-of-the-giants-–-round-2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="battle pack 3 monster league"				Set Variable		Battle Pack 3: Monster League		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legend of blue eyes white dragon"			Set Variable		the legend of blue eyes white dragon		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="promotional cards 1"							Set Variable		McDonald's Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game one"				Set Variable		Champion Pack 1		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game two"				Set Variable		Champion Pack 2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game three"			Set Variable		Champion Pack 3		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game four"				Set Variable		Champion Pack 4		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game five"				Set Variable		Champion Pack 5		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game six"				Set Variable		Champion Pack 6		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game seven"			Set Variable		Champion Pack 7		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game eight"			Set Variable		Champion Pack 8		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="champion pack game nine"				Set Variable		Champion Pack 9		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! capsule monster coliseum bundles"			Set Variable		Yu-Gi-Oh! Video Game Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="gold series: maximum gold"			Set Variable		maximum gold		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="gold series"							Set Variable		Gold Series 2008		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="tournament pack 1st season"			Set Variable		Tournament Pack 1		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="tournament pack 2nd season"			Set Variable		Tournament Pack 2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="tournament pack 3rd season"			Set Variable		Tournament Pack 3		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="tournament pack 4th season"			Set Variable		Tournament Pack 4		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="tournament pack 5th season"			Set Variable		Tournament Pack 5		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yugi's legendary decks"				Set Variable		King of Games: Yugi's Legendary Decks		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 18"					Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="retro pack"							Set Variable		Retro Pack 1		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 2 the duel academy years mega pack"			Set Variable		Legendary Collection 2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 5d's mega pack"			Set Variable		Legendary Collection 5D's		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! world championship 2008 bundles"	Set Variable		World Championship 2008		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="hidden arsenal 4 trishula's triumph"			Set Variable		Hidden Arsenal 4		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2012 wave 2"					Set Variable		2012 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="battles of legend -light's revenge-"			Set Variable		Battles of Legend: Light's Revenge		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="code of the duelist special edition"			Set Variable		Code of the Duelist		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 1"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 2"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 3"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 4"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 5"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 6"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 7"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 8"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 9"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 10"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 11"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 12"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 13"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 14"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 15"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 16"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 17"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 18"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league 2010"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 1"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 2"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 3"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 4"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 5"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 6"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 7"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 8"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 9"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 10"							Set Variable		Duelist League Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="speed duel starter decks: match of the millennium"	Set Variable		Speed Duel Decks: Match of the Millennium		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="speed duel starter deck: destiny masters"			Set Variable		Speed Duel Decks: Destiny Masters		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="speed duel starter deck: duelists of tomorrow"		Set Variable		Speed Duel Decks: Duelists of Tomorrow		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="speed duel starter decks: twisted nightmares"		Set Variable		Speed Duel Decks: twisted nightmares		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="speed duel tournament pack 02"			Set Variable		Speed Duel Tournament Pack 2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! advent calendar 2018"			Set Variable		Advent Calendar 2018		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="dragons of legend -unleashed-"			Set Variable		Dragons of Legend: Unleashed		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="gladiator's assault special edition"		Set Variable		gladiator's assault se		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="exclusive pack"							Set Variable		Yu-Gi-Oh! Movie Exclusive Pack		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's dueltranser bundles"		Set Variable		Yu-Gi-Oh! Video Game Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the dark side of dimensions movie pack"						Set Variable		The Dark Side of Dimensions Movie Pack		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the dark side of dimensions movie pack secret edition"			Set Variable		The Dark Side of Dimensions Movie Pack Secret Edition		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the dark side of dimensions movie pack gold edition"			Set Variable		The Dark Side of Dimensions Movie Pack Gold Edition		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! world championship 2008"		Set Variable		yugioh-world-championship-series		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yugi's legendary decks"					Set Variable		King of Games: Yugi's Legendary Decks		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 5 wave 1"			Set Variable		2008 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 5 wave 2"			Set Variable		2008 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2009 wave 1"				Set Variable		2009 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2009 wave 2"				Set Variable		2009 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2010 wave 1"				Set Variable		2010 Collectors Tins		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2010 wave 2"				Set Variable		2010 Collectors Tins		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2011 wave 1"				Set Variable		2011 Collectors Tins		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2011 wave 2"				Set Variable		2011 Collectors Tins		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2013 wave 1"				Set Variable		2013 Collectors Tins		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2013 wave 2"				Set Variable		2013 Collectors Tins		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 2"				Set Variable		2005 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 4 wave 1"			Set Variable		2007 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 4 wave 2"			Set Variable		2007 Collectors Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="demo deck"								Set Variable		Demo Pack		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="premium collection tin"					Set Variable		2012 Premium Collection Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx tag force 3 bundles"		Set Variable		Yu-Gi-Oh! GX Tag Force Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="turbo pack booster one"		Set Variable		Turbo Pack Booster One Pack		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="zexal collection tin"		Set Variable		2013 Zexal Collection Tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel terminal - preview wave 1"		Set Variable		Duel Terminal-Preview		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="the new challengers super edition"		Set Variable		The New Challengers		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="the new challengers sneak peek"		Set Variable		The New Challengers		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="starter deck - link strike -"		Set Variable		Starter Deck: Link Strike		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="dragons collide structure deck"		Set Variable		Structure Deck: Dragons Collide		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="battle pack tournament"		Set Variable		Battle Pack Tournament Prize Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! destiny board traveler bundles"		Set Variable		Destiny Board Traveler Promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="ancient　guardians"		Set Variable		ancient-guardians		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack collection tin 2011"		Set Variable		2011 Duelist Pack Tin		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="secrets of eternity sneak peek"		Set Variable		Secrets of Eternity		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="the duelist genesis special edition"		Set Variable		the duelist genesis		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx tag force bundles"		Set Variable		GX Tag Force Promo		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="force of the breaker special edition"		Set Variable		Force of the Breaker		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="extreme victory sneak peek"		Set Variable		Extreme Victory		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! world championship 2004"		Set Variable		World Championship Series		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2012 wave 1"		Set Variable		2012 Collectors Tin		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="the shining darkness sneak peek"		Set Variable		the shining darkness		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="ancient prophecy sneak peek"		Set Variable		ancient prophecy		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="exclusive tins 2009"		Set Variable		Exclusive Tin 2009		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="exclusive tins 2008"		Set Variable		2008 collectors tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="exclusive tins 2007"		Set Variable		Exclusive Tin 2007		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="exclusive tins 2006"		Set Variable		Exclusive Tin 2006		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="ignition assault special edition"		Set Variable		ignition assault		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duel power variant cards"		Set Variable		duel power		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="savage strike special edition"		Set Variable		savage strike		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="circuit break special edition"		Set Variable		circuit break		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="rising rampage special edition"		Set Variable		rising rampage		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="speed duel starter decks: ultimate predators"		Set Variable		Speed Duel Decks: Ultimate Predators		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="dark neostorm special edition"		Set Variable		dark neostorm		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="super starter power-up pack"		Set Variable		super Starter: V for Victory Power-Up Pack		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="shining victories sneak peek"		Set Variable		shining victories		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="dimension of chaos sneak peek"		Set Variable		dimension of chaos		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="breakers of shadow sneak peek"		Set Variable		breakers of shadow		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="speed duel: trials of the kingdom sneak peak"		Set Variable		speed duel: trials of the kingdom		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="starter deck saber force (us)"		Set Variable		Starter Deck: Saber Force		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="2-player starter deck yuya & declan (uk)"		Set Variable		Starter Deck: Saber Force		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="photon shockwave sneak peek"		Set Variable		photon shockwave		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2018"		Set Variable		World Championship Series		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2019"		Set Variable		World Championship Series		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2014"		Set Variable		Yu-Gi-Oh! Championship Series Prize Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 1 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 2 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 3 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 4 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 5 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 6 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 7 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 8 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 9 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="legendary collection 4 joey's world mega pack"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 7 promotional card"		Set Variable		Yu-Gi-Oh! 5D's Manga Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - rivals of the pharaoh -"		Set Variable		Duelist Pack: Rivals of the Pharaoh		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="movie pack"		Set Variable		Yu-Gi-Oh! The Movie Promo Set		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - battle city -"		Set Variable		Duelist Pack: Battle City		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the dark side of dimensions movie pack secret edition variant cards"		Set Variable		the dark side of dimensions movie pack secret edition		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="the lost millennium special edition"		Set Variable		the lost millennium		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2018"		Set Variable		Yu-Gi-Oh! Championship Series Prize Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v volume 2 promotional card"		Set Variable		Yu-Gi-Oh! ARC-V Promo Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="kaiba's collector box"		Set Variable		Collector's Boxes		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary decks ii"		Set Variable		Legendary Decks II		ELSE		Set Variable	${newName}
	${isjump}=				Run Keyword And Return Status		Should Contain		${setname}		shonen
	${newName}=				Run Keyword If			${isjump}		Set Variable		Shonen Jump Magazine Promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shonen jump membership promotional card - march 2014 -"		Set Variable		shonen-jump-magazine-promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! power of chaos kaiba the revenge bundles"		Set Variable		Yu-Gi-Oh! Video Game Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! dark duel stories bundles"		Set Variable		Yu-Gi-Oh! Video Game Promotional Cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="pharaoh tour 2005"		Set Variable		pharaoh tour promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="starter deck dark legion (us)"		Set Variable		starter deck dark legion		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! world championship 2007 bundles"		Set Variable		world championship 2007		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="judgment of the light sneak peek"		Set Variable		judgment of the light		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="extreme force special edition"		Set Variable		extreme force		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! forbidden memories bundles"		Set Variable		Yu-Gi-Oh! Video Game Promotional Cards		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="elemental energy special edition"		Set Variable		elemental energy		ELSE		Set Variable	${newName}
	# ${newName}=				Run Keyword If			"${setname}"=="maximum crisis special edition"		Set Variable		maximum crisis		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's volume 6 promotional card"		Set Variable		yu-gi-oh-5ds-manga-promotional-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! world championship 2013"		Set Variable		yu-gi-oh-zexal-manga-promotional-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.1"		Set Variable		yu-gi-oh-arc-v-promo-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.2"		Set Variable		yu-gi-oh-arc-v-promo-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.3"		Set Variable		yu-gi-oh-arc-v-promo-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.4"		Set Variable		yu-gi-oh-arc-v-promo-cards		ELSE		Set Variable	${newName}
	
	${isSE}=				Run Keyword And Return Status		Should Contain		${setname}		special edition
	${newName}=				Run Keyword If			${isSE}		Fetch From Left		${setname}	${space}special edition			ELSE	Set Variable		${newName}
	
	${isSP}=				Run Keyword And Return Status		Should Contain		${setname}		sneak peek
	${newName}=				Run Keyword If			${isSP}		Fetch From Left		${setname}	${space}sneak peek			ELSE	Set Variable		${newName}
	
	${isSuperE}=				Run Keyword And Return Status		Should Contain		${setname}		super edition
	${newName}=				Run Keyword If			${isSuperE}	Fetch From Left		${setname}	${space}super edition		ELSE	Set Variable		${newName}
	
	${newName}=				Run Keyword If			"${setname}"=="storm of ragnarok special edition"		Set Variable		storm of ragnarok se		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="ancient prophecy special edition"		Set Variable		ancient prophecy se		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="breakers of shadow special edition"		Set Variable		breakers of shadow special edition		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="absolute powerforce special edition"		Set Variable		absolute powerforce special edition		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="light & darkness - power pack -"		Set Variable		dark-revelation-volume-4		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 1 promotional card"		Set Variable		yu-gi-oh-gx-manga-promotional-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 3d bonds beyond time movie pack"		Set Variable		bonds-beyond-time-movie-pack		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 3d bonds beyond time promotional card"		Set Variable		bonds-beyond-time-movie-pack		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.5"		Set Variable		yu-gi-oh-arc-v-promo-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v volume 1 promotional card"		Set Variable		yu-gi-oh-arc-v-promo-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! power of chaos joey the passion bundles"		Set Variable		power-of-chaos-joey-the-passion		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack collection tin 2009"		Set Variable		duelist-pack-collection-tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="clash of rebellions special edition (us)"		Set Variable		clash of rebellions special edition		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="gx next generation"		Set Variable		gx-next-generation-blister-pack-promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="crossed souls advance edition"		Set Variable		crossed souls advanced edition		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="hidden arsenal 5 steelswarm invasion special edition"			Set Variable		Hidden Arsenal 5: Steelswarm Invasion SE			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the dark side of dimensions theatrical distribution"			Set Variable		the-dark-side-of-dimensions-movie-pack-gold-edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="order of chaos special edition"			Set Variable		order of chaos se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! r volume 4 promotional card"			Set Variable		yu-gi-oh-r-manga-promo			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 2 the duel academy years"			Set Variable		legendary-collection-2			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2010"			Set Variable		yu-gi-oh-championship-series-prize-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2015"			Set Variable		yu-gi-oh-championship-series-prize-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 5 promotional card"			Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 4 promotional card"			Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 3 promotional card"			Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 2 promotional card"			Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 1 promotional card"			Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="twilight edition - light & dark -"			Set Variable		twilight edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="premium pack"			Set Variable		premium-pack-1			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shonen jump subscription promotional card - december 2011 -"			Set Variable		yugioh-shonen-jump-magazine-promos			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="primal origin deluxe edition"			Set Variable		primal origin			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.6"			Set Variable		yu-gi-oh-arc-v-promo-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.7"			Set Variable		yu-gi-oh-arc-v-promo-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.8"			Set Variable		yu-gi-oh-arc-v-promo-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! arc-v manga vol.9"			Set Variable		yu-gi-oh-arc-v-promo-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="starstrike blast sneak peek"			Set Variable		sneak-preview-series-5			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2011"			Set Variable		yu-gi-oh-championship-series-prize-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="5d's starter deck duelist toolbox"			Set Variable		starter-deck-duelist-toolbox			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shonen jump championship 2008"			Set Variable		shonen-jump-championship-series-prize-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="invasion: vengeance special edition"			Set Variable		invasion: vengeance special edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legacy of the valiant deluxe edition"			Set Variable		legacy of the valiant			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's stardust accelerator － world championship 2009 － bundles"			Set Variable		stardust-accelerator-promos			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="ots tournament pack 2"			Set Variable		ots-tournament-pack-2			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! power of chaos yugi the destiny bundles"			Set Variable		power-of-chaos-yugi-the-destiny			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx tag force 2 bundles"			Set Variable		yu-gi-oh-video-game-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="tactical evolution special edition"			Set Variable		tactical evolution special edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx spirit caller bundles"			Set Variable		gx-spirit-caller-promo			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="gladiator's assault special edition"			Set Variable		gladiators assault se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 3 wave 1"			Set Variable		2006-collectors-tin			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="speed duel: trials of the kingdom sneak peak"			Set Variable		speed-duel-trials-of-the-kingdom			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin 2012 wave 2.5"			Set Variable		2012-collectors-tin			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's world championship 2010 － reverse of arcadia － bundles"			Set Variable		yu-gi-oh-5ds-reverse-of-arcadia-promo			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! ultimate masters - world championship tournament 2006 - bundles"			Set Variable		world-championship-2006-ultimate-masters			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the falsebound kingdom bundles"			Set Variable		the-falsebound-kingdom			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 1"			Set Variable		2004-collectors-tin			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the sacred cards bundles"			Set Variable		yu-gi-oh-video-game-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's tag force 4 bundles"			Set Variable		yu-gi-oh-video-game-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="hidden arsenal special edition"			Set Variable		hidden arsenal special edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 8 promotional card"			Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shonen jump championship 2004"			Set Variable		shonen-jump-championship-series-promos			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2012"			Set Variable		yu-gi-oh-championship-series-prize-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's tag force 5 bundles"			Set Variable		yu-gi-oh-5ds-tag-force-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="raging tempest special edition"			Set Variable		raging-tempest-special-edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's world championship 2011 － over the nexus － bundles"			Set Variable		yu-gi-oh-5ds-over-the-nexus-promo-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2013"			Set Variable		yu-gi-oh-championship-series-prize-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="generation force special edition"			Set Variable		generation force se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2017"		Set Variable		-yu-gi-oh-championship-series-prize-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="collectible tin series 3 wave 2"		Set Variable		2006-collectors-tin			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="force of the breaker sneak peek"		Set Variable		sneak-preview-series-3			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! worldwide edition - stairway to the destined duel - bundles"		Set Variable		stairway-to-the-destined-duel			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the duelists of the roses bundles"		Set Variable		duelist-of-the-roses			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack special edition"		Set Variable		duelist-pack-special-edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="return of the duelist special edition"		Set Variable		return-of-the-duelist-se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shadow of infinity sneak peek"		Set Variable		sneak-preview-series-2			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 7 promotional card"		Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack collection tin 2008"		Set Variable		duelist-pack-collection-tin			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yugi's collector box"		Set Variable		collectors-boxes			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the dawn of destiny bundles"		Set Variable		dawn-of-destiny-xbox			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shining victories special edition"		Set Variable		shining-victories-special-edition			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="noble knights of the round table box set power-up pack"		Set Variable		noble-knights-of-the-round-table-box-set			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="abyss rising special edition"		Set Variable		abyss-rising-se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="tactical evolution sneak peek"		Set Variable		sneak-preview-series-3			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! nightmare troubadour bundles"		Set Variable		nintendo-ds-nightmare-of-troubadour			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="pharaoh tour 2006"		Set Variable		pharaoh-tour-promos			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 6"		Set Variable		duelist-league-promo			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! world championship 2007"		Set Variable		world-championship-series			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="raging battle special edition"		Set Variable		raging-battle-se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="the lost millennium sneak peek"		Set Variable		sneak-preview-series-1			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="cosmo blazer special edition"		Set Variable		cosmo-blazer-se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="crimson crisis sneak peek"		Set Variable		sneak-preview-series-4			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! reshef of destruction bundles"		Set Variable		yu-gi-oh-video-game-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack - aster phoenix -"		Set Variable		duelist-pack-5-aster-phoenix			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist revolution special edition"		Set Variable		duelist-revolution-se			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 2 the duel academy years mega pack"		Set Variable		legendary-collection-2			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 9 promotional card"		Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx duel academy bundles"		Set Variable		gx-duel-academy-gba-promo			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack collection - jaden yuki - tin"		Set Variable		duelist-pack-collection-tin			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="pharaoh tour 2007"		Set Variable		pharaoh-tour-promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx volume 6 promotional card"		Set Variable		yu-gi-oh-gx-manga-promotional-cards			ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="strike of neos sneak peek"		Set Variable		sneak-preview-series-3		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="ra yellow mega pack special edition"		Set Variable		ra-yellow-mega-pack-se		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! championship series 2016"		Set Variable		yu-gi-oh-championship-series-prize-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="the dark illusion special edition (uk)"		Set Variable		the-dark-illusion-special-edition-eu		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 7 trials to glory - world championship tournament 2005 - bundles"		Set Variable		world-championship-2005-7-trials-to-glory		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 7"		Set Variable		duelist-league-promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! r volume 5 promotional card"		Set Variable		yu-gi-oh-r-manga-promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="exclusive tins 2009"		Set Variable		2009-collectors-tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="flaming eternity sneak peek"		Set Variable		sneak-preview-series-1		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="gladiator's assault sneak peek"		Set Variable		sneak-preview-series-3		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! 5d's wheelie breakers bundles"		Set Variable		yu-gi-oh-5ds-wheelie-breakers-promotional-cards		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 8"		Set Variable		duelist-league-promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 9"		Set Variable		duelist-league-promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist league series 10"		Set Variable		duelist-league-promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="cybernetic horizon sneak peak"		Set Variable		cybernetic-horizon		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 5d's mega pack"		Set Variable		legendary-collection-5ds		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! the eternal duelist soul bundles"		Set Variable		eternal-duelist-soul		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shonen jump championship 2007 a"		Set Variable		shonen-jump-championship-series-promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shonen jump championship 2007 b"		Set Variable		shonen-jump-championship-series-promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! duelist volume 16 promotional card"		Set Variable		shonen-jump-magazine-promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="fire fists special edition"		Set Variable		fire-fists-special-editon		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="shonen jump championship 2006"		Set Variable		shonen-jump-championship-series-promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! gx the beginning of destiny bundles"		Set Variable		yu-gi-oh-gx-tag-force-evolution-promo		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="elemental energy sneak peek"		Set Variable		sneak-preview-series-1		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="power of the duelist sneak peek"		Set Variable		sneak-preview-series-2		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="yu-gi-oh! millennium world volume 4 promotional card"		Set Variable		shonen-jump-magazine-promos		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="duelist pack collection tin 2010"		Set Variable		2010-duelist-pack-collection-tin		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="cybernetic revolution sneak peek"		Set Variable		sneak-preview-series-1		ELSE		Set Variable	${newName}
	${newName}=				Run Keyword If			"${setname}"=="legendary collection 5d's mega pack"		Set Variable		legendary-collection-5ds		ELSE		Set Variable	${newName}
	
	${isZexalPromo}=				Run Keyword And Return Status		Should Contain		${setname}		yu-gi-oh! zexal volume
	${newName}=				Run Keyword If			${isZexalPromo}		Set Variable		yu-gi-oh-zexal-manga-promotional-cards			ELSE	Set Variable		${newName}
	
	${retunvalue}=			Run Keyword If			"${newName}"=="None"		Set Variable		${setname}		ELSE	Set Variable		${newName}
	[Return]				${retunvalue}
	#####

Go To TCG And Get Prices
	####
	[Arguments]			${ediiedset}		${ediiedName}	${Nameaddendum}		${code}
	${ediiedName}=		Run Keyword If		"${code}"=="MGED-EN003"		Set Variable		Red-Eyes Black Dragon		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="SBCB-EN167"		Set Variable		Red-Eyes Black Dragon		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="MAGO-EN003"		Set Variable		Red-Eyes Black Dragon		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="LDS1-EN001"		Set Variable		Red-Eyes Black Dragon		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DUSA-EN099"		Set Variable		Armityle the Chaos Phantom		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="ANPR-EN091"		Set Variable		Armityle the Chaos Phantom		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="LCGX-EN211"		Set Variable		Armityle the Chaos Phantom		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="LON-046"		Set Variable		Marie the Fallen One		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DB1-EN241"		Set Variable		Marie the Fallen One		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DR3-EN090"		Set Variable		Big Core		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="RDS-EN030"		Set Variable		Big Core		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="TU06-EN013"		Set Variable		Kinetic Soldier		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="SDMM-EN010"		Set Variable		Kinetic Soldier		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="WC4-002"		Set Variable		Kinetic Soldier		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="CP04-EN010"		Set Variable		Kinetic Soldier		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="LOD-060"		Set Variable		Gradius's Option		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DR1-EN069"		Set Variable		Vampire Orchis		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="MFC-014"		Set Variable		Vampire Orchis		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="TP8-EN020"		Set Variable		Necrolancer the Timelord		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DB2-EN085"		Set Variable		Oscillo Hero 2		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="TP1-016"		Set Variable		Oscillo Hero 2		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DB1-EN233"		Set Variable		Amazon Archer		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="LON-032"		Set Variable		Amazon Archer		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DR1-EN133"		Set Variable		Cliff the Trap Remover		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="MFC-078"		Set Variable		Cliff the Trap Remover		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="PSV-049"		Set Variable		Harpies Brother		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DB1-EN094"		Set Variable		Harpies Brother		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="SDJ-011"		Set Variable		Harpies Brother		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="PSV-090"		Set Variable		Red-Moon Baby		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DB1-EN200"		Set Variable		Red-Moon Baby		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="TSHD-EN060"		Set Variable		Forbidden Graveyard		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DR1-EN148"		Set Variable		Pigeonholing Books of Spell		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="MFC-093"		Set Variable		Pigeonholing Books of Spell		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="STBL-EN080"		Set Variable		Dark Trap Hole		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DR1-EN154"		Set Variable		Hidden Book of Spell		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="MFC-099"		Set Variable		Hidden Book of Spell		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="JMP-EN008"		Set Variable		Judgment of the Pharaoh		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="DR3-EN057"		Set Variable		Null and Void		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="SOD-EN057"		Set Variable		Null and Void		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="CRMS-EN077"		Set Variable		Metaphysical Regeneration		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="LC5D-EN159"		Set Variable		Roar of the Earthbound		ELSE		Set Variable		${ediiedName}
	${ediiedName}=		Run Keyword If		"${code}"=="SOVR-EN071"		Set Variable		Roar of the Earthbound		ELSE		Set Variable		${ediiedName}
	
	
	${ediiedset}=		Run Keyword If		"${code}"=="ROD-EN003"		Set Variable		reshef-of-destruction		ELSE		Set Variable		${ediiedset}
	${ediiedset}=		Run Keyword If		"${code}"=="OPTP-EN003"		Set Variable		miscellaneous-promotional-cards		ELSE		Set Variable		${ediiedset}
	${ediiedset}=		Run Keyword If		"${code}"=="WC08-EN003"		Set Variable		world-championship-2008-ds-game		ELSE		Set Variable		${ediiedset}
	${URL}=			Set Variable			https://store.tcgplayer.com/yugioh/${ediiedset}/${ediiedName}${Nameaddendum}?partner=YGOPRODeck&utm_campaign=affiliate&utm_medium=card-database-set-prices&utm_source=YGOPRODeck
	Go To					${URL}
	Sleep					1s
	${isSDCR}=			Run Keyword And Return Status		Should Contain		${code}		SDCR-EN
	${code}=				Run Keyword If		${isSDCR}==False	Replace String		${code}				DCR-EN				DCR-		ELSE		Set Variable		${code}
	${code}=				Replace String		${code}				IOC-EN				IOC-
	${code}=				Replace String		${code}				LOD-086				LOD-EN086
	${code}=				Replace String		${code}				CP08-EN001			CP08-EN001${space}
	${code}=				Replace String		${code}				CP05-EN001			CP05-EN001${space}
	${code}=				Replace String		${code}				CP05-EN002			CP05-EN002${space}${space}
	${code}=				Replace String		${code}				CP05-EN003			CP05-EN003${space}${space}
	${code}=				Replace String		${code}				CP05-EN006			CP05-EN006${space}${space}
	${code}=				Replace String		${code}				CP06-EN001			CP06-EN001${space}
	${code}=				Replace String		${code}				CP06-EN002			CP06-EN002${space}${space}
	${code}=				Replace String		${code}				CP06-EN003			CP06-EN003${space}${space}
	${code}=				Replace String		${code}				CP07-EN001			CP07-EN001${space}
	${code}=				Replace String		${code}				CP07-EN002			CP07-EN002${space}${space}
	${code}=				Replace String		${code}				CP07-EN003			CP07-EN003${space}${space}
	${code}=				Replace String		${code}				CP08-EN002			CP08-EN002${space}${space}
	${code}=				Replace String		${code}				CP08-EN003			CP08-EN003${space}${space}
	${code}=				Replace String		${code}				JMP-EN007			JUMP-EN007
	${code}=				Replace String		${code}				JMP-EN006			JUMP-EN006
	${code}=				Replace String		${code}				JMP-EN008			JUMP-EN008
	${isReadMoreVisible}=	Run Keyword And Return Status			Page Should Contain Element					//div[@class="pd-description__toggle masked"]
	Run Keyword If			${isReadMoreVisible}	Click Element									//div[@class="pd-description__toggle masked"]
	Wait Until Element Is Visible					//span[.='${code}']
	${MarketPrice}=			Get Text				//ul[@class="price-points__rows"]/li[1]/span[@class="price"]
	${MedianPrice}=			Get Text				//ul[@class="price-points__rows"]/li[3]/span[@class="price"]
	[Return]				${MarketPrice}			${MedianPrice}
	#####
	
Is Code In Skip List
	####
	[Arguments]			${code}
	${bool}=			Set Variable		False
	${isWGRT}=			Run Keyword And Return Status		Should Contain		${code}		WGRT-EN
	${bool}=			Run Keyword If		${isWGRT}					Set Variable		True	ELSE	Set Variable		${bool}
	${isDEM3}=			Run Keyword And Return Status		Should Contain		${code}		DEM3-EN
	${bool}=			Run Keyword If		${isDEM3}					Set Variable		True	ELSE	Set Variable		${bool}
	${isYS15NY}=		Run Keyword And Return Status		Should Contain		${code}		YS15-EN
	${bool}=			Run Keyword If		${isYS15NY}					Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="MP1-004"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-EN701"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2015-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-AE803"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-AE503"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-AE403"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-AE903"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-AE603"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2014-AE003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2013-AE003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2012-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="BP03-EN094"		Set Variable		True	ELSE	Set Variable		${bool}	
	# ${bool}=			Run Keyword If		"${code}"=="BP02-EN084"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="ORCS-EN093"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-EN402"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-EN801"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2018-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2017-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2016-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2010-AE003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2014-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	# ${bool}=			Run Keyword If		"${code}"=="YS15-ENL06"		Set Variable		True	ELSE	Set Variable		${bool}	
	# ${bool}=			Run Keyword If		"${code}"=="YS15-END10"		Set Variable		True	ELSE	Set Variable		${bool}	
	# ${bool}=			Run Keyword If		"${code}"=="YS15-END05"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LCJW-EN038"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SDDC-EN007"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DR3-EN017"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SD1-EN007"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SOD-EN017"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LC05-EN004"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LDS1-EN012"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LDS1-EN006"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LDS1-EN013"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LCJW-EN054"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2020-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	# ${bool}=			Run Keyword If		"${code}"=="YS15-END09"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DDS-001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DDS-002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DDS-003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DDS-004"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DDS-005"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SJCS-EN007"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SJC-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LC06-EN003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DOCS-ENSE1"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-002"		Set Variable		True	ELSE	Set Variable		${bool}	
	# ${bool}=			Run Keyword If		"${code}"=="KICO-EN063"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="JMPS-EN005"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LED7-EN000"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="GX05-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="JUMP-EN054"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2011-AE003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2010-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="GX1-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-EN901"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DOCS-ENSE2"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SECE-ENS03"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-EN501"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WC4-003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WC4-002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LCGX-EN176"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2013-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2017-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SECE-ENS06"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-EN802"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WQ11-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WQ11-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WQ11-EN003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="ZDC1-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="ZDC1-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="ZDC1-EN003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="ZDC1-EN004"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WC4-001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2011-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2016-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SP2-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2019-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2021-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DOCS-ENSE3"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2013-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2015-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2012-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2019-EN004"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2018-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-AE703"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-EN601"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCPS-EN602"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-003"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WC5-EN002"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="REDU-DESP1"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="2019-EN001"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="WCS-EN401"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="LC5D-EN156"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SOVR-EN087"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="SOD-EN059"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DP07-EN024"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DOCS-ENSE4"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DL16-EN018"		Set Variable		True	ELSE	Set Variable		${bool}	
	${bool}=			Run Keyword If		"${code}"=="DL16-EN017"		Set Variable		True	ELSE	Set Variable		${bool}	
	[Return]			${bool}
	#####
	
Create Price List
	[Arguments]			${NewCardList}				${cardcount}
	#//Create the file
	${filestring}=			Set Variable			${cardcount}\n
	:FOR	${cardinfo}	IN		@{NewCardList}
	\		${filestring}=		Set Variable		${filestring}${cardinfo}\n
	
	Create File				${EXECDIR}/PricesList.txt		${filestring}
	
Scan Set Group
	[Arguments]						${boosterPackXpath}		${groupName}
	${boosterPackXpath}=			Set Variable			${boosterPackXpath}
	${yeahXpath}=					Set Variable			//div[@class="pack_m${SPACE}${SPACE}open${SPACE}"]
	${fullyearxpath}=				Set Variable			${boosterPackXpath}${yeahXpath}
	${years}=						Get Matching Xpath Count			${fullyearxpath}
	${loopIterations}=				Evaluate			${years} + 1
	
	@{yearlist}=					Create List
	@{yearsetslist}=				Create List
	:FOR	${i}	IN RANGE		1		${loopIterations}
	\		${index}=				Set Variable				[${i}]
	\		${thisyear}=			Get Text					${fullyearxpath}${index}/p
	\		Append To List			${yearlist}					${thisyear}
	# \		Run Keyword If 			"${i}"!="1"		Click Element		${fullyearxpath}${index}
	# \		Sleep					1s
	\		${yearline}=			Get This Year Pack String	${thisyear}		${boosterPackXpath}		${index}
	\		Append to List			${yearsetslist}				${yearline}
	
	${filestring}=					Set Variable				${groupName}\n${years}\n
	:FOR	${i}	IN RANGE		0		${years}
	\		${filestring}=			Set Variable				${filestring}${yearsetslist}[${i}]\n
	
	Create File				${EXECDIR}/${groupName}.txt		${filestring}
	
Scan Set Group2
	[Arguments]						${boosterPackXpath}		${groupName}
	${boosterPackXpath}=			Set Variable			${boosterPackXpath}
	${yeahXpath}=					Set Variable			//div[@class="pack_m open"]
	${fullyearxpath}=				Set Variable			${boosterPackXpath}${yeahXpath}
	${years}=						Get Matching Xpath Count			${fullyearxpath}
	${loopIterations}=				Evaluate			${years} + 1
	
	@{yearlist}=					Create List
	@{yearsetslist}=				Create List
	:FOR	${i}	IN RANGE		1		${loopIterations}
	\		${index}=				Set Variable				[${i}]
	\		${thisyear}=			Get Text					${fullyearxpath}${index}/p
	\		Append To List			${yearlist}					${thisyear}
	# \		Run Keyword If 			"${i}"!="1"		Click Element		${fullyearxpath}${index}
	# \		Sleep					1s
	\		${yearline}=			Get This Year Pack String	${thisyear}		${boosterPackXpath}		${index}
	\		Append to List			${yearsetslist}				${yearline}
	
	${filestring}=					Set Variable				${groupName}\n${years}\n
	:FOR	${i}	IN RANGE		0		${years}
	\		${filestring}=			Set Variable				${filestring}${yearsetslist}[${i}]\n
	
	Create File				${EXECDIR}/${groupName}.txt		${filestring}
	
	
Get This Year Pack String
	[Arguments]				${thisyear}		${boosterPackXpath}		${index}
	${numberofpacks}=		Get Matching Xpath Count	${boosterPackXpath}//div[@class="toggle"]${index}/div
	${loopIterations}=				Evaluate			${numberofpacks} + 1
	
	@{PackNameList}=				Create List
	:FOR	${i}	IN RANGE		1		${loopIterations}
	\		${packindex}=			Set Variable	[${i}]
	\		${xpath}=				Set Variable	${boosterPackXpath}//div[@class="toggle"]${index}/div${packindex}/p/strong
	\		${thispackname}=		Get text		${xpath}
	\		Append To List			${PackNameList}			${thispackname}
	
	#//Create the single string
	${retunvalue}=					Set Variable			${thisyear}|${numberofpacks}
	:FOR	${thissetname}	IN		@{PackNameList}
	\		${retunvalue}=			Set variable			${retunvalue}|${thissetname}
	
	[Return]				${retunvalue}