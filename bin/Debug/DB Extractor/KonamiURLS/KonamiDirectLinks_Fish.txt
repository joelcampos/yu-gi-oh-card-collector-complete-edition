100
7 Colored Fish|/yugiohdb/card_search.action?ope=2&cid=4446
Abyss Keeper|/yugiohdb/card_search.action?ope=2&cid=16938
Abyss Shark|/yugiohdb/card_search.action?ope=2&cid=17039
Abyssal Kingshark|/yugiohdb/card_search.action?ope=2&cid=7336
Amazon of the Seas|/yugiohdb/card_search.action?ope=2&cid=4632
Amphibian Beast|/yugiohdb/card_search.action?ope=2&cid=5066
Arionpos, Serpent of the Ghoti|/yugiohdb/card_search.action?ope=2&cid=18038
Askaan, the Bicorned Ghoti|/yugiohdb/card_search.action?ope=2&cid=17753
Beautunaful Princess|/yugiohdb/card_search.action?ope=2&cid=11065
Big Jaws|/yugiohdb/card_search.action?ope=2&cid=9622
Big Whale|/yugiohdb/card_search.action?ope=2&cid=10489
Bottom Dweller|/yugiohdb/card_search.action?ope=2&cid=4445
Buzzsaw Shark|/yugiohdb/card_search.action?ope=2&cid=15005
Chrysalis Dolphin|/yugiohdb/card_search.action?ope=2&cid=6764
Citadel Whale|/yugiohdb/card_search.action?ope=2&cid=13177
Cranium Fish|/yugiohdb/card_search.action?ope=2&cid=7335
Crazy Fish|/yugiohdb/card_search.action?ope=2&cid=4513
Creeping Doom Manta|/yugiohdb/card_search.action?ope=2&cid=6194
Crystal Shark|/yugiohdb/card_search.action?ope=2&cid=17040
Cure Mermaid|/yugiohdb/card_search.action?ope=2&cid=5204
Cyber Shark|/yugiohdb/card_search.action?ope=2&cid=7852
Deep Sweeper|/yugiohdb/card_search.action?ope=2&cid=10229
Deepsea Macrotrema|/yugiohdb/card_search.action?ope=2&cid=7564
Deepsea Shark|/yugiohdb/card_search.action?ope=2&cid=4443
Depth Shark|/yugiohdb/card_search.action?ope=2&cid=10851
Double Fin Shark|/yugiohdb/card_search.action?ope=2&cid=10849
Double Shark|/yugiohdb/card_search.action?ope=2&cid=10369
Dream Shark|/yugiohdb/card_search.action?ope=2&cid=17641
Eagle Shark|/yugiohdb/card_search.action?ope=2&cid=10492
Eanoc, Sentry of the Ghoti|/yugiohdb/card_search.action?ope=2&cid=17752
Earthbound Immortal Chacu Challhua|/yugiohdb/card_search.action?ope=2&cid=8308
Enchanting Mermaid|/yugiohdb/card_search.action?ope=2&cid=4257
Fishborg Archer|/yugiohdb/card_search.action?ope=2&cid=10460
Fishborg Blaster|/yugiohdb/card_search.action?ope=2&cid=8318
Fishborg Doctor|/yugiohdb/card_search.action?ope=2&cid=11001
Fishborg Launcher|/yugiohdb/card_search.action?ope=2&cid=10111
Fishborg Planter|/yugiohdb/card_search.action?ope=2&cid=10252
Flyfang|/yugiohdb/card_search.action?ope=2&cid=9636
Flying Fish|/yugiohdb/card_search.action?ope=2&cid=5065
Flying Red Carp|/yugiohdb/card_search.action?ope=2&cid=14608
Fortress Whale|/yugiohdb/card_search.action?ope=2&cid=4724
Friller Rabca|/yugiohdb/card_search.action?ope=2&cid=9726
Gazer Shark|/yugiohdb/card_search.action?ope=2&cid=11035
Ghoti of the Deep Beyond|/yugiohdb/card_search.action?ope=2&cid=17754
Gishki Abyss|/yugiohdb/card_search.action?ope=2&cid=9158
Gishki Grimness|/yugiohdb/card_search.action?ope=2&cid=18163
Gladiator Beast Murmillo|/yugiohdb/card_search.action?ope=2&cid=7284
Gladiator Beast Torax|/yugiohdb/card_search.action?ope=2&cid=7481
Gluttonous Reptolphin Greethys|/yugiohdb/card_search.action?ope=2&cid=15506
Golden Flying Fish|/yugiohdb/card_search.action?ope=2&cid=7566
Great White|/yugiohdb/card_search.action?ope=2&cid=4066
Guoglim, Spear of the Ghoti|/yugiohdb/card_search.action?ope=2&cid=18039
Hammer Shark|/yugiohdb/card_search.action?ope=2&cid=10031
Hyper-Ancient Shark Megalodon|/yugiohdb/card_search.action?ope=2&cid=10371
Infernalqueen Salmon|/yugiohdb/card_search.action?ope=2&cid=17786
Ixeep, Omen of the Ghoti|/yugiohdb/card_search.action?ope=2&cid=18036
Lantern Shark|/yugiohdb/card_search.action?ope=2&cid=15004
Left-Hand Shark|/yugiohdb/card_search.action?ope=2&cid=14977
Lifeless Leaffish|/yugiohdb/card_search.action?ope=2&cid=15267
Man-eating Black Shark|/yugiohdb/card_search.action?ope=2&cid=4571
Marine Beast|/yugiohdb/card_search.action?ope=2&cid=4623
Mega Fortress Whale|/yugiohdb/card_search.action?ope=2&cid=17046
Mermaid Shark|/yugiohdb/card_search.action?ope=2&cid=11034
Mermail Abysslung|/yugiohdb/card_search.action?ope=2&cid=10241
Mermail Abyssmander|/yugiohdb/card_search.action?ope=2&cid=10351
Mermail Abyssnose|/yugiohdb/card_search.action?ope=2&cid=10392
Mermail Abysspike|/yugiohdb/card_search.action?ope=2&cid=10240
Mermail Abyssturge|/yugiohdb/card_search.action?ope=2&cid=10239
Metabo-Shark|/yugiohdb/card_search.action?ope=2&cid=8599
Minairuka|/yugiohdb/card_search.action?ope=2&cid=18450
Misairuzame|/yugiohdb/card_search.action?ope=2&cid=4548
Needle Sunfish|/yugiohdb/card_search.action?ope=2&cid=9728
Nimble Angler|/yugiohdb/card_search.action?ope=2&cid=10253
Nimble Manta|/yugiohdb/card_search.action?ope=2&cid=9580
Nimble Sunfish|/yugiohdb/card_search.action?ope=2&cid=9038
Oceans Keeper|/yugiohdb/card_search.action?ope=2&cid=7333
Oyster Meister|/yugiohdb/card_search.action?ope=2&cid=7663
Paces, Light of the Ghoti|/yugiohdb/card_search.action?ope=2&cid=17750
Panther Shark|/yugiohdb/card_search.action?ope=2&cid=10491
Performapal Seal Eel|/yugiohdb/card_search.action?ope=2&cid=12594
Performapal Sword Fish|/yugiohdb/card_search.action?ope=2&cid=11216
Phantom Dragonray Bronto|/yugiohdb/card_search.action?ope=2&cid=7339
Piercing Moray|/yugiohdb/card_search.action?ope=2&cid=9842
Piranha Army|/yugiohdb/card_search.action?ope=2&cid=5968
Rage of the Deep Sea|/yugiohdb/card_search.action?ope=2&cid=9581
Rare Fish|/yugiohdb/card_search.action?ope=2&cid=4236
Ravenous Crocodragon Archethys|/yugiohdb/card_search.action?ope=2&cid=15029
Right-Hand Shark|/yugiohdb/card_search.action?ope=2&cid=14976
Rock Scales|/yugiohdb/card_search.action?ope=2&cid=17229
Root Water|/yugiohdb/card_search.action?ope=2&cid=4253
Royal Swamp Eel|/yugiohdb/card_search.action?ope=2&cid=8144
Saber Shark|/yugiohdb/card_search.action?ope=2&cid=10852
Scrap Shark|/yugiohdb/card_search.action?ope=2&cid=9321
Shark Caesar|/yugiohdb/card_search.action?ope=2&cid=10652
Shark Cruiser|/yugiohdb/card_search.action?ope=2&cid=8319
Shark Fortress|/yugiohdb/card_search.action?ope=2&cid=10529
Shark Stickers|/yugiohdb/card_search.action?ope=2&cid=9727
Sharkraken|/yugiohdb/card_search.action?ope=2&cid=10488
Shif, Fairy of the Ghoti|/yugiohdb/card_search.action?ope=2&cid=17751
Shocktopus|/yugiohdb/card_search.action?ope=2&cid=9880