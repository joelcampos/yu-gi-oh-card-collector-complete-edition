100
Amazement Abomination Arlekino|/yugiohdb/card_search.action?ope=2&cid=18486
Amazement Administrator Arlekino|/yugiohdb/card_search.action?ope=2&cid=15967
Ariel, Priestess of the Nekroz|/yugiohdb/card_search.action?ope=2&cid=12945
Armored Axon Kicker|/yugiohdb/card_search.action?ope=2&cid=8320
Armored Kappa|/yugiohdb/card_search.action?ope=2&cid=10466
Caam, Serenity of Gusto|/yugiohdb/card_search.action?ope=2&cid=9168
Chosen by the World Chalice|/yugiohdb/card_search.action?ope=2&cid=13058
Chronomaly Crystal Chrononaut|/yugiohdb/card_search.action?ope=2&cid=10172
Crusadia Maximus|/yugiohdb/card_search.action?ope=2&cid=13730
D.D. Seeker|/yugiohdb/card_search.action?ope=2&cid=13401
D.D. Telepon|/yugiohdb/card_search.action?ope=2&cid=9745
Daigusto Eguls|/yugiohdb/card_search.action?ope=2&cid=9181
Daigusto Falcos|/yugiohdb/card_search.action?ope=2&cid=9600
Daigusto Gulldos|/yugiohdb/card_search.action?ope=2&cid=9180
Daigusto Laplampilica|/yugiohdb/card_search.action?ope=2&cid=16229
Daigusto Sphreez|/yugiohdb/card_search.action?ope=2&cid=9395
Destructotron|/yugiohdb/card_search.action?ope=2&cid=7721
Dimensional Allotrope Varis|/yugiohdb/card_search.action?ope=2&cid=18173
Doctor Cranium|/yugiohdb/card_search.action?ope=2&cid=7715
Dr. Frankenderp|/yugiohdb/card_search.action?ope=2&cid=11985
El Shaddoll Wendigo|/yugiohdb/card_search.action?ope=2&cid=11567
Esper Girl|/yugiohdb/card_search.action?ope=2&cid=9494
Final Psychic Ogre|/yugiohdb/card_search.action?ope=2&cid=8968
Fullmetalfoes Alkahest|/yugiohdb/card_search.action?ope=2&cid=12629
Gagaku-P.U.N.K. Wa Gon|/yugiohdb/card_search.action?ope=2&cid=16725
Genetic Woman|/yugiohdb/card_search.action?ope=2&cid=8321
Genomix Fighter|/yugiohdb/card_search.action?ope=2&cid=10770
Ghost Fairy Elfobia|/yugiohdb/card_search.action?ope=2&cid=10599
Ghost Ogre & Snow Rabbit|/yugiohdb/card_search.action?ope=2&cid=11708
Gold Pride - Roller Baller|/yugiohdb/card_search.action?ope=2&cid=18669
Grapple Blocker|/yugiohdb/card_search.action?ope=2&cid=7866
Gravity Controller|/yugiohdb/card_search.action?ope=2&cid=14858
Heavymetalfoes Amalgam|/yugiohdb/card_search.action?ope=2&cid=15740
Heavymetalfoes Electrumite|/yugiohdb/card_search.action?ope=2&cid=13507
HTS Psyhemuth|/yugiohdb/card_search.action?ope=2&cid=10649
Hu-Li the Jewel Mikanko|/yugiohdb/card_search.action?ope=2&cid=18482
Hushed Psychic Cleric|/yugiohdb/card_search.action?ope=2&cid=9498
Hyper Psychic Blaster|/yugiohdb/card_search.action?ope=2&cid=8037
Hyper Psychic Blaster/Assault Mode|/yugiohdb/card_search.action?ope=2&cid=8018
Hyper Psychic Riser|/yugiohdb/card_search.action?ope=2&cid=14119
Hypnosister|/yugiohdb/card_search.action?ope=2&cid=11252
Impcantation Penciplume|/yugiohdb/card_search.action?ope=2&cid=13910
Joruri-P.U.N.K. Madame Spider|/yugiohdb/card_search.action?ope=2&cid=16726
Kamui, Hope of Gusto|/yugiohdb/card_search.action?ope=2&cid=9595
Kashtira Fenrir|/yugiohdb/card_search.action?ope=2&cid=17768
Kashtira Ogre|/yugiohdb/card_search.action?ope=2&cid=17770
Kashtira Shangri-Ira|/yugiohdb/card_search.action?ope=2&cid=17800
Kashtira Unicorn|/yugiohdb/card_search.action?ope=2&cid=17769
Kozmo Farmgirl|/yugiohdb/card_search.action?ope=2&cid=12033
Kozmo Goodwitch|/yugiohdb/card_search.action?ope=2&cid=12034
Kozmo Scaredy Lion|/yugiohdb/card_search.action?ope=2&cid=12385
Kozmo Soartroopers|/yugiohdb/card_search.action?ope=2&cid=12244
Kozmo Strawman|/yugiohdb/card_search.action?ope=2&cid=12100
Kozmo Tincan|/yugiohdb/card_search.action?ope=2&cid=12243
Kozmoll Dark Lady|/yugiohdb/card_search.action?ope=2&cid=12386
Kozmoll Wickedwitch|/yugiohdb/card_search.action?ope=2&cid=12101
Krebons|/yugiohdb/card_search.action?ope=2&cid=7716
Leafplace Plaice|/yugiohdb/card_search.action?ope=2&cid=16834
Libromancer Geek Boy|/yugiohdb/card_search.action?ope=2&cid=17232
Lifeforce Harmonizer|/yugiohdb/card_search.action?ope=2&cid=8024
Magical Android|/yugiohdb/card_search.action?ope=2&cid=7737
Master Gig|/yugiohdb/card_search.action?ope=2&cid=8182
Mekk-Knight Avram|/yugiohdb/card_search.action?ope=2&cid=13570
Mekk-Knight Blue Sky|/yugiohdb/card_search.action?ope=2&cid=13384
Mekk-Knight Green Horizon|/yugiohdb/card_search.action?ope=2&cid=13385
Mekk-Knight Indigo Eclipse|/yugiohdb/card_search.action?ope=2&cid=13389
Mekk-Knight Orange Sunset|/yugiohdb/card_search.action?ope=2&cid=13386
Mekk-Knight Purple Nightfall|/yugiohdb/card_search.action?ope=2&cid=13390
Mekk-Knight Red Moon|/yugiohdb/card_search.action?ope=2&cid=13388
Mekk-Knight Yellow Star|/yugiohdb/card_search.action?ope=2&cid=13387
Mental Seeker|/yugiohdb/card_search.action?ope=2&cid=9495
Mental Tuner|/yugiohdb/card_search.action?ope=2&cid=18157
Metalfoes Adamante|/yugiohdb/card_search.action?ope=2&cid=12442
Metalfoes Crimsonite|/yugiohdb/card_search.action?ope=2&cid=12444
Metalfoes Goldriver|/yugiohdb/card_search.action?ope=2&cid=12422
Metalfoes Mithrilium|/yugiohdb/card_search.action?ope=2&cid=12630
Metalfoes Orichalc|/yugiohdb/card_search.action?ope=2&cid=12443
Metalfoes Silverd|/yugiohdb/card_search.action?ope=2&cid=12421
Metalfoes Steelen|/yugiohdb/card_search.action?ope=2&cid=12420
Metalfoes Vanisher|/yugiohdb/card_search.action?ope=2&cid=15712
Metalfoes Volflame|/yugiohdb/card_search.action?ope=2&cid=12423
Mind Master|/yugiohdb/card_search.action?ope=2&cid=7714
Mind Protector|/yugiohdb/card_search.action?ope=2&cid=7717
Musto, Oracle of Gusto|/yugiohdb/card_search.action?ope=2&cid=9596
Myutant GB-88|/yugiohdb/card_search.action?ope=2&cid=15773
Myutant M-05|/yugiohdb/card_search.action?ope=2&cid=15568
Myutant Mutant|/yugiohdb/card_search.action?ope=2&cid=16824
Myutant ST-46|/yugiohdb/card_search.action?ope=2&cid=15569
Myutant Synthesis|/yugiohdb/card_search.action?ope=2&cid=15774
Myutant Ultimus|/yugiohdb/card_search.action?ope=2&cid=15775
Naelshaddoll Ariel|/yugiohdb/card_search.action?ope=2&cid=14966
Nightmell the Dark Bonder|/yugiohdb/card_search.action?ope=2&cid=17440
Noh-P.U.N.K. Ze Amin|/yugiohdb/card_search.action?ope=2&cid=16727
Number 18: Heraldry Patriarch|/yugiohdb/card_search.action?ope=2&cid=10975
Number 26: Spaceway Octobypass|/yugiohdb/card_search.action?ope=2&cid=14563
Number 69: Heraldry Crest|/yugiohdb/card_search.action?ope=2&cid=10323
Number 74: Master of Blades|/yugiohdb/card_search.action?ope=2&cid=10706
Number 89: Diablosis the Mind Hacker|/yugiohdb/card_search.action?ope=2&cid=13166
Number 8: Heraldic King Genom-Heritage|/yugiohdb/card_search.action?ope=2&cid=10267
Number C69: Heraldry Crest of Horror|/yugiohdb/card_search.action?ope=2&cid=10935