*** Setting ***
Library				Selenium2Library
Library				String
Library				Collections
Library				OperatingSystem
Library 			DateTime

Suite Setup 		YGO_Setup
Suite Teardown		SuiteTeardown

Test Setup			Log To Console			\nStarting DB extraction
Test Teardown  	 	TestTeardown

*** Variables ***
${WIN_POS}				-2000
&{KonamiURLs}
&{ProDeckURLs}
&{PriceListExtracted}
&{TCGURLS}
${CURRENT_PAGE}			1
${CARDTYPE}				NONE

#//Runtime Data
${GROUPCARDCOUNT}
@{GroupCardList}
@{FailedProSearchCardList}

*** Test Case ***
TC-YGO-001 Aqua Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Aqua
	TCMasterKeyword
	#####
	
TC-YGO-002 Beast Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Beast
	TCMasterKeyword
	#####

TC-YGO-003 Beast-Warrior Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Beast-Warrior
	TCMasterKeyword
	#####
	
TC-YGO-004 Cyberse Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Cyberse
	TCMasterKeyword
	#####
	
TC-YGO-005 Dinosaur Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Dinosaur
	TCMasterKeyword
	#####
	
TC-YGO-006 Divine-Beast Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Divine-Beast
	TCMasterKeyword
	#####
	
TC-YGO-007 Dragon Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Dragon
	TCMasterKeyword
	#####
	
TC-YGO-008 Fairy Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Fairy
	TCMasterKeyword
	#####
	
TC-YGO-009 Fiend Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Fiend
	TCMasterKeyword
	#####
	
TC-YGO-010 Insect Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Insect
	TCMasterKeyword
	#####
	
TC-YGO-011 Fish Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Fish
	TCMasterKeyword
	#####
	
TC-YGO-012 Plant Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Plant
	TCMasterKeyword
	#####
	
TC-YGO-013 Machine Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Machine
	TCMasterKeyword
	#####
	
TC-YGO-014 Psychic Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Psychic
	TCMasterKeyword
	#####
	
TC-YGO-015 Pyro Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Pyro
	TCMasterKeyword
	#####
	
TC-YGO-016 Reptile Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Reptile
	TCMasterKeyword
	#####
	
TC-YGO-017 Rock Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Rock
	TCMasterKeyword
	#####
	
TC-YGO-018 Sea Serpent Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Sea Serpent
	TCMasterKeyword
	#####

TC-YGO-019 Spellcaster Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Spellcaster
	TCMasterKeyword
	#####
	
TC-YGO-020 Thunder Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Thunder
	TCMasterKeyword
	#####
	
TC-YGO-021 Warrior Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Warrior
	TCMasterKeyword
	#####
	
TC-YGO-022 Wyrm Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Wyrm
	TCMasterKeyword
	#####
	
TC-YGO-023 Winged Beast Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Winged Beast
	TCMasterKeyword
	#####
	
TC-YGO-024 Zombie Monsters
	####
	Set Suite Variable		${CARDTYPE}		Monster
	Set Suite Variable		${CURRENTGROUP}	Zombie
	TCMasterKeyword
	#####

TC-YGO-025 Normal Spells
	####
	Set Suite Variable		${CARDTYPE}		Spell
	Set Suite Variable		${CURRENTGROUP}	Normal
	TCMasterKeyword
	#####

TC-YGO-026 Continuous Spells
	####
	Set Suite Variable		${CARDTYPE}		Spell
	Set Suite Variable		${CURRENTGROUP}	Continuous
	TCMasterKeyword
	#####
	
TC-YGO-027 Equip Spells
	####
	Set Suite Variable		${CARDTYPE}		Spell
	Set Suite Variable		${CURRENTGROUP}	Equip
	TCMasterKeyword
	#####
	
TC-YGO-028 Field Spells
	####
	Set Suite Variable		${CARDTYPE}		Spell
	Set Suite Variable		${CURRENTGROUP}	Field
	TCMasterKeyword
	#####
	
TC-YGO-029 Quick-Play Spells
	####
	Set Suite Variable		${CARDTYPE}		Spell
	Set Suite Variable		${CURRENTGROUP}	Quick-Play
	TCMasterKeyword
	#####
	
TC-YGO-030 Ritual Spells
	####
	Set Suite Variable		${CARDTYPE}		Spell
	Set Suite Variable		${CURRENTGROUP}	Ritual
	TCMasterKeyword
	#####

TC-YGO-031 Normal Traps
	####
	Set Suite Variable		${CARDTYPE}		Trap
	Set Suite Variable		${CURRENTGROUP}	Normal
	TCMasterKeyword
	#####

TC-YGO-032 Continuous Traps
	####
	Set Suite Variable		${CARDTYPE}		Trap
	Set Suite Variable		${CURRENTGROUP}	Continuous
	TCMasterKeyword
	#####

TC-YGO-033 Counter Traps
	####
	Set Suite Variable		${CARDTYPE}		Trap
	Set Suite Variable		${CURRENTGROUP}	Counter
	TCMasterKeyword
	#####
	
TC-YGO-034 Scan Sets
	####
	Set Suite Variable		${CARDTYPE}		SETS
	Set Suite Variable		${CURRENTGROUP}	Sets
	
	Go To				https://www.db.yugioh-card.com/yugiohdb/card_list.action
	Wait Until Element Is Visible		//h1[.='Card Lists']
	
	#//Do the products
	Click Element				//span[.='Products']
	Sleep						3s
	
	@{XpathsList}=					Create List
	Append To List					${XpathsList}			//div[@id="list_title_1"]
	Append To List					${XpathsList}			//div[@id="list_title_2"]
	Append To List					${XpathsList}			//div[@id="list_title_3"]
	Append To List					${XpathsList}			//div[@id="list_title_4"]
	Append To List					${XpathsList}			//div[@id="list_title_5"]
	Append To List					${XpathsList}			//div[@id="list_title_20"]
	Append To List					${XpathsList}			//div[@id="list_title_60"]
	Append To List					${XpathsList}			//div[@id="list_title_70"]
	Append To List					${XpathsList}			//div[@id="list_title_80"]
	
	@{nameList}=					Create List
	Append To List					${nameList}				Booster Packs
	Append To List					${nameList}				Special Edition Boxes
	Append To List					${nameList}				Starter Decks
	Append To List					${nameList}				Structure Decks
	Append To List					${nameList}				Tins
	Append To List					${nameList}				SPEED DUEL
	Append To List					${nameList}				Duelist Packs
	Append To List					${nameList}				Duel Termina Cards
	Append To List					${nameList}				Others
	
	
	:FOR	${i}	IN RANGE		0		9
	\				Scan Set Group			${XpathsList}[${i}]				${nameList}[${i}]
	
	#//Do the bundles
	Click Element				//span[.='Perks/Bundles']
	Sleep						3s
	
	@{XpathsList2}=					Create List
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][1]
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][2]
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][3]
	Append To List					${XpathsList2}			//div[@id="card_list_2"]//div[@class="pac_set"][4]
	
	@{nameList2}=					Create List
	Append To List					${nameList2}				Magazines, Books, Comics
	Append To List					${nameList2}				Tournaments
	Append To List					${nameList2}				Promotional Cards
	Append To List					${nameList2}				Video Game Bundles
	
	:FOR	${i}	IN RANGE		0		4
	\				Scan Set Group2			${XpathsList2}[${i}]				${nameList2}[${i}]
	#####

*** Keywords 
TCMasterKeyword
	####
	#//Step 1: Search the group
	Run Keyword If			"${CARDTYPE}"=="Monster"	Konami_Search Monster Group			${CURRENTGROUP}
	Run Keyword If			"${CARDTYPE}"=="Spell"		Konami_Search Spell Group			${CURRENTGROUP}
	Run Keyword If			"${CARDTYPE}"=="Trap"		Konami_Search Trap Group			${CURRENTGROUP}
	
	#//Step 2: Determine how many pages this group HasPendulum
	${GROUPCARDCOUNT}=		CardList_Get Group Card Count
	Set Suite Variable		${GROUPCARDCOUNT}		${GROUPCARDCOUNT}
	${PageCount}=			CardList_Get Pages Count
	
	#//Step 3: Scan all the Names/Direct URLs from the pages
	:FOR	${i}	IN RANGE		0		${PageCount}
	#//Extract all the data for that page
	\		CardList_Extract Names and URLs
	#//As long as the iteration is NOT on the last page, go to the next page
	\		${LastIterationIndex}=	Evaluate	${PageCount} - 1	
	\		Run Keyword If			${i}!=${LastIterationIndex}		CardList_Go To Next Page
	
	#//Step 4: Go to each URL to extract each CARD KONAMI INFO
	:FOR    ${key}    IN    @{KonamiURLs}
	\		${SkipCard}=	Skip Card Validation	${key}
	\		Run Keyword If			${SkipCard}		Continue For Loop
	\		${CardNavSuccess}=		Run Keyword and Return Status	Konami_Go To Direct Card Page		${KonamiURLs['${key}']}
	\		Run Keyword If			${CardNavSuccess}==False		Continue For Loop
	\		${CardDataString}		${cardname}=		Run Keyword If		"${CARDTYPE}"=="Monster"	CardInfo_Scan Monster Card	ELSE	CardInfo_Scan SpellTrap Card
	\		@{CodeList}=			Create List
	\		${SetsString}	${CodeList}=			CardInfo_Scan Sets
	
	#//Step 5: Go to Prodeck to obtain its ID
	\		${SearchSucessful}=		Run Keyword And Return Status		Search Card In Prodeck	${key}	${cardname}
	\		${CardID}=		Run Keyword If		${SearchSucessful}		ProDeckCard_Extract ID	ELSE	Set Variable	IDMISSING
	\		Run Keyword If		${SearchSucessful}==False	Append To List	${FailedProSearchCardList}	${key}
	
	#//EX: Check for cards with custom IDs (mostly illegal prize cards)
	\		${CardID}=			Override Custom ID		${key}		${CardID}
	
	#//Step 6: Extract prices
	\		${PricesString}=		Run Keyword If		${SearchSucessful}	ProDeckCard_ExtractPriceList		${CodeList}		ELSE	Set Variable		MISSING_PRICES
	\		${PricesString}=		Override Prices		${key}		${CodeList}		${PricesString}
	
	#//Step 7: Gather all the info to create the data to write
	\		${FullCardInfoString}=	Set Variable		${CardID}|${CardDataString}|${SetsString}${PricesString}
	\		Append to List			${GroupCardList}		${FullCardInfoString}
	\		Log To Console			Card Extracted: [${cardname}]
	#####
	
Skip Card Validation
	####
	[Arguments]			${key}
	${SkipON}=			Set Variable			False
	#//LINE TO TEST SINGLE CARDS: ${SkipON}=			Run Keyword If			"${key}"=="Mementotlan-Horned Dragon"				Set Variable	False		ELSE	Set Variable	True
	# ${cardone}=			Run Keyword If			"${key}"=="Blue-Eyes White Dragon"				Set Variable	False		ELSE	Set Variable	True
	# ${cardtwo}=			Run Keyword If			"${key}"=="Yosenju Wind Worship"		Set Variable	False		ELSE	Set Variable	True
	# ${cardthree}=		Run Keyword If			"${key}"=="Zombie Power Struggle"		Set Variable	False		ELSE	Set Variable	True
	# [Return]			${SkipON} and ${cardone} and ${cardtwo} and ${cardthree}
	# [Return]			${SkipON} and ${cardone} and ${cardtwo} and ${cardthree}
	# [Return]			${SkipON} and ${cardone}
	[Return]			${SkipON}
	#####
	
YGO_Setup
	####
	#//Open the Browser
	Set Selenium Timeout				15
	${chrome_options}=    				Evaluate    sys.modules['selenium.webdriver.chrome.options'].Options()    sys, selenium.webdriver.chrome.options
    Create Webdriver    				Chrome
	#//Go To Konami DB Card Search Page
	Sleep								1s
	YGO_Go to Card Search
	Set Window Position					${WIN_POS}		0
	Maximize Browser Window
	#//Try to accept cockies
	Run Keyword and Ignore Error		Click Element		//button[@id="onetrust-accept-btn-handler"]
	#//Initialize Prodeck direct urls
	YGO_Initialize Prodeck URLs
	#####
	
TestTeardown
	####
	#//Create the final File
	${FileData}=			Create File String		${GroupCardList}
	${FileData}=			Set Variable			${GROUPCARDCOUNT}\n${FileData}
	Create File				${EXECDIR}/ExtractedFiles/${CURRENTGROUP} ${CARDTYPE}.txt		${FileData}
	
	#//Reset the konami URL List
	:FOR    ${key}    IN    @{KonamiURLs}
	\		Remove From Dictionary		${KonamiURLs}	${key}
	#//Clear the group card List
	@{GroupCardList}=	Create List
	Set Suite Variable	${GroupCardList}	${GroupCardList}
	
	YGO_Go to Card Search
	Set Suite Variable		${CURRENT_PAGE}			1
	#####

SuiteTeardown
	####
	#//Create a file with the failed cards searchs from prodeck
	${FileData}=			Create File String		${FailedProSearchCardList}
	Create File				${EXECDIR}/ExtractedFiles/FailedProSearchCards.txt		${FileData}

	#//Create a full price list File
	${Filedata}=		Get Length			${PriceListExtracted}
	Set To Dictionary   	${PriceListExtracted}    LASTLINE=NONE
	:FOR    ${key}    IN    @{PriceListExtracted}
	\		${Line}=			Set Variable		${key}|${PriceListExtracted['${key}']}
	\		${Filedata}=		Set Variable		${Filedata}\n${Line}
	Create File				${EXECDIR}/ExtractedFiles/FullPriceList.txt		${FileData}
	
	
	#//Create a file with the TCG urls
	${Filedata}=		Get Length			${TCGURLS}
	Set To Dictionary   	${TCGURLS}    LASTLINE=NONE
	:FOR    ${key}    IN    @{TCGURLS}
	\		${Line}=			Set Variable		${key}|${TCGURLS['${key}']}
	\		${Filedata}=		Set Variable		${Filedata}\n${Line}
	Create File				${EXECDIR}/ExtractedFiles/TCGURLSExtracted.txt		${FileData}
	
	Close All Browsers
	#####
	
YGO_Initialize Prodeck URLs
	####
	${FILE_PATH}				Set Variable		${EXECDIR}/ProdeckURLS/URLsDB.txt
	${FILE_CONTENT}=			Get File								${FILE_PATH}
	@{LINES}=					Split To Lines							${FILE_CONTENT}
	${URLsCount}=				Convert To Integer	${LINES}[0]
	
	:FOR	${i}	IN RANGE		1		${URLsCount}+1
	\		${thisLine}=			Set Variable		${LINES}[${i}]
	\		@{LineTokens}=			Split String		${thisLine}		|
	#//		 ${LineTokens}[0] == Card Name | ${LineTokens}[1] == Prodeck URL
	\		Set To Dictionary   	${ProDeckURLs}    ${LineTokens}[0]=${LineTokens}[1]
	#####
	
YGO_Go to Card Search
	####
	Go To	https://www.db.yugioh-card.com/yugiohdb/card_search.action
	#//Try to accept cockies
	Run Keyword and Ignore Error		Click Element		//button[@id="onetrust-accept-btn-handler"]
	Sleep			2s
	#####
	
YGO_Add Card URL to File
	####
	[Arguments]			${cardname}
	#//Get the URL of this page
	${currentURL}=	Get Location
	Set To Dictionary   	${ProDeckURLs}    ${cardname}=${currentURL}
	#//Generate the string to save
	${totalcount}=		Get Length			${ProDeckURLs}
	${STRINGLINE}=		Set Variable		${totalcount}
	:FOR    ${key}    IN    @{ProDeckURLs}
	\	${Line}=			Set Variable		${key}|${ProDeckURLs['${key}']}
	\	${STRINGLINE}=		Set Variable		${STRINGLINE}\n${Line}
	#//Save File
	Create File				${EXECDIR}/ProdeckURLS/URLsDB.txt		${STRINGLINE}
	#####
	
Konami_Search Monster Group
	####
	[Arguments]			${type}
	#//Click on the Monster Type Selection
	Run Keyword If		"${type}"=="Warrior"						Click Element		//li[@class="species_4_en"]//span
	Run Keyword If		"${type}"=="Beast"							Click Element		//li[@class="species_6_en"]//span
	Run Keyword If		"${type}"!="Warrior" and "${type}"!="Beast"	Click Element		//span[contains(text(), "${type}")]
	#//Click Search and Wait for the results page
	Click Element						//div[@id="submit_area"]//span[.='Search']
	Wait Until Element Is Visible		//div[contains(text(), "Results")]
	#//Accept cockies 
	Run Keyword And Ignore Error		//button[@id="onetrust-accept-btn-handler"]
	Sleep			2s
	
	#//Select Click View 100 Items
	Click Element		//select[@id="rp"]
	Click Element		//option[.='Show 100 items per page.']
	Click Element		//span[.='View as List']
	Sleep				3s
	#####

Konami_Search Spell Group
	####
	[Arguments]			${type}
	#//Click on the Spell Cards Tab
	Click Element		//span[.='Spell Cards']
	Sleep				1s
	Click Element		//ul[@class="fliter_btns filter_effect_magic"]//span[contains(text(), "${type}")]
	#//Click Search and Wait for the results page
	Click Element						//div[@id="submit_area"]//span[.='Search']
	Wait Until Element Is Visible		//div[contains(text(), "Results")]
	#//Select Click View 100 Items
	Click Element		//select[@id="rp"]
	Click Element		//option[.='Show 100 items per page.']
	Click Element		//span[.='View as List']
	Sleep				3s
	#####
	
Konami_Search Trap Group
	####
	[Arguments]			${type}
	#//Click on the Spell Cards Tab
	Click Element		//span[.='Trap Cards']
	Sleep				1s
	Click Element		//ul[@class="fliter_btns filter_effect_trap"]//span[contains(text(), "${type}")]
	#//Click Search and Wait for the results page
	Click Element						//div[@id="submit_area"]//span[.='Search']
	Wait Until Element Is Visible		//div[contains(text(), "Results")]
	#//Select Click View 100 Items
	Click Element		//select[@id="rp"]
	Click Element		//option[.='Show 100 items per page.']
	Click Element		//span[.='View as List']
	Sleep				3s
	#####
	
Konami_Go To Direct Card Page
	####
	[Arguments]			${url}
	${fullURL}=			Set Variable	https://www.db.yugioh-card.com${url}
	Go To 				${fullURL}
	Wait Until Element Is Visible		//div[@id="cardname"]
	Wait Until Element Is Visible		//div[@id="update_list"]
	Wait Until Element Is Visible		//div[@id="card_frame"]
	#####

CardList_Get Group Card Count
	####
	${results}=			Get Text				//div[@class="text"]
	${extracted}=		Fetch From Right		${results}		of
	${extracted}=		Replace String			${extracted}	,		${EMPTY}
	${int}=				Convert To Integer		${extracted}
	[Return]			${int}
	#####
	
CardList_Get Pages Count
	####
	#//If card count is less or equal 100: there is only 1 page
	${pagescount}=		Run Keyword If		${GROUPCARDCOUNT} < 101		Set Variable	1		ELSE	Set Variable	0
	Run Keyword If		${GROUPCARDCOUNT} < 101	Return From Keyword    ${pagescount}
	
	#//else, there are multiple
	#//formula to calculate pages: (Groupcardcount/100)+1
	${pagescount}=		Evaluate	(${GROUPCARDCOUNT}/100) + 1
	[Return]			${pagescount}
	#####
	
ModCardName For Direct Link List
	####
	[Arguments]			${cardname}
	${cardnameModded}=			Set Variable	${cardname}
	${cardnameModded}=			Replace String	${cardnameModded}	'	${EMPTY}
	${cardnameModded}=			Replace String	${cardnameModded}	"	${EMPTY}
	${cardnameModded}=			Replace String	${cardnameModded}	ü	u
	${cardnameModded}=			Replace String	${cardnameModded}	Ü	u
	${cardnameModded}=			Replace String	${cardnameModded}	ú	u
	${cardnameModded}=			Replace String	${cardnameModded}	・	${EMPTY}
	${cardnameModded}=			Replace String	${cardnameModded}	é	e
	${cardnameModded}=			Replace String	${cardnameModded}	ä	a
	${cardnameModded}=			Replace String	${cardnameModded}	ñ	n
	${cardnameModded}=			Replace String	${cardnameModded}	α	${EMPTY}
	${cardnameModded}=			Replace String	${cardnameModded}	β	${EMPTY}
	${cardnameModded}=			Replace String	${cardnameModded}	Ω	${EMPTY}
	${cardnameModded}=			Replace String	${cardnameModded}	☆	${EMPTY}
	${cardnameModded}=			Replace String	${cardnameModded}	★	${EMPTY}
	[Return]			${cardnameModded}
	#####
	
CardList_Extract Names and URLs
	####
	${PageCardCount}=		Get Matching Xpath Count		//div[@id="card_list"]//div[@class="card_name flex_1"]/input
	:FOR	${i}	IN RANGE		1		${PageCardCount}+1
	\		${cardName}=		Get text				//div[@id="card_list"]/div/div[${i}]//div[@class="card_name flex_1"]//span
	#//Remove the "(updated from)" if contains in
	\		${cardName}=		CardList_Remove Name Change Note	${cardName}
	\		${url}=				Get Element Attribute	//div[@id="card_list"]/div/div[${i}]//div[@class="card_name flex_1"]//input@value
	\		${moddedCardName}=	ModCardName For Direct Link List		${cardName}
	\		Set To Dictionary   	${KonamiURLs}    ${moddedCardName}=${url}
	#####

CardList_Remove Name Change Note
	####
	[Arguments]			${cardname}
	${nameUpdated}=			Run Keyword And Return Status		Should Contain		${cardname}		(Updated from:
	${cardname}=			Run Keyword If		${nameUpdated}	Fetch From Left		${cardname}			${space}(Updated from:		ELSE		Set Variable	${cardname}
	[Return]				${cardname}
	#####
	
CardList_CreateDirectLinksFile
	####
	${totalcount}=		Get Length			${KonamiURLs}
	${STRINGLINE}=		Set Variable		${totalcount}
	:FOR    ${key}    IN    @{KonamiURLs} 
	\		${Line}=			Set Variable		${key}|${KonamiURLs['${key}']}
	\		${STRINGLINE}=		Set Variable		${STRINGLINE}\n${Line}
	#//Save File
	Create File				${EXECDIR}/KonamiURLS/KonamiDirectLinks_${CURRENTGROUP}.txt		${STRINGLINE}
	#####
	
CardList_Go To Next Page
	####
	${tmp}=	Evaluate		${CURRENT_PAGE} + 1
	Set Suite Variable		${CURRENT_PAGE}			${tmp}
	
	${y}=				Get Vertical Position		//a[.='${CURRENT_PAGE}']
	${scroll}=			Evaluate		int(float("${y}")) - 100
	Execute Javascript							scrollTo(0, ${scroll})
	
	Click Element		//a[.='${CURRENT_PAGE}']
	Sleep				3s
	#####

CardInfo_Scan Monster Card
	####
	#//Card Name
	${CARDNAME}=		Get Text		//div[@id="cardname"]//h1
	${nameUpdated}=		Run Keyword And Return Status		Should Contain		${CARDNAME}		(Updated from:
	${CARDNAME}=		Run Keyword If		${nameUpdated}	Fetch From Left		${CARDNAME}			${space}(Updated from:		ELSE		Set Variable	${CARDNAME}
	
	#//Monster Species
	${SpeciesCount}=	Get Matching Xpath Count		//p[@class="species"]/span
	${SPECIES}=			Set Variable	${EMPTY}
	:FOR	${i}	IN RANGE		1	${SpeciesCount}+1
	\		${specie}=	Get Text	//p[@class="species"]/span[${i}]
	\		${SPECIES}=	Set Variable		${SPECIES}${specie}
	
	
	#//Level/Rank/Link
	${LEVELRANKLINK}=		Get Text		//div[@id="CardTextSet"]/div[@class="CardText"][1]/div[@class="frame imgset"]/div[2]/span[2]
	#//ATA/DEF
	${ATA}=		Get Text		//div[@id="CardTextSet"]/div[@class="CardText"][1]/div[2]/div[1]/span[2]
	${DEF}=		Get Text		//div[@id="CardTextSet"]/div[@class="CardText"][1]/div[2]/div[2]/span[2]
	#//Attribute
	${ATTRIBUTE}=		Get Text	//div[@id="CardTextSet"]/div[@class="CardText"][1]/div[1]/div[1]/span[2]
	#//Pendulum Scale
	${HasPendulum}=		Run Keyword And Return Status		Page Should Contain Element		//span[contains(text(), "Pendulum Scale")]
	${PEND}=			Run Keyword If		${HasPendulum}	Get Text	//div[@class="CardText pen"]//div[@class="item_box t_center pen_s"]/span[2]		ELSE	Set Variable	0
	#//Sets/Prints count
	${SETSCOUNT}=		Get Matching Xpath Count		//div[@id="update_list"]/div[2]/div[@class='t_row']	
	#//Generate the data set
	# @{CARDDATA}=		Create List		${CARDNAME}	${ATTRIBUTE}	${SPECIES}	${LEVELRANKLINK}	${ATA}	${DEF}	${PEND}	${SETSCOUNT}
	${CARDDATA}=		Set Variable	${CARDNAME}|${ATTRIBUTE}|${SPECIES}|${LEVELRANKLINK}|${ATA}|${DEF}|${PEND}|${SETSCOUNT}
	[Return]			${CARDDATA}		${CARDNAME}
	#####
	
CardInfo_Scan SpellTrap Card
	####
	#//Card Name
	${CARDNAME}=		Get Text		//div[@id="cardname"]//h1
	${nameUpdated}=		Run Keyword And Return Status		Should Contain		${CARDNAME}		(Updated from:
	${CARDNAME}=		Run Keyword If		${nameUpdated}	Fetch From Left		${CARDNAME}			${space}(Updated from:		ELSE		Set Variable	${CARDNAME}
	#//Spell/Trap type
	${SPECIES}=			Get Text		//div[@class="item_box t_center"]/span[2]
	#//Level/Rank/Link
	${LEVELRANKLINK}=	Set Variable	0
	#//ATA/DEF
	${ATA}=				Set Variable	0
	${DEF}=				Set Variable	0
	#//Attribute
	${ATTRIBUTE}=		Set Variable	NONE
	#//Pendulum Scale
	${PEND}=			Set Variable	0
	#//Sets/Prints count
	${SETSCOUNT}=		Get Matching Xpath Count		//div[@id="update_list"]/div[2]/div[@class='t_row']	
	#//Generate the data set
	${CARDDATA}=		Set Variable	${CARDNAME}|${ATTRIBUTE}|${SPECIES}|${LEVELRANKLINK}|${ATA}|${DEF}|${PEND}|${SETSCOUNT}
	[Return]			${CARDDATA}		${CARDNAME}
	#####
	
CardInfo_Scan Sets
	####
	${sets}=				Get Matching Xpath Count		//div[@id="update_list"]//div[@class="t_body"]/div[@class="t_row"]
	@{CodeList}=			Create List
	${FullSetsString}=		Set Variable		${EMPTY}
	:FOR	${i}	IN RANGE		1	${sets}+1
	\		${date}=		Get Text			//div[@id="update_list"]//div[@class="t_body"]/div[@class="t_row"][${i}]//div[@class="time"]
	\		${code}=		Get Text			//div[@id="update_list"]//div[@class="t_body"]/div[@class="t_row"][${i}]//div[@class="card_number"]
	\		${set}=			Get Text			//div[@id="update_list"]//div[@class="t_body"]/div[@class="t_row"][${i}]//div[@class="pack_name flex_1"]
	\		${rarity}=		Get Rarity			${i}
	\		Append To List			${CodeList}		${code}
	\		${setstring}=	Set Variable			${date}|${code}|${set}|${rarity}
	\		${FullSetsString}=		Set Variable	${FullSetsString}${setstring}|
	[Return]			${FullSetsString}			${CodeList}
	#####

Get Rarity
	####
	[Arguments]							${i}	
	${iconClass}=			Get Element Attribute	//div[@id="update_list"]//div[@class="t_body"]/div[@class="t_row"][${i}]//div[3]/div@class
	${rarity}=				Set Variable			?
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_1"		Set Variable			Common			ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_2"		Set Variable			Rare			ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_3"		Set Variable			Super Rare		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_4"		Set Variable			Ultra Rare		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_5"		Set Variable			Secret Rare		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_6"		Set Variable			Ultimate Rare	ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_7"		Set Variable			Ghost Rare		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_8"		Set Variable			Gold Rare		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_9"		Set Variable			Hobby			ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_14"	Set Variable			Gold Secret		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_15"	Set Variable			Platinum Secret Rare	ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_16"	Set Variable			COLLECTOR'S RARE		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_17"	Set Variable			Platinum Rare		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_18"	Set Variable			Starfoil				ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_19"	Set Variable			Mosaic Rare				ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_20"	Set Variable			Shattefoil				ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_36"	Set Variable			Prismatic Secret Rare	ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_37"	Set Variable			10000 SECRET RARE	ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_38"	Set Variable			Ultra Rare (Pharaoh's Rare)		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_51"	Set Variable			Quarter Century Secret Rare		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_44"	Set Variable			Ultra Rare (Pharaoh's Rare)		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_46"	Set Variable			Ultra Rare (Duelist Saga Version)		ELSE		Set Variable		${rarity}
	${rarity}=				Run Keyword If			"${iconClass}"=="lr_icon rid_47"	Set Variable			Starlight Rare		ELSE		Set Variable		${rarity}
	[Return]				${rarity}
	#####

Override Custom ID
	####
	[Arguments]			${cardname}		${currentID}
	${id}=				Set Variable	${currentID}
	${id}=				Run Keyword IF	"${cardname}"=="Grizzly, the Red Star Beast"				Set Variable	90000001		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="The Twin Kings, Founders of the Empire"		Set Variable	90000002		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="King Landia the Goldfang"					Set Variable	90000003		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Skuna, the Leonine Rakan"					Set Variable	90000004		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Legendary Dragon of White"					Set Variable	90000005		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Juno, the Celestial Goddess"				Set Variable	90000006		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Queen of Fate - Eternia"					Set Variable	90000007		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Stardust Divinity"							Set Variable	90000008		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Tyr, the Vanquishing Warlord"				Set Variable	90000009		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Skyfaring Castle of the Black Forest"		Set Variable	90000010		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Grandopolis, The Eternal Golden City"		Set Variable	90000011		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Leonardos Silver Skyship"					Set Variable	90000012		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Lorelei, the Symphonic Arsenal"				Set Variable	90000013		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Card of Last Will"							Set Variable	90000014		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Queen Nereia the Silvercrown"				Set Variable	90000015		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Shelga, the Tri-Warlord"					Set Variable	90000016		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Kuzunoha, the Onmyojin"						Set Variable	90000017		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Legendary Magician of Dark"					Set Variable	90000018		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Testament of the Arcane Lords"				Set Variable	90000019		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Emperor of Lightning"						Set Variable	90000020		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="EHERO Pit Boss"								Set Variable	90000021		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Kaiser Eagle, the Heavens Mandate"			Set Variable	90000023		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Armament of the Lethal Lords"				Set Variable	90000030		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Sakyo, Swordmaster of the Far East"			Set Variable	90000031		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Aggiba, the Malevolent Shnn Syo"			Set Variable	90000032		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Gatebridgeo the Waterfront Warbeast"		Set Variable	90000033		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Masterful Magician, Servant of the Sanctuary"		Set Variable	90000034		ELSE	Set Variable	${id}
	${id}=				Run Keyword IF	"${cardname}"=="Meteo the Matchless"						Set Variable	501000001		ELSE	Set Variable	${id}
	[Return]			${id}
	#####
	
Override Prices
	####
	[Arguments]			${cardname}		${CodeList}		${CurrentPricesString}
	
	${ZeroesPriceList}=		Set Variable		${EMPTY}
	:FOR    ${code}    IN    @{CodeList}
	\		${Prices}=		Set Variable	$0.00|$0.00
	\		${ZeroesPriceList}=		Set Variable	${ZeroesPriceList}${Prices}|
	
	#//if the card in question if one of the illegal cards return the zeroes priceliss
	Return from Keyword IF	"${cardname}"=="Grizzly, the Red Star Beast"				${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="The Twin Kings, Founders of the Empire"		${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="King Landia the Goldfang"					${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Skuna, the Leonine Rakan"					${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Legendary Dragon of White"					${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Skyfaring Castle of the Black Forest"		${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Juno, the Celestial Goddess"				${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Queen of Fate - Eternia"					${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Stardust Divinity"							${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Tyr, the Vanquishing Warlord"				${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Grandopolis, The Eternal Golden City"		${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Leonardos Silver Skyship"					${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Lorelei, the Symphonic Arsenal"				${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Card of Last Will"							${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Queen Nereia the Silvercrown"				${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Shelga, the Tri-Warlord"					${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Kuzunoha, the Onmyojin"						${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Legendary Magician of Dark"					${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Testament of the Arcane Lords"				${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Emperor of Lightning"						${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="EHERO Pit Boss"								${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Kaiser Eagle, the Heavens Mandate"			${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Armament of the Lethal Lords"				${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Sakyo, Swordmaster of the Far East"			${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Ulevo"										${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Aggiba, the Malevolent Shnn Syo"			${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Gatebridgeo the Waterfront Warbeast"		${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Masterful Magician, Servant of the Sanctuary"	${ZeroesPriceList}
	Return from Keyword IF	"${cardname}"=="Meteo the Matchless"						${ZeroesPriceList}
	
	#//otherwise return the current to not affect the OG prise string
	[Return]			${CurrentPricesString}
	#####
	
Create File String
	####
	[Arguments]		${cardlist}
	${FileString}=	Set Variable	${EMPTY}
	:FOR	${item}	IN		@{cardlist}
	\		${FileString}=		Set Variable	${FileString}${item}\n
	[Return]		${FileString}
	#####

#------PRODECK CARD SEARCH------#

Search Card In Prodeck
	####
	[Arguments]		${key}		${cardname}
	#//First determine if key is in the ProDeckURLs dict to use the direct link
	${DirectLinkAvailable}=		Run keyword if  '${key}' in $ProDeckURLs		Set Variable	True	ELSE	Set Variable	False
	${DirectURL}=				Run keyword if  ${DirectLinkAvailable}		Set Variable	${ProDeckURLs['${key}']}	ELSE	Set Variable	NONE
	
	#//Go to direct link or do the search
	Run Keyword if			${DirectLinkAvailable}		Go To	${DirectURL}		ELSE		Prodeck Manual search	${cardname}	

	#//Verify Page Loaded correctly
	#//DO NOT ENABLE THIS LINE AGAIN: Wait Until Element Is Visible		//div[@class="card-sets-tcgplayer-small"]
	Wait Until Element Is Visible		//div[@class="card-image"]
	Wait Until Element Is Visible		//ul[@class="card-data-info"]
	Sleep		1s
	
	#if you got to this point while direct link was not available, add the ULR to the dict of prodeck urls
	Run Keyword If	${DirectLinkAvailable}==False			YGO_Add Card URL to File			${key}
	#####

Prodeck Manual search
	####
	[Arguments]			${name}
	Log To Console		Doing a Manual Search for [${name}]
	Go To		https://ygoprodeck.com/card-database/?&sort=name&num=24&offset=0
	#//Wait For the page to load and select to display 100 items
	Wait Until Element Is Visible		//input[@id="filter-dq"]
	
	#//Remove the "Fuzzy Search Option"
	${checked}=		Get Element Attribute		//input[@id="fuzzySearch"]@checked
	Run Keyword IF	"${checked}"=="true"	Click Element		//label[@for="fuzzySearch"]
	Sleep			1s
	
	Click Element	//select[@id="filter-limit"]
	Click Element	//select[@id="filter-limit"]/option[.='Limit 100']
	
	#//Input the card name and wait for the results to appear
	Input Text		//input[@id="filter-dq"]	${name}
	Sleep			2s
	
	${Results}=		Run Keyword And Return Status		Page Should Not Contain Element	//h1[.='No Results Found.']
	${Results}=		Run Keyword IF	${Results}			Prodeck_Select Result		${name}		ELSE	Set Variable	False
	[Return]		${Results}
	#####
	
Prodeck_Select Result
	####
	[Arguments]			${name}
	#//Once the results area is loaded click the card 
	Wait Until Element Is Visible		//div[@id="api-area-results"]
	ProDeck_Click Card From Results		${name}
	
	#//In case of ADS, reload the page. This should either take u to the card page or back to results
	Reload Page
	Sleep		1s
	
	#//Case of Ads and reload send you back to the results page, click on the card again
	${InCardPage}=		Run Keyword And Return Status		Page Should Contain Element		//ul[@class="card-data-info"]
	Run Keyword If		${InCardPage}==False		ProDeck_Click Card From Results			${name}

	#//Verify you are in the right page
	
	${CardFound}=		Run Keyword And Return Status		Page Should Not Contain Element	//h1[.='Not Found']
	Run Keyword IF		${CardFound}		Page Should Contain Element		//ul[@class="card-data-info"]
	[Return]			${CardFound}
	#####
	
ProDeck_Click Card From Results
	####
	[Arguments]			${name}
	
	#//Remove the "Fuzzy Search Option"
	${checked}=		Get Element Attribute		//input[@id="fuzzySearch"]@checked
	Run Keyword IF	"${checked}"=="true"	Scroll and click		//label[@for="fuzzySearch"]
	Sleep			1s
	
	${oneResult}=	Run Keyword And Return Status	Page Should Contain Element		//span[@class="api-paging-total-cards"][.='1']
	Run Keyword If 	${oneResult}		Click Element	//div[@id="api-area-results"]/a
	# ${name}=		Prodeck_Name MOD	${name}
	# Run Keyword If 	${oneResult}==False	Click Element	//div[@title="${name}"]
	Run Keyword If 	${oneResult}==False			Scroll and click	//div[@title="${name}"]
	Sleep			1s
	#####

Scroll and click
	####
	[Arguments]		${xpath}
	${y}=				Get Vertical Position		${xpath}
	${scroll}=			Evaluate		int(float("${y}")) - 100
	Execute Javascript							scrollTo(0, ${scroll})
	Click Element			${xpath}
	#####
	
#----------PRODECK ID and PRICE EXTRACTIONS-----------------#

ProDeckCard_Extract ID
	####
	${source}=		Get Element Attribute		//img[@class="zoom"]@src
	${id}=			Fetch From Left		${source}		.jpg
	${id}			Fetch From Right	${id}			cards/
	[Return]		${id}
	#####
	
ProDeckCard_ExtractPriceList
	####
	[Arguments]		${CodeList}
	#//IN case this page doesnt have any prices....
	${HasTCGSets}=	Run Keyword And Return Status	Page Should Contain Element		//div[@class="card-sets-tcgplayer-small"]

	${HaViewMore}=	Run Keyword And Return Status	Page Should Contain Element		//div[@class="card-sets-tcgplayer-small"]//a[.='View More']
	
	#//Attempt to close out the subscribe popup
	Run Keyword And Ignore Error			Click Element			//button[.='Later']
	
	#//Extract URLS and TCGPrices if this page has TCG sets in the page
	Run Keyword If		${HasTCGSets}		Extract URLS and Prices		${HaViewMore}
	
	#//After the above executes the PriceListExtracted dict should be populated
	#//Use this dictionary to pupulate the codes based on the CodeList
	${PriceListString}=		Set Variable		${EMPTY}
	:FOR    ${code}    IN    @{CodeList}
	\		${IsCodeInPriceListExtracted}=		Run keyword if  '${code}' in $PriceListExtracted	Set Variable	True	ELSE	Set Variable	False
	\		${Prices}=			Run keyword if	${IsCodeInPriceListExtracted}		Set Variable	${PriceListExtracted['${code}']}	ELSE	Set Variable	$0.00|$0.00
	\		${PriceListString}=		Set Variable	${PriceListString}${Prices}|
	
	[Return]		${PriceListString}
	#####
	
Extract URLS and Prices
	####
	[Arguments]		${HaViewMore}
	#//Extract the URLs to TCG Player that Prodeck has
	${TCG_URLS}=		Run Keyword If	${HaViewMore}		ProDeckCard_Extract URLS From More Window	ELSE	ProDeckCard_Extract URLS From Page
	
	#//Visit each URL to extract the prices of each page and populate the PriceListExtracted dict
	TCG_Extract Prices From URL List		${TCG_URLS}
	#####
	
ProDeckCard_Extract URLS From More Window
	####
	${y}=				Get Vertical Position		//div[@class="card-sets-tcgplayer-small"]//a[.='View More']
	${scroll}=			Evaluate		int(float("${y}")) - 100
	Execute Javascript							scrollTo(0, ${scroll})
	
	Click Element	//div[@class="card-sets-tcgplayer-small"]//a[.='View More']
	Wait Until Element Is Visible		//div[@class="modal-body tcgplayer-modal"]
	
	@{URLlist}=			Create List
	${SetsCount}=		Get Matching Xpath Count		//div[@class="card-sets-tcgplayer"]/li
	:FOR	${i}	IN RANGE		1	${SetsCount}+1
	#//Scroll element into view to prevent issues with visibility
	\		Run Keyword IF		"${i}"!="${SetsCount}"	Execute Javascript	window.document.getElementsByClassName("list-group-item d-flex justify-content-between")[${i}].scrollIntoView()
	#//Attempt to close out the subscribe popup
	\		Run Keyword And Ignore Error		Click Element			//button[.='Later']
	#//Extract the URL
	\		${url}=				Get Element Attribute			//div[@class="card-sets-tcgplayer"]/li[${i}]/span/a@href
	\		Append To List		${URLlist}			${url}	
	
	#//Now that we have all URLS in the list just return the List
	[Return]		${URLlist}
	#####
	
ProDeckCard_Extract URLS From Page
	####
	@{URLlist}=			Create List
	${Sets}=		Get Matching Xpath Count	//div[@class="card-sets-tcgplayer-small"]/div
	:FOR	${i}	IN RANGE		1	${Sets}+1
	\		${y}=				Get Vertical Position		//div[@class="card-sets-tcgplayer-small"]/div[${i}]/a
	\		${scroll}=			Evaluate		int(float("${y}")) - 100
	\		Execute Javascript							scrollTo(0, ${scroll})
	#//Extract the URL
	\		${url}=				Get Element Attribute			//div[@class="card-sets-tcgplayer-small"]/div[${i}]/a@href
	\		Append To List		${URLlist}			${url}	
	#//Now that we have all URLS in the list just return the List
	[Return]		${URLlist}
	#####

TCG_Extract Prices From URL List
	####
	[Arguments]		${URL_List}
	:FOR    ${url}    IN    @{URL_List}
	\		${IsURLValid}=		Run Keyword And Return Status	TCG_Go To Card Page		${url}
	\		${code}		${MarketPrice}	${MedianPrice}=		Run Keyword If	${IsURLValid}		TCGCardPage_Extract Prices	ELSE	Set Variable		XXXX-EN000	$0.00	$0.00
	#//Add price to dictionary if it is not in yet
	\		${IsCodeInPriceListExtracted}=		Run keyword if  '${code}' in $PriceListExtracted	Set Variable	True	ELSE	Set Variable	False
	\		Run Keyword if		${IsCodeInPriceListExtracted}		ComparePrice	${code}		${MarketPrice}	${MedianPrice}	${url}		ELSE		Set Price	${code}		${MarketPrice}	${MedianPrice}	${url}
	#####
	
Set Price
	[Arguments]		${code}		${MarketPrice}	${MedianPrice}		${URL}
	Set To Dictionary   	${PriceListExtracted}   ${code}=${MarketPrice}|${MedianPrice}
	Set To Dictionary		${TCGURLS}  			${code}=${URL}
	
ComparePrice
	####
	[Arguments]		${code}		${MarketPrice}	${MedianPrice}		${URL}
	Return From Keyword If		"${MarketPrice}"=="$0.00"		0
	#//ELSE compare PRICE
	${MarketPrice}=				Replace String		${MarketPrice}	$		${EMPTY}
	${MarketPriceInFloat}=		Convert To Number		${MarketPrice}
	${PriceAlreadyInDic}=		Set Variable	${PriceListExtracted['${code}']}
	${PriceAlreadyInDic}=		Fetch From Left		${PriceAlreadyInDic}	|
	${PriceAlreadyInDic}=		Replace String		${PriceAlreadyInDic}	$		${EMPTY}
	${PriceAlreadyInDic}=		Convert To Number	${PriceAlreadyInDic}
	#//Do the less than comparison
	${NewPriceIsLess}=			Run Keyword If		${MarketPriceInFloat} < ${PriceAlreadyInDic}		Set Variable	True		ELSE	Set Variable	False
	#//if less, replace the old price with the current in check
	Run Keyword If				${NewPriceIsLess}		Set To Dictionary		${PriceListExtracted}  ${code}=${MarketPriceInFloat}|${MedianPrice}
	Run Keyword If				${NewPriceIsLess}		Set To Dictionary		${TCGURLS}  			${code}=${URL}
	#####
	
TCG_Go To Card Page
	####
	[Arguments]		${url}
	Go To			${url}
	Wait Until Element Is Visible		//h2[.='Product Details']
	Wait Until Element Is Visible		//span[.=' Market Price ']
	Wait Until Element Is Visible		//span[.=' Listed Median Price ']
	Run Keyword And Ignore Error		Click Element	//div[@class="product__item-details__toggle masked"]
	Sleep	1s
	#//Check for rate this page popup
	Run Keyword And Ignore Error		//button[@aria-label="Close"]
	#####
	
TCGCardPage_Extract Prices
	####
	${code}=		Get Text			//ul[@class="product__item-details__attributes"]/li[1]//span
	${MarketPrice}=	Get Text			//section[@class="price-points price-guide__points"]/table/tr[2]/td[2]/span
	${MedianPrice}=	Get Text			//section[@class="price-points price-guide__points"]/table/tr[4]/td[2]/span
	# ${Prices}=		Set Variable		${MarketPrice}|${MedianPrice}
	#//If the price extracted was "-" replaced with $0.00
	${MarketPrice}=		Replace String		${MarketPrice}	-		$0.00
	${MarketPrice}=		Replace String		${MarketPrice}	,		${EMPTY}
	
	${MedianPrice}=		Replace String		${MedianPrice}	-		$0.00
	${MedianPrice}=		Replace String		${MedianPrice}	,		${EMPTY}
	[Return]		${code}		${MarketPrice}	${MedianPrice}
	#####

#--------------Scanning sets keywords--------------#
	
Scan Set Group
	####
	[Arguments]						${boosterPackXpath}		${groupName}
	${boosterPackXpath}=			Set Variable			${boosterPackXpath}
	${yeahXpath}=					Set Variable			//div[@class="pack_m${SPACE}${SPACE}open${SPACE}"]
	${fullyearxpath}=				Set Variable			${boosterPackXpath}${yeahXpath}
	${years}=						Get Matching Xpath Count			${fullyearxpath}
	${loopIterations}=				Evaluate			${years} + 1
	
	@{yearlist}=					Create List
	@{yearsetslist}=				Create List
	:FOR	${i}	IN RANGE		1		${loopIterations}
	\		${index}=				Set Variable				[${i}]
	\		${thisyear}=			Get Text					${fullyearxpath}${index}/p
	\		Append To List			${yearlist}					${thisyear}
	# \		Run Keyword If 			"${i}"!="1"		Click Element		${fullyearxpath}${index}
	# \		Sleep					1s
	\		${yearline}=			Get This Year Pack String	${thisyear}		${boosterPackXpath}		${index}
	\		Append to List			${yearsetslist}				${yearline}
	
	${filestring}=					Set Variable				${groupName}\n${years}\n
	:FOR	${i}	IN RANGE		0		${years}
	\		${filestring}=			Set Variable				${filestring}${yearsetslist}[${i}]\n
	
	Create File				${EXECDIR}/ExtractedFiles/Sets/${groupName}.txt		${filestring}
	#####
	
Scan Set Group2
	####
	[Arguments]						${boosterPackXpath}		${groupName}
	${boosterPackXpath}=			Set Variable			${boosterPackXpath}
	${yeahXpath}=					Set Variable			//div[@class="pack_m open"]
	${fullyearxpath}=				Set Variable			${boosterPackXpath}${yeahXpath}
	${years}=						Get Matching Xpath Count			${fullyearxpath}
	${loopIterations}=				Evaluate			${years} + 1
	
	@{yearlist}=					Create List
	@{yearsetslist}=				Create List
	:FOR	${i}	IN RANGE		1		${loopIterations}
	\		${index}=				Set Variable				[${i}]
	\		${thisyear}=			Get Text					${fullyearxpath}${index}/p
	\		Append To List			${yearlist}					${thisyear}
	# \		Run Keyword If 			"${i}"!="1"		Click Element		${fullyearxpath}${index}
	# \		Sleep					1s
	\		${yearline}=			Get This Year Pack String	${thisyear}		${boosterPackXpath}		${index}
	\		Append to List			${yearsetslist}				${yearline}
	
	${filestring}=					Set Variable				${groupName}\n${years}\n
	:FOR	${i}	IN RANGE		0		${years}
	\		${filestring}=			Set Variable				${filestring}${yearsetslist}[${i}]\n
	
	Create File				${EXECDIR}/ExtractedFiles/Sets/${groupName}.txt		${filestring}
	#####
	
Get This Year Pack String
	[Arguments]				${thisyear}		${boosterPackXpath}		${index}
	${numberofpacks}=		Get Matching Xpath Count	${boosterPackXpath}//div[@class="toggle"]${index}/div
	${loopIterations}=				Evaluate			${numberofpacks} + 1
	
	@{PackNameList}=				Create List
	:FOR	${i}	IN RANGE		1		${loopIterations}
	\		${packindex}=			Set Variable	[${i}]
	\		${xpath}=				Set Variable	${boosterPackXpath}//div[@class="toggle"]${index}/div${packindex}/p/strong
	\		${thispackname}=		Get text		${xpath}
	\		Append To List			${PackNameList}			${thispackname}
	
	#//Create the single string
	${retunvalue}=					Set Variable			${thisyear}|${numberofpacks}
	:FOR	${thissetname}	IN		@{PackNameList}
	\		${retunvalue}=			Set variable			${retunvalue}|${thissetname}
	
	[Return]				${retunvalue}