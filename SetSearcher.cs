﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yu_Gi_Oh__Card_Collector___Complete_Edition
{
    public partial class SetSearcher : Form
    {
        public SetSearcher()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            string searchTerm = txtCodeSearch.Text.ToString();

            //scan the data base for each set group match
            if(searchTerm.Length >= 3)
            {
                List<string> results = DataBase.GetSetsWithCode(searchTerm);

                //print the results
                lblCodeSearchOutput.Text = "";
                foreach (string line in results)
                {
                    lblCodeSearchOutput.Text = lblCodeSearchOutput.Text + line + "\r\n";
                }
            }
        }
    }
}
